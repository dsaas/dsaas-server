var elements = {};
var time = null;
var active = true;
module.exports = {
	activate: function () {
		active = true;
	},

	collect: function (name) {
		if (active) {
			if (!time) {
				time = process.hrtime();
				return;
			} else {
				if (!elements[name]) elements[name] = {sec: 0, ns: 0, called: 0};
				var diff = process.hrtime(time);
				elements[name].sec += diff[0];
				elements[name].ns += diff[1];
				elements[name].called++;
				time = process.hrtime();
			}
		}
	},

	display: function () {
		var total = 0;
		for (var t in elements) {
			total += elements[t].sec + elements[t].ns * 1e-9;
		}
		console.log(total);
		for (t in elements) {
			console.log(t, (elements[t].sec + elements[t].ns * 1e-9) / total * 100);
		}
	}
};