var crypto = require('crypto'),
	xor = require('bitwise-xor');

var BIT_STRING_LENGTH = 64;


function toBuffer(version) {
	if (version == 0) {
		var newBuff = new Buffer(BIT_STRING_LENGTH, 'hex');
		newBuff.fill(0);
		return newBuff;
	}
	return new Buffer(version, 'hex');
}


module.exports = {

	popcount: function (x) {
		// http://en.wikipedia.org/wiki/Hamming_weight#Efficient_implementation
		var count;
	    for (count=0; x; count++) x &= x-1;
	    return count;
	},

	genHash: function (str) {
		// http://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript-jquery
		str = String(str);
		var hash = 0, i, chr, len;
		for (i = 0, len = str.length; i < len; i++) {
			chr   = str.charCodeAt(i);
			hash  = ((hash << 5) - hash) + chr;
			hash |= 0; // Convert to 32bit integer
		}
		return hash;
	},

	randomBitString: function (len) {
		return crypto.randomBytes(Math.ceil(len/2))
			.toString('hex')
			.slice(0, len);
	},

	randomKeyValueString: function () {
		return this.randomBitString(BIT_STRING_LENGTH);
	},

	zobristHash: function (state, newhash) {
		return xor(toBuffer(state), toBuffer(newhash)).toString('hex');
		// return (parseInt(state, 16) ^ parseInt(newhash, 16)).toString(16);
	},

	ROOT_VERSION: '0'
};
