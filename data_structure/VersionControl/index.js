
/**
 * VersionControl
 */
module.exports = function (ID, db) {
	return {
		next: function (version, message) {
			return utils.hash(version + message);
		},

		exists: function* (version) {
			try {
				yield db.get(db.__(ID, db.constant.root, version));
				return true;
			} catch (e) {
				return false;
			}
		}
	}
};