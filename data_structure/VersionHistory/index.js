'use strict';

var	notifier = require('../../config/notifier')('global');
var utils = require('../utils');

module.exports = function VersionHistory(ID, db) {

	var versionHistoryKey = db.__(ID, db.constant.DAG);

	return {
		create: function* create() {
			console.log('Root: ', utils.ROOT_VERSION);
			yield [
				db.putJSON(db.__(versionHistoryKey, utils.ROOT_VERSION), {children: {}, parents: {}})
			];
			return this;
		},

		exists: function* exists(version) {
			var key = db.__(versionHistoryKey, version);
			try {
				version = yield db.getJSON(key);
				return true;
			} catch (err) {
				return false;
			}
		},

		add: function add (child, parent, cb) {

			try {
				if (Number(child) === 0) {
					child = '0';
				}
			} catch (e){}
			try {
				if (Number(parent) == 0) {
					parent = '0';
				}
			} catch (e) {}

			notifier.emit('versionAdded', child, parent, ID);

			// Removing self-loops from parent. Issue 1.
			if (child == parent) return cb(null, this);

			// Retrieve the "child" version from the database if it exists,
			// otherwise create a new child version with empty children and empty
			// parents.
			var childVersion = child;
			var childKey = db.__(versionHistoryKey, child);

			db._getJSON(childKey, parseChild);

			function parseChild(err, c) {
				if (err) {
					child = {children: {}, parents: {}};
				} else {
					child = c;
				}
			
				db._getJSON(db.__(versionHistoryKey, parent), parseParent);	
			}

			function parseParent(err, p) {
				// Get the parent version from the database
				if (err) return cb(err);
				if (!p) return cb(new Error('The parent does not exist'));

				// Only add the parent if it does not already exist.
				// NOTE: Are there performance issues here with use objects?
				// Should we rather be using arrays?
				// Test this...
				if (!child.parents[parent]) {
					
					child.parents[parent] = 1;
					p.children[childVersion] = 1;

					var doneCalled = 0, doneErrs = [];
					db._putJSON(childKey, child, done);
					db._putJSON(db.__(versionHistoryKey, parent), p, done);

					function done(err) {
						if (err) doneErrs.push(err);
						doneCalled++;
						if (doneCalled == 2) {
							if (doneErrs.length > 0) return cb(doneErrs);
							return cb(null, this);
						}
					}
				} else {
					return cb(null, this);
				}
			}
		},

		commonAncestor: function* commonAncestor(v1, v2) {
			var set1 = {}, set2 = {};
			var seq1 = [], seq2 = [];
			var explored = {};

			seq1.push(v1);
			set1[v1] = true;
			seq2.push(v2);
			set2[v2] = true;


			while (v1 != db.__(versionHistoryKey, 0) && v2 != db.__(versionHistoryKey, 0)
				&& seq1.length > 0 && seq2.length > 0) {
			
				// Remove the next parent to explore
				v1 = seq1.shift();
				// Avoid re-exploring parents that have been explored
				while (explored[v1]) v1 = seq1.shift();
				explored[v1] = true;

				v2 = seq2.shift();
				while (explored[v2]) v2 = seq2.shift();
				explored[v2] = true;

				// Retrieve the parents from the database
				let p1 = yield db.getJSON(db.__(versionHistoryKey, v1));
				let p2 = yield db.getJSON(db.__(versionHistoryKey, v2));

				// Loop through the parents and add them to the set and sequence
				for (let k in p1.parents) {
					set1[k] = true;
					// If the parent exists in the other set, return that parent
					if (set2[k]) return k;
					seq1.push(k);
				}

				for (let k in p2.parents) {
					set2[k] = true;
					if (set1[k]) return k;
					seq2.push(k);
				}
			}

			return 0;
		},

		get: function get (cb, level) {
			var obj = {};
			var start = db.__(versionHistoryKey, '\0'),
				end = db.__(versionHistoryKey, '~');

			db.createReadStream({
				gte: start,
				lte: end
			})
			.on('data', function(data) {
				
				var key = data.key.substring(versionHistoryKey.length + 1);
				var value = JSON.parse(data.value);

				if (level == 0) {
					obj[key] = Object.keys(value.parents);
				} else if (level == 1) {
					obj[key] = Object.keys(value.children);
				} else {
					obj[key] = {
							parents: Object.keys(value.parents),
							children: Object.keys(value.children)
						};
				}
			})
			.on('error', cb)
			.on('end', function () { cb(null, obj); });
		},

		import: function importHistory (importID, cb) {
			var importKey = db.__(importID, db.constant.DAG),
				obj = {},
				start = db.__(importKey, '\0'),
				end = db.__(importKey, '~'),
				putData = [];

			db.createReadStream({
				gte: start,
				lte: end
			})
			.on('data', function (data) {
				data.key = db.__(versionHistoryKey, data.key.substring(importKey.length + 1));
				putData.push({
					type: 'put',
					key: data.key,
					value: data.value
				});
			})
			.on('error', cb)
			.on('end', function () {
				db._db.batch(putData, function () {
					cb(null);
				});
			});
		},

		callback: function callback (err, vh) {
			if (err) {
				console.log("An error occured with the version history updating.");
				console.log(err.stack);
			}
		}
	};
};