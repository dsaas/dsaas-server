'use strict';

var utils = require('../utils'),
	Node = require('./Node'),
	Package = require('./Package');

module.exports = function (Helpers, BUILD_VERSION) {

	function* diffPackageExplore(node, packageContainer, A, level) {
		let str = '';
		let packageContainerHandled = false;
		let hash = utils.genHash(packageContainer.key);

		for (let p of node.arrayOfPointers) {
			let tempNode = yield Helpers.get(p);
			if (tempNode instanceof Package) {
				if (tempNode.key !== packageContainer.key) {
					if (A) str = concat(str, yield remove(tempNode), true);
					else str = concat(str, yield add(tempNode));
				} else {
					packageContainerHandled = true;
				}
			} else if (tempNode.HID == Helpers.computeHID(hash, level)) {
				str = concat(str, yield diffPackageExplore(tempNode, packageContainer, A, level - Helpers.BITS));
				packageContainerHandled = true;
			} else {
				if (A) str = concat(str, yield remove(tempNode), true);
				else str = concat(str, yield add(tempNode));
			}
		}

		if (!packageContainerHandled) {
			if (A) str = concat(str, yield add(packageContainer));
			else str = concat(str, yield remove(packageContainer), 'f');
		}
		return str;
	}

	function* add    (node) { return yield addStrDiff(node, '+'); }

	function* remove (node) { return yield addStrDiff(node, '-'); }

	function* addStrDiff(node, op) {
		var str = '';
		if (node instanceof Package) {
			if (BUILD_VERSION) {
				let bitString = (yield packageCollection.findById(node.dbID)).bitString;
				return bitString;
			}
			else return op + ' ' + node.key + ': ' + JSON.stringify(node.value) + '\n';
		} else if (node instanceof Node) {
			for (let p of node.arrayOfPointers) {
				str = concat(str, yield addStrDiff(yield Helpers.get(p), op));
			}
			return str;
		} else {
			console.assert('Illegal state in diff of versions of data structure.');
		}
	}

	function concat(s1, s2, dont) {
		if (BUILD_VERSION) {
			if (dont) return s1;
			if (s1 == '') return s2;
			if (s2 == '') return s1;
			return utils.zobristHash(s1, s2);
		} else {
			if (!s2) return s1;
			if (!s1) return s2;
			return s1 + s2;
		}
	}

	function*  VersionedTrieDiff (node, level) {
		// The recursive function for calculating the difference
		var str = '';
		
		if (node.A instanceof Node && node.B instanceof Node) {
			var i = 0, j = 0, 
				sizeA = node.A.arrayOfPointers.length, 
				sizeB = node.B.arrayOfPointers.length;

			// Compare the pointers in nodeA with the pointers in nodeB
			while (i < sizeA && j < sizeB) {
				// If the pointers are equal, just continue
				if (node.A.arrayOfPointers[i] === node.B.arrayOfPointers[j]) {
					i++; j++;
					continue;
				}

				// They are unequal, so we need the object of the pointer
				var tempNode = yield {
					A: Helpers.get(node.A.arrayOfPointers[i]),
					B: Helpers.get(node.B.arrayOfPointers[j])
				};

				// Check if their hashed identifiers are equal
				if (tempNode.A.HID === tempNode.B.HID || tempNode.A instanceof Package || tempNode.B instanceof Package) {
					// Hashed identifiers are equal, so we recursively explore these nodes
					str = concat(str, yield VersionedTrieDiff(tempNode, level-Helpers.BITS));
					// Move on to the next pointers
					i++; j++;
					continue;
				} else if (tempNode.A.HID > tempNode.B.HID) {
					// NodeA's hashed identifier is bigger than nodeB's hashed identifier
					// So add all the elements in nodeB and move the pointer for nodeB
					str = concat(str, yield add(tempNode.B));
					j++;
				} else {
					// The alternative (opposite) operation than the one above
					str = concat(str, yield remove(tempNode.A), true);
					i++;
				}
			}

			// All the pointers of either have been explored, finish the exploration
			if (i == sizeA) {
				while (j < sizeB) {
					var nodeB = yield Helpers.get(node.B.arrayOfPointers[j]);
					str = concat(str, yield add(nodeB));
					j++;
				}
			} else if (j == sizeB) {
				while (i < sizeA) {
					var nodeA = yield Helpers.get(node.A.arrayOfPointers[i]);
					str = concat(str, yield remove(nodeA), true);
					i++;
				}
			}
		} else if (node.A instanceof Node) {
			str = concat(str, yield diffPackageExplore(node.A, node.B, true, level));
		} else if (node.B instanceof Node) {
			str = concat(str, yield diffPackageExplore(node.B, node.A, false, level));
		} else {
			if (node.B.key != node.A.key) {
				str = concat(str, yield add(node.B));
				str = concat(str, yield remove(node.A), true);
			}
		}

		return str;
	};

	return {
		exec: VersionedTrieDiff,
		concat: concat
	};
}