'use strict';

var	VersionHistory = require('../VersionHistory'),
	Node = require('./Node'),
	Package = require('./Package'),
	// Profiler = require('../profiler'),
	VersionedTrieDiff = require('./VersionedTrieDiff'),
	VersionedTrieMerge = require('./VersionedTrieMerge'),
	VersionedTrieRemove = require('./VersionedTrieRemove'),
	utils = require('../utils');

/**
 * Versioned Trie
 * @param {String} namespace       User's namespace
 * @param {String} datastructureId The identifier for the data structure
 * @param {Object} db              The leveldb wrapper
 */
module.exports = function VersionedTrie(ID, db, versionHistoryID) {

	// Private Objects and Methods
	var Helpers = require('./Helpers')(ID, db);
	
	if (!ID) throw new Error('ID is not defined');
	if (!db) throw new Error('db is not defined');
	if (!versionHistoryID) throw new Error('Version History ID is not defined');

	var versionHistory = VersionHistory(versionHistoryID, db);

	// var redis = require('../../config/redisdb');

	function* batchWrite(rootNode, version) {
		var operations = [];
		// Get lock on nodeId
		// Get nodeId
		var nodeId = yield db.get(db.__(ID, db.constant.nodeId));
		// Profiler.collect('Get node identification: ');

		yield prepareForWrite(rootNode, operations, true);
		// Profiler.collect('Done with the prepare for write: ');
		function* prepareForWrite(cnode, operations, thisIsRoot) {
			if (!thisIsRoot) {
				cnode.id = ++nodeId;
			} else {
				cnode.id = 0;
			}

			for (var pointer of cnode.arrayOfPointers) {
				if (pointer instanceof Node || pointer instanceof Package) {
					if (pointer instanceof Node) {
						yield prepareForWrite(pointer, operations, false);
						cnode.setPointer(pointer.HID, pointer.id);
					} else if (pointer instanceof Package) {
						if (!pointer.id) {
							pointer.id = yield Helpers.genHashDBKey(pointer);
							operations.push({
								type: 'put', 
								key: db.__(ID, db.constant.trie, pointer.id),
								value: JSON.stringify(pointer)
							});
						}
						cnode.setPointer(pointer.HID, pointer.id);
					}
					// The ONLY time pointer.HID should be used is like this,
					// for when a new Node or a new Package is created.
					// Warning: Check for the merge case when it merges a package,
					// something weird might happen.
				} else {
					if (typeof(pointer) != 'number' && typeof(pointer) != 'string')
						throw new Error('Cannot save to DB, pointer is the wrong type');
				}
			}

			if (thisIsRoot) {
				operations.push({
					type: 'put',
					key: db.__(ID, db.constant.root, version),
					value: JSON.stringify(cnode)
				});
			} else {
	 			operations.push({
					type: 'put',
					key: db.__(ID, db.constant.trie, cnode.id),
					value: JSON.stringify(cnode)
				});
			}
		}

		// Update nodeId
		operations.push({
			type: 'put',
			key: db.__(ID, db.constant.nodeId),
			value: nodeId
		});

		if (0) console.log(operations);
		// Release lock on nodeId
		// Execute operations
		
		yield db.batch(operations);
		// Profiler.collect('Done batch put: ');
	}

	// Public Methods
	return {
		/**
		 * Creates the data structure and initializes the values.
		 */
		create: function *() {
			// Check if the trie has already been created.
			try {
				yield db.get(db.__(ID, db.constant.nodeId));
			} catch (err) {
				var rootVersionId = utils.ROOT_VERSION;
				yield [
					db.put(db.__(ID, db.constant.nodeId), 0),
					db.putJSON(db.__(ID, db.constant.root, rootVersionId), new Node(0, null, 0)),
					versionHistory.create()
				];
				return 'success';
			}
			throw new Error('Versioned Trie has already been created.');
		},

		/**
		 * Add a batch of key-value pairs to the given version.
		 * @param  {Object} keysAndValues A map containing key-value pairs
		 * @param  {Number} version       The version identifier
		 */
		 // TODO: Queue updates to avoid asynchronous bugs!
		put: function *(keysAndValues, version, noVersionUpdate, rootNode) {

			// try { yield redis.lock.acquire(ID + ':put'); }
			// catch (e) { console.log('Lock acquired failed, retry: ' + ID); }

			// Profiler.collect('Starting da thing: ');

			// Retrieve the next version identifier
			if (!rootNode) rootNode = new Node(0, yield Helpers.getRoot(version));
			
			// Profiler.collect('Fetching root node: ');

			// For storing the key-values
			var originalVersion = version;
			var updateNecessary = false;
			var nextVersion = version;

			// Loop through all the key value pairs

			for (var key in keysAndValues) {
				// Profiler.collect('Starting with for loop: ');
				// The following section is to check if a similar version exists and to adapt accordingly
				// Does the key-value pair exist in the version?
				let packageContainer = yield this.get(key, version, true);
				// Profiler.collect('Initial package container fetch (for duplication): ');
				
				if (packageContainer && packageContainer.value == keysAndValues[key]) continue;

				// Remove any previous element - this is essentially when updating a key-value
				if (packageContainer) nextVersion = utils.zobristHash(nextVersion, packageContainer.bitString);
				
				// Does the key-value pair exist in the database?
				// Profiler.collect('Initial package container retrieval: ');
				packageContainer = yield Helpers.getWithHash(key, keysAndValues[key]);
				// Profiler.collect('Get with hash: ');

				let bitString = null;

				// console.log('Key Value Pair: ', keyValuePair);

				if (packageContainer) {
					// Use the random bit string with Zobrist Hashing to check if a similar version exists.
					nextVersion = utils.zobristHash(nextVersion, packageContainer.bitString);
					bitString = packageContainer.bitString;

					let newVersionRootNode = yield Helpers.getRoot(nextVersion);
					if (!newVersionRootNode.error) {
						// The version does exist, set this to the version
						version = nextVersion;
						updateNecessary = false;
						continue;
					}
				} else {
					bitString = utils.randomKeyValueString();
					nextVersion = utils.zobristHash(nextVersion, bitString);
				}
				// Profiler.collect('Zobrist Done: ');


				updateNecessary = true;

				// Add the key-value pair to the trie
				var node = rootNode;
				// Add the key-value pair.
				// Retrieve the different nodes from the database that changes.
				var hashKey = utils.genHash(key);
				// Profiler.collect('Starting Insertion: ');

				for (var level = Helpers.SHIFT; level >= 0; level -= Helpers.BITS) {

					// Get the correct number of k bits as the hashed identifier for this level
					var HID = Helpers.computeHID(hashKey, level);
					// Check if the position in the bitmap is set
					if (node.contains(HID)) {
						// Bitmap is set
						var pointer = node.getPointer(HID);
						// The pointer can be of type Node or Package or Something else.
						// In the case it is something else, then it means that the actual element
						// needs to be fetched from the database before any operations may be performed.
						if (!(pointer instanceof Node) && !(pointer instanceof Package)) {
							// Fetch the actual element from the database and set the pointer to that element
							node.setPointer(HID, yield Helpers.get(pointer));
							pointer = node.getPointer(HID);
						}

						if (pointer instanceof Node) {
							/*
							* t1 - node
							*/
							// Follow the pointer of the node
							node = pointer;
						} else if (pointer instanceof Package) {
							// Is the key of the value the same as this key?
							if (pointer.key == key) {
								// ``Replace'' the package pointer
								node.setPointer(HID, new Package(HID, key, keysAndValues[key], bitString));
							} else {
								/**
								* t1 - p1
								*             / p1
								* t2 - - - - -
								* 	          \ p2
								*/
								// ``Remove'' value and expand with nodes
								var hashTempKey = utils.genHash(pointer.key);
								node.setPointer(HID, new Node(HID));
								node = node.getPointer(HID);
								var k1, k2;
								level -= Helpers.BITS;
								// Add the new value and the tempPointer to the array
								for (; level >= 0; level -= Helpers.BITS) {
									k1 = Helpers.computeHID(hashKey, level);
									k2 = Helpers.computeHID(hashTempKey, level);
									if (k1 != k2) break;
									node.addPointer(k1, new Node(k1));
									node = node.getPointer(k1);	
								}
								node.addPointer(k1, new Package(k1, key, keysAndValues[key], bitString));
								node.addPointer(k2, new Package(k2, pointer.key, pointer.value, pointer.bitString, pointer.id));
							}
							break;				
						}
					} else {
						/*
						* t1 - null
						* t2 - p1
						*/
						// If the position is not set, then
						node.addPointer(HID, new Package(HID, key, keysAndValues[key], bitString));
						break;
					}
				}
				// Profiler.collect('Insertion (in memory) done: ');
			}

			// Profiler.collect('Starting the storage...');
			if (noVersionUpdate) return rootNode;
			else {
				if (updateNecessary) {
					yield batchWrite(rootNode, nextVersion);
					// Profiler.collect('After Batch');
					// Update the version history
					versionHistory.add(nextVersion, originalVersion, versionHistory.callback);
					// Profiler.collect('Version History added');
				}

				// try {
				// 	yield redis.lock.release();
				// } catch (e) { console.log('lock release fail: ' + ID); }
				
				// Profiler.collect('lock released and finished insertion...');
				return nextVersion;
			}
		},

		/**
		 * Get the value of the associated key and version
		 * @param {String} key           Key of the value
		 * @param {Number} version       The version of the data structure
		 * @yield {Object} The object (value) associated with the key.
		 */
		get: function* (key, version, asContainer) {
			if (version === undefined) throw new Error('Version must be defined');

			var hashKey = utils.genHash(key),
				node    = yield Helpers.getRoot(version),
				level   = Helpers.SHIFT;

			if (node.error) throw new Error('Version '+version+' does not exist');

			while (level >= 0) {
				var HID = Helpers.computeHID(hashKey, level);
				if (node.contains(HID)) {
					var pointer = yield Helpers.get(node.getPointer(HID));
					if (pointer instanceof Package) {
						if (pointer.key == key) {
							if (asContainer) return pointer;
							return pointer.value;
						}
						else return null;
					} else {
						node = pointer;
					}
				} else return null;
				level -= Helpers.BITS;
			}
			return null;
		},

		/**
		 * Batch remove a list of keys from a specific version
		 * @param {Array} keys          Array of the keys to remove
		 * @param {Number} version       The version to remove from.
		 * @yield {Number} Next version number
		 */
		remove: function* (keys, version, noVersionUpdate, rootNode) {
			if (version === undefined) throw new Error('Version must be defined');
			// if (keys instanceof Array) throw new Error('Removal, keys must be an array of strings not "'+keys+'".');

			if (!rootNode) {
				rootNode = yield Helpers.getRoot(version);
				if (rootNode.error) throw new Error('Version does not exist');
			}

			var nextVersion = version;

			for (let key of keys) {
				// Find the key
				let temp = yield VersionedTrieRemove(ID, db).exec(key, rootNode, Helpers.SHIFT, nextVersion);
				nextVersion = temp[0];
				let pointer = temp[1];

				if (pointer) {
					let pointerHashKey = utils.genHash(pointer.key);
					let pointerHID = Helpers.computeHID(pointerHashKey);
					if (!pointer.id) pointer.HID = pointerHID;				
					rootNode.addPointer(pointerHID, pointer.id || pointer);
				}
			} // End for loop

			if (noVersionUpdate) {
				return rootNode;
			} else {
				let nextRoot = yield Helpers.getRoot(nextVersion);
				if (nextRoot.error) {
					yield batchWrite(rootNode, nextVersion);
					versionHistory.add(nextVersion, version, versionHistory.callback);
				}
				
				// Update the version history
				return nextVersion;
			}
		},

		/**
		 * Generates a set of all the keys in some version of the trie.
		 * @param {Number} version       The version of the trie to work on.
		 * @param {String} filter 		 (Optional) Add a filter to search only for keys containing the filter as substring.
		 * @yield {Array} A set of all the keys in the version.
		 */
		keysOrValues: function* (version, filter, strEdit, values, both, start, end) {
			var node = yield Helpers.getRoot(version),
				lst  = [],
				count = 0,
				set = {};

			if (node.error) throw new Error('Version '+version+' does not exist');

			// Start the search at the root node
			yield     collect(node);
			function* collect(node) {
				// DFS!
				for (var pointer of node.arrayOfPointers) {
					// Retrieve pointer from the database
					pointer = yield Helpers.get(pointer);
					if (pointer instanceof Package) {
						// Retrieve the actual elements
						if (!filter || filter(pointer.key)) {
							var flag = true;
							if (start !== undefined || end !== undefined) {
								flag = false;
								if (start !== undefined && end !== undefined && start < count && count <= end) {
									flag = true;
								} else if (start !== undefined && start < count) {
									flag = true;
								} else if (end !== undefined && count <= end) {
									flag = true;
								}
								if (end !== undefined && count > end) return;
							}
							if (flag) {
								if (!values && !both) {
									// Only the keys
									if (strEdit) lst.push(strEdit(pointer.key));
									else lst.push(pointer.key);
								} else if (!both) {
									// Only the values
									lst.push(pointer.value);
								} else {
									// Both keys and values
									var key = pointer.key;
									if (strEdit) key = strEdit(pointer.key);
									set[key] = pointer.value;
								}
							}
							count++;
						}
					} else if (pointer instanceof Node) {
						if (end !== undefined && count > end) return;
						
						// Continue the search down the tree
						yield collect(pointer);
					} else {
						console.log('Invalid state for retrieving the keys.');
						console.assert(false);
					}
				}
			}

			return both ? set : lst;
		},

		/**
		 * Merge two different versions fo the same trie
		 * @param {Number} versionA      The first version
		 * @param {Number} versionB      The second version
		 * @yield {Number} The new version
		 */
		merge: function* (versionA, versionB) {
			if (versionA == versionB) throw new Error('May not merge the same version.');
			var errors = 0;
			while (errors < 10) {
				try {
					// Find the common ancestor
					var versionK = yield versionHistory.commonAncestor(versionA, versionB);
					console.assert( typeof(versionK) !== 'undefined' );

					// Do a simultanous DFS
					var node = yield {
						K: Helpers.getRoot(versionK), 
						A: Helpers.getRoot(versionA), 
						B: Helpers.getRoot(versionB),
						N: Helpers.getRoot(versionB)
					};

					if (node.A.error || node.B.error) {
						throw new Error('Versions does not exist.');
					}

					var nextVersion = yield VersionedTrieMerge(ID, db)
						.exec(
							node.K, 
							node.A, 
							node.B, 
							node.N, 
							Helpers.SHIFT, 
							versionB
						);

					// If the next version does not exist, do the update.
					var nextRoot = yield Helpers.getRoot(nextVersion);
					if (nextRoot.error) {
						// Store all the new nodes in node.N
						yield batchWrite(node.N, nextVersion);
					}

					// Update the version history
					versionHistory.add(nextVersion, versionA, function (err) {
						if (err) versionHistory.callback(err);
						versionHistory.add(nextVersion, versionB, versionHistory.callback);
					});
					return nextVersion;
				} catch (error) {
					errors++;
					function wait() {
						return function (callback) {
							setTimeout(callback, 5);
						}
					}
					yield wait();
				}
			}
		},

		/**
		 * Calculates the difference between two tries.
		 * The output shows what needs to be changed in versionA to obtain versionB.
		 * 
		 * @param {Number} versionA      The first version
		 * @param {Number} versionB      The second version
		 * @yield {String} The difference
		 */
		diff: function* (versionA, versionB, BUILD_VERSION) {
			var str = '';

			// If the versions are the same then return an empty string
			if (versionA == versionB) return str;

			// Get the roots of the two nodes
			var node = yield {
				A: Helpers.getRoot(versionA),
				B: Helpers.getRoot(versionB)
			};

			// Calculate the difference (recursively)
			let versionedTrieDiff = VersionedTrieDiff(Helpers, BUILD_VERSION);
			str = yield versionedTrieDiff.exec(node, Helpers.SHIFT, BUILD_VERSION);
			
			if (BUILD_VERSION) return versionedTrieDiff.concat(versionA, str, true);
			return str;
		},

		getRoot: Helpers.getRoot,
		getNode: Helpers.get,
		versionHistory: versionHistory,
		type: 'VersionedTrie'
	};

};
