'use strict';

var utils = require('../utils'),
	Node = require('./Node'),
	Package = require('./Package');

module.exports = function (ID, db, docDB) {
	
	var Helpers = require('./Helpers')(ID, db);

	 function* remove(key, node, level, nextVersion) {
		let hashKey = utils.genHash(key); 	
		let HID = Helpers.computeHID(hashKey, level);

		if (node.contains(HID)) {

			let pointer = node.getPointer(HID);
			if (typeof(pointer) === 'number' || typeof(pointer) === 'string') {
				pointer = yield Helpers.get(pointer);
				// console.assert(pointer.HID === HID);
				node.setPointer(HID, pointer);
			}

			if (pointer instanceof Package) {
				// The package has been found!
				if (pointer.key == key) {
					// Remove the random bit string from the version to create the next version
					nextVersion = utils.zobristHash(nextVersion, pointer.bitString);

					// Remove the reference to the package
					node.remove(HID);
					if (node.arrayOfPointers.length === 1) {
						pointer = node.arrayOfPointers[0];
						if (typeof(pointer) === 'number' || typeof(pointer) === 'string')
							pointer = yield Helpers.get(pointer);

						if (pointer instanceof Package) {
							return [nextVersion, pointer];
						} else {
							return [nextVersion, null];
						}

					} else {
						return [nextVersion, null];
					}

				} else {
					throw new Error('(1) Remove: Key "'+key+'" not found.');
				}
			} else {

				let temp = yield remove(key, pointer, level - Helpers.BITS, nextVersion);
				nextVersion = temp[0];
				pointer = temp[1];
				
				if (pointer) {
					// Collapse structure if necessary
					node.remove(HID);
					if (node.arrayOfPointers.length === 0) {
						
						return [nextVersion, pointer];

					} else {
						let pointerHashKey = utils.genHash(pointer.key);
						let pointerHID = (pointerHashKey >> level) & Helpers.MASK;

						if (node.contains(pointerHID)) {

							// Create a new diverging branch
							var pointer2 = node.getPointer(pointerHID);
							if (typeof(pointer2) === 'number' || typeof(pointer2) === 'string') pointer2 = yield Helpers.get(pointer2);
							var pointerHashKey2 = utils.genHash(pointer2.key);

							node.addPointer(pointerHID, new Node(pointerHID));
							node = node.getPointer(pointerHID);
							var k1, k2;
							level -= Helpers.BITS;
							
							// Add the new value and the tempPointer to the array
							for (; level >= 0; level -= Helpers.BITS) {
								k1 = Helpers.computeHID(pointerHashKey, level);
								k2 = Helpers.computeHID(pointerHashKey2, level);
								if (k1 != k2) break;
								node.addPointer(k1, new Node(k1));
								node = node.getPointer(k1);	
							}
							if (!pointer.id) pointer.HID = k1;
							node.addPointer(k1, pointer.id || pointer);
							if (!pointer.id) pointer.HID = k2;
							node.addPointer(k2, pointer2.id|| pointer2);
							return [nextVersion, null];

						} else {

							if (!pointer.id) pointer.HID = pointerHID;
							node.addPointer(pointerHID, pointer.id || pointer);
							return [nextVersion, null];

						}
					}
				} else {
					return [nextVersion, null];
				}
			}
		} else {
			throw new Error('(2) Remove: Key "'+key+'" not found.');
		}
	} // End Function

	return {
		exec: remove
	};
};