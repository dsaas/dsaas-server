var Node = require('./Node'),
	read = require('co-read'),
	Package = require('./Package');

module.exports = function (ID, db) {
	'use strict';

	var BITS = 5, 
		LENGTH = 6, 
		WIDTH = 1 << BITS,
		SHIFT = BITS * LENGTH,
		MASK = WIDTH - 1,
		MAX_COLLISIONS = 100;

	return {
		BITS: BITS,
		LENGTH: LENGTH,
		WIDTH: WIDTH,
		SHIFT: SHIFT,
		MASK: MASK,
		MAX_COLLISIONS: MAX_COLLISIONS,

		computeHID: function (hash, level) {
			if (level === undefined) level = SHIFT;
			return (hash >> level) & MASK;
		},

		get: function* (k) {
			var pointer = yield db.getJSON(db.__(ID, db.constant.trie, k));
			if (pointer.type === 'Node') return new Node(pointer.HID, pointer);
			if (pointer.type === 'Package') return new Package(pointer);
		},

		getWithHash: function* (key, value, hashValue) {
			try {
				// Profiler.collect('start with hash');
				var hash = Package.hash(key, value);
				var data, stream = db.createReadStream({
					gte: db.__(ID, db.constant.trie, hash, 0),
					lte: db.__(ID, db.constant.trie, hash, MAX_COLLISIONS)
				});
				while (data = yield read(stream)) {
					let p = JSON.parse(data.value);
					if (p && p.key == key) {
						// Profiler.collect('end with hash');
						if (hashValue) return data.key.substring(data.key.indexOf(db.constant.trie) + db.constant.trie.length + 1);
						return new Package(p);
					}
				}
			} catch (e) {
				console.log('Get With Hash: ', e);
			}
			// Profiler.collect('done with hash');
			return null;
		},

		getWithHash2: function* (key, value, hashValue) {
			try {
				var hash = Package.hash(key, value);
				var changeNumber = 0;
				var p = JSON.parse(yield db.get(db.__(ID, db.constant.trie, hash, changeNumber)));
				while (!p || p.key != key || changeNumber < MAX_COLLISIONS) {
					p = JSON.parse(yield db.get(db.__(ID, db.constant.trie, hash, changeNumber++)));
				}
				if (p && p.key == key) {
					if (hashValue) return data.key.substring(data.key.indexOf(db.constant.trie) + db.constant.trie.length + 1);
					return new Package(p);
				}
			} catch (e) {
				console.log('Get With Hash: ', e);
			}
			return null;
		},

		genHashDBKey: function* (packageContainer) {
			var hash = Package.hash(packageContainer.key, packageContainer.value);
			var numCollision = 0;
			var p;
			try {
				var dbKey = db.__(ID, db.constant.trie, hash, numCollision);
				p = yield db.getJSON(dbKey);
			} catch (e) {}

			if (p) {
				numCollision++;
				if (p.key == packageContainer.key && p.value == packageContainer.value) 
					throw new Error('genHashDBKey error, package exists');					
			}
			return db.__(hash, numCollision);
		},

		getRoot: function* (version) {
			if (version === 0) {
				version = ROOT_VERSION;
			}
			var pointer;
			try {
				pointer = yield db.getJSON(db.__(ID, db.constant.root, version));
			} catch (e) {
				return {error: true, message: 'Version does not exist', name: 'WrongVersion'};
			}
			return new Node(0, pointer);
		},

		next: function* (el) {
			// Get lock
			// Get nodeId/version
			var obj = yield db.get(db.__(ID, db.constant[el]));
			// Update nodeId/version
			obj++;
			yield db.put(db.__(ID, db.constant[el]), obj);
			// Release lock
			// Return nodeId/version
			return obj;
		},

		dfsPrint: function(rootNode) {
			console.log('>', rootNode);
			for (var node of rootNode.arrayOfPointers) {
				if (typeof(node) != 'number')
					dfsPrint(node);
			}
		}
	};
};