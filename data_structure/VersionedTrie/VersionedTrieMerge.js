'use strict';

var utils = require('../utils'),
	Node = require('./Node'),
	Package = require('./Package');

module.exports = function (ID, db, docDB) {
	
	var Helpers = require('./Helpers')(ID, db);

	function* exec(nodeK, nodeA, nodeB, nodeN, level, nextVersion) {
		// The identifiers of the nodes differ
		// which means that somewhere along one of their branches they differ.
		// Compare their nodes!
		for (var i = 0; i < Helpers.WIDTH; i++) {
			// The pointer is in A but not in B
			if (nodeA.contains(i) && !nodeB.contains(i) && 
				// The pointer is not in nodeK
				(!nodeK || !nodeK.contains || !nodeK.contains(i) 
					// The pointer is different from nodeK if it is in nodeK
					|| nodeK.getPointer(i) != nodeA.getPointer(i))) {

				// Add everything in A to B that is not in B
				nextVersion = yield computeNextVersion(nodeA.getPointer(i), nextVersion, 'a');
				nodeN.addPointer(i, nodeA.getPointer(i));

			} else if (!nodeA.contains(i) && nodeB.contains(i)) {
				// It exists in nodeB but not in nodeA.
				// Was it added while building nodeB or was it removed from nodeK?
				if (nodeK instanceof Node && nodeK.contains(i) && nodeK.getPointer(i) === nodeB.getPointer(i)) {
					// It was removed from nodeK
					// So remove it from nodeB
					nextVersion = yield computeNextVersion(nodeN.getPointer(i), nextVersion, 'b');
					nodeN.remove(i);
				}
			} else if (nodeA.contains(i) && nodeB.contains(i)) {
				// Both bitmaps are the same.
				// Check if both pointers are the same.
				if (nodeA.getPointer(i) === nodeB.getPointer(i)) {
					// They are the same, so it exists in A and B, we can move on...
					continue;
				} else {
					// They are not the same
					// Check with the common root to see which one is the same as the common root
					if (nodeK instanceof Node && nodeK.getPointer(i) === nodeB.getPointer(i)) {
						// Node B is the same, node A differs, so changes are in node A
						nextVersion = yield computeNextVersion(nodeA.getPointer(i), nextVersion);
						nodeN.addPointer(i, nodeA.getPointer(i));
					} else if (nodeK instanceof Node && nodeK.getPointer(i) === nodeA.getPointer(i)) {
						// Node A is the same, node B differs, so changes are in node B
						// Because we are adding changes in nodeA to nodeB to create nodeC, we don't have
						// to do anything here.
						// The changes are already in node B.
						continue;
					} else {
						// The pointer at nodeK is not the same as either.  
						// This is a conflict.
						// If both pointers are nodes, then we will have to explore the nodes.
						
						var pointer = yield {
							A: Helpers.get(nodeA.getPointer(i)),
							B: Helpers.get(nodeB.getPointer(i)),
							N: Helpers.get(nodeN.getPointer(i)),
							K: null
						};
						
						nodeN.addPointer(i, pointer.N);

						if (nodeK instanceof Node && nodeK.getPointer(i)) {
							// We also have to descend the common ancestor tree...
							pointer.K = yield Helpers.get(nodeK.getPointer(i));
						}

						if (pointer.A instanceof Node && pointer.B instanceof Node) {
							
							nextVersion = yield exec(pointer.K, pointer.A, pointer.B, pointer.N, level-Helpers.BITS, nextVersion);

						} else if (pointer.A instanceof Package && pointer.B instanceof Package) {
							// If both are Packages then the conflict resolution is used.
							// The conflict resolution is that everything from A must override B.
							if (pointer.A.id !== pointer.B.id) {
								// Does the pointer exist in nodeK? and is the pointer equivalent to A or B?
								if (!pointer.K || (pointer.K.id !== pointer.A.id && pointer.K.id !== pointer.B.id)) {
									// The pointer is not in nodeK, so it has been added in both A and B.
									nodeN.addPointer(i, new Node(i));

									nextVersion = yield computeNextVersion(pointer.A, nextVersion);
									mergeValueAndValue(pointer.A, pointer.B, nodeN.getPointer(i), level-Helpers.BITS);

								} else if (pointer.K.id == pointer.A.id) {
									// It is equivalent to A, but has been changed in B.
									continue;
								} else if (pointer.K.id == pointer.B.id) {
									// It is equivalent to B, but has been changed in A.
									nextVersion = yield computeNextVersion(pointer.A, nextVersion);
									nodeN.addPointer(i, pointer.A.id);
								} else {
									// This else is never supposed to happen.
									console.log('Merging two versions of the same Trie, an illegal state');
									console.assert(false);
								}
							}
						} else {
							// If one is a Package and the other is a Node, then explore the Node and find
							// a Node where the package can be effectively added.
							var tempNode, tempPackage;
							if (pointer.A instanceof Node) {
								nodeN.addPointer(i, new Node(i));
								tempNode = pointer.A;
								tempPackage = pointer.B;
							} else {
								tempNode = pointer.B;
								tempPackage = pointer.A;
							}

							nextVersion = yield mergeBranchAndValue(
								pointer.K, 
								tempNode, 
								tempPackage, 
								nodeN.getPointer(i), 
								tempNode == pointer.A, 
								level - Helpers.BITS, 
								nextVersion);

							nextVersion = yield computeNextVersion(pointer.A, nextVersion);
						} // End If
					} // End Same Check
				}
			} // End Bitmap check
		} // End For
		return nextVersion;
	} // End Function

	function* mergeBranchAndValue(nodeK, tempNode, tempPackage, nodeN, useConflictResolution, level, nextVersion) {
		var hash = utils.genHash(tempPackage.key);

		// Go down the levels to merge the branch and value
		for (; level >= 0; level -= Helpers.BITS) {

			var HID = Helpers.computeHID(hash, level);
			
			if (!tempNode.contains(HID)) {
				// tempNode does not contain anything at this level with an identifier equivalent to HID,
				// so we may add it...
				nodeN.addPointer(HID, tempPackage.id);

				if (nodeK) {
					// nodeK exists and nodeN may contain unnecessary elements, so..
					// Remove any packages that are in nodeK and nodeN.
					let v = yield removePackages(nodeK, nodeN);
					nextVersion = combineVersions(nextVersion, v);
				}

				return nextVersion;
			} else {
				// tempNode contains a similar element with the same HID.
				// Let's retrieve that pointer...
				var pointer = yield Helpers.get(tempNode.getPointer(HID));
				// Check what the pointer points to...
				if (pointer instanceof Package) {
					if (pointer.key == tempPackage.key) {
						if (useConflictResolution) {
							nextVersion = yield computeNextVersion(tempPackage, nextVersion, 'g');
						} else {
							nextVersion = yield computeNextVersion(pointer, nextVersion, 'f');
						}
					}
					// A package is at this level, use the conflict resolution.
					// That is, add whatever is derived from NodeA
					if (useConflictResolution) {
						// We must add the temp node...
						nodeN.addPointer(HID, pointer.id);
					} else {
						// Otherwise we must add the package...
						nodeN.addPointer(HID, tempPackage.id);
						tempPackage.HID = HID;
					}
					return nextVersion;
				} else {
					// The pointer is a Node, so we will have to go deeper into the Trie
					nodeN.addPointer(HID, pointer);
					tempNode = pointer;
					// Remove any unnecessary packages
					if (useConflictResolution && nodeK) {
						let v = yield removePackages(nodeK, nodeN);
						nextVersion = combineVersions(nextVersion, v);
					}
					// Go deeper with nodeN
					nodeN = nodeN.getPointer(HID);
				}
			}
		}
		console.log('Hit rock bottom');
		// The bottom nodes will always have values.
	}

	function mergeValueAndValue(pcA, pcB, nodeNew, level) {
		// Generate the hash values
		var hashA = utils.genHash(pcA.key);
		var hashB = utils.genHash(pcB.key);
		var kA, kB;

		// Find a level where their bits differ -- they will differ.
		for (; level >= 0; level -= Helpers.BITS) {
			kA = Helpers.computeHID(hashA, level);
			kB = Helpers.computeHID(hashB, level);
			if (kA != kB) break;
			nodeNew.addPointer(kA, new Node(kA));
			nodeNew = nodeNew.getPointer(kA);
		}

		// Add the final values to the node
		pcA.HID = kA;
		pcB.HID = kB;
		nodeNew.addPointer(kA, pcA.id);
		nodeNew.addPointer(kB, pcB.id);
	}

	function* removePackages(nodeK, nodeN) {
		if (!nodeK) return;
		var nextVersion = '';
		for (let i = 0; i < Helpers.WIDTH; i++) {
			if (nodeK.getPointer(i) == nodeN.getPointer(i)) {
				nextVersion = yield computeNextVersion(nodeN.getPointer(i), nextVersion, 'remove');
				nodeN.remove(i);
			}
		}
		return nextVersion;
	}

	function combineVersions(versionA, versionB) {
		if (versionA == '') return versionB;
		if (versionB == '') return versionA;
		return utils.zobristHash(versionA, versionB);
	}

	function* CNV(node, nextVersion) {
		// All the elements in the node must either be added or removed
		// Either way it is the same operation
		if (node instanceof Package) {
			// console.log(p.key, p.value);
			return combineVersions(nextVersion, node.bitString);
		} else if (node instanceof Node) {
			for (let p of node.arrayOfPointers) {
				nextVersion = combineVersions(nextVersion, yield CNV(p, ''));
			}
			return nextVersion;
		} else {
			// Node is a number
			nextVersion = yield CNV(yield Helpers.get(node), nextVersion);
			return nextVersion;
		}
	}

	function* computeNextVersion(node, nextVersion, what) {
		let cnv = yield CNV(node, nextVersion);
		// console.log(nextVersion, what);
		return cnv;
	}

	return {
		exec: exec
	};
};