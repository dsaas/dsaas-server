'use strict';

var hash = require('object-hash');

function Package (HID, key, value, bitString, id) {
	if (HID === null || HID === undefined) throw Error('The hashed identifier (HID) of this node must be set');
	if (typeof(HID) === 'number') {
		/* Note that the HID is used when inserting.
		It will become unreliable when merges occur */
		this.HID = HID;
		this.key = key;
		this.value = value;
		this.bitString = bitString;
		this.id = id;
	} else {
		var packageContainer = HID;
		this.HID   = packageContainer.HID;
		this.key   = packageContainer.key;
		this.value = packageContainer.value;
		this.bitString = packageContainer.bitString;
		this.id = packageContainer.id;
	}
}

Package.hash = function (key, value) {
	let obj = {};
	obj[key] = value;
	return hash(obj);
}

Package.prototype.equals = function (other) {
	return this.key == other.key && this.value == other.value;
};

Package.prototype.toString = function () { return this.id; };
Package.prototype.type = function () { return 'Package'; };
Package.prototype.toJSON = function () {
	return {
		HID: this.HID,
		id: this.id,
		value: this.value,
		key: this.key,
		bitString: this.bitString,
		type: 'Package'
	};
};

module.exports = Package;