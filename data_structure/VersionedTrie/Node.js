'use strict';

var popcount = require('../utils').popcount;

// function arrayShallowCopy(arr) {
// 	var narr = [];
// 	for (var HID in arr) {
// 		narr.push(arr[HID]);
// 	}
// 	return narr;
// }

function Node (HID, node) {
	if (typeof(HID) !== 'number') throw Error('The hashed identifier (HID) of this node must be set');
	if (node) {
		this.id = node.id;
		this.HID = HID;
		this.bitmap = node.bitmap;
		this.arrayOfPointers = node.arrayOfPointers;
		// this.arrayOfPointers = arrayShallowCopy(node.arrayOfPointers);
	} else {
		this.id = 0;
		this.HID = HID;
		this.bitmap = 0;
		this.arrayOfPointers = [];
	}
}

function getPosition(HID, node) {
	// Get the position by counting the number of set bits.
	if (HID === 0) return 0;
	var bitstring = (-1 >>> (32 - HID)) & node.bitmap;
	var pos = popcount(bitstring);
	return pos;
}

Node.prototype.getPointer = function (HID) {
	// Get the element in the array
	return this.arrayOfPointers[getPosition(HID, this)];
};

Node.prototype.addPointer = function (HID, value) {
	if (this.bitmap >> HID & 1) {
		// Already set, so replace
		this.arrayOfPointers[getPosition(HID, this)] = value;
	} else {
		// Set the bit in the bitmap
		this.bitmap |= (1 << HID);
		// Add the element to the array
		this.arrayOfPointers.splice(getPosition(HID, this), 0, value);
	}
};

Node.prototype.setPointer = function (HID, value) {
	// Set the position in the array
	this.arrayOfPointers[getPosition(HID, this)] = value;
};

Node.prototype.contains = function (HID) {
	// Check if the position is set in the bitmap array
	return this.bitmap >> HID & 1;
};

Node.prototype.remove = function (HID) {
	this.arrayOfPointers.splice(getPosition(HID, this), 1);
	this.bitmap &= ~(1 << HID);
};

Node.prototype.toJSON = function () {
	return {
		id: this.id,
		HID: this.HID,
		bitmap: this.bitmap,
		arrayOfPointers: this.arrayOfPointers,
		type: 'Node'
	};
};

Node.prototype.toString = function () { return this.id; };
Node.prototype.type = function () { return 'Node'; };

module.exports = Node;