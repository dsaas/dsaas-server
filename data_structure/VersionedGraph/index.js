'use strict';

var VersionedTrie = require('../VersionedTrie'),
	_ = require('underscore');

var STR = {
	NODE: 'node',
	EDGE: 'edge',
	EDGE_SEPARATOR: '-->'
};

var FROM_TO = ['from', 'to'];

function VersionedGraph(ID, db, trie, versionHistoryID) {

	if (!trie || trie.type !== 'VersionedTrie') {
		trie = VersionedTrie(ID, db, versionHistoryID);
	}

	return {
		create: function *() {
			yield [
				trie.create(),
				db.put(db.__(ID, db.constant.graphNodeId), 0),
				db.put(db.__(ID, db.constant.graphEdgeId), 0)
			];
		},

		/**
		 * Add a batch of nodes with the given properties.
		 * @param {Object} nodesProperties Map NodeIds -> node properties, the properties are Objects (JSON).
		 * @param {Number} version         The version of the data structure.
		 * @yield {Object} {version: $new_version, nodeIds: nodeIds}
		 */
		addNodes: function *(nodesProperties, version, nodeIds) {
			var nodeId = yield db.get(db.__(ID, db.constant.graphNodeId));
			var obj = {};
			let count = 0, generateIds = false;

			if (nodeIds === undefined) {
				nodeIds = [];
				generateIds = true;
			}

			for (let prop of nodesProperties) {
				let usingId;
				if (generateIds) {
					nodeId++;
					nodeIds.push(nodeId);
					usingId = nodeId;
				} else {
					usingId = nodeIds[count++];
				}

				obj[STR.NODE + usingId] = {
					id: usingId,
					edges: {},
					props: prop
				};
			}


			if (generateIds) {
				yield db.put(db.__(ID, db.constant.graphNodeId), nodeId);
			}
			
			return {
				version: yield trie.put(obj, version), 
				nodeIds: nodeIds
			};
		},

		/**
		 * Batch updates the properties of the nodes
		 * @param {Array} nodeProperties Array of node properties, the properties are Objects (JSON).
		 * @param {Number} version       The version of the data structure.
		 * @yield {Object} {version: $version}
		 */
		updateNodes: function *(nodeProperties, version) {
			var obj = {};
			for (let nodeId in nodeProperties) {
				obj[STR.NODE + nodeId] = yield trie.get(STR.NODE + nodeId, version);
				if (!obj[STR.NODE + nodeId]) {
					throw new Error('Identifier "' + nodeId + '" does not exist.');
				}
				obj[STR.NODE + nodeId].props = nodeProperties[nodeId];
			}

			return yield trie.put(obj, version);
		},

		/**
		 * Retrieves the node
		 * @param {Number} nodeId        The node identifier
		 * @param {Number} version       The version of the data structure
		 * @yield {Object} {id: $node_id, edges: $edge_set, properties: $props}
		 */
		getNode: function *(nodeId, version) {
			return yield trie.get(STR.NODE + nodeId, version);
		},

		/**
		 * Removes a batch of nodes by identifiers
		 * @param {Array} nodeIds        Node identifiers
		 * @param {Number} version       The version of the data structure
		 * @yield {Object} {version: $version}
		 */
		removeNodes: function *(nodeIds, version) {
			var ids = {}, obj = {}, message = 'removeNodes ';

			for (let id of nodeIds) {
				ids[STR.NODE + id] = 1;
				message += id + ' ';
			}

			for (let id of nodeIds) {
				let node = yield trie.get(STR.NODE + id, version);
				for (let edgeId in node.edges) {
					if (!ids[STR.EDGE + edgeId]) {
						ids[STR.EDGE + edgeId] = 1;
						let edge = yield trie.get(STR.EDGE + edgeId, version);
						let updateNode = null;
						if (!ids[STR.NODE + edge.from]) {
							updateNode = obj[STR.NODE + edge.from];
							if (!updateNode)
								updateNode = yield trie.get(STR.NODE + edge.from, version);
						} else if (!ids[STR.NODE + edge.to]) {
							updateNode = obj[STR.NODE + edge.to];
							if (!updateNode)
								updateNode = yield trie.get(STR.NODE + edge.to, version);
						}

						if (updateNode) {
							delete updateNode.edges[edgeId];
							obj[STR.NODE + updateNode.id] = updateNode;
						}
					}
				}
			}

			// TODO: Change the function to something else instead of 'put' -> may be confusing
			var rootNode = yield trie.put(obj, version, true);
			return yield trie.remove(Object.keys(ids), version, false, rootNode);
		},

		/**
		 * Create edges between nodes.
		 * @param {Array} edges         The array of edges
		 * @param {Number} version       The version number
		 * @yield {Object} {version: $version, edgeIds: [edgeIds...]}
		 */
		addEdges: function *(edges, version) {
			var obj = {}, edgeIds = [];

			for (let edge of edges) {
				// Add the edge
				let edgeId = edge.from + STR.EDGE_SEPARATOR + edge.to;
				edge.id = edgeId;
				obj[STR.EDGE + edgeId] = edge;
				edgeIds.push(edgeId);

				// Add the identifier of the edge to the start and end node
				for (let n of FROM_TO) {
					if (!edge[n]) throw new Error('Edge "'+edge+'" does not contain the '+n+' node.');
					if (!obj[STR.NODE + edge[n]]) {
						obj[STR.NODE + edge[n]] = yield trie.get(STR.NODE + edge[n], version);
						if (!obj[STR.NODE + edge[n]]) throw new Error('Node "'+edge[n]+'" does not exist in version "'+version+'"');
					}
					obj[STR.NODE + edge[n]].edges[edgeId] = 1;
				}
			}

			let nextVersion = yield trie.put(obj, version);
			return {version: nextVersion, edgeIds: edgeIds};
		},

		/**
		 * Updates the edges
		 * @param {Object} edgeProperties Map of EdgeIds->Objects (JSON)
		 * @param {Number} version        The data structure version number
		 * @yield {Version} {version: $version}
		 */
		updateEdges: function *(edgeProperties, version) {
			var obj = {};
			for (let edgeId in edgeProperties) {
				let temp = yield trie.get(STR.EDGE + edgeId, version);
				let newEdge = {};

				for (let k in edgeProperties[edgeId])
					newEdge[k] = edgeProperties[edgeId][k];

				newEdge.from = temp.from;
				newEdge.to = temp.to;
				newEdge.id = temp.id;

				obj[STR.EDGE + edgeId] = newEdge;
			}
			return yield trie.put(obj, version);
		},

		/**
		 * Retrieve an edge
		 * @param {Number} edgeId        Edge identifier
		 * @param {Number} version       Version of the data structure
		 * @yield {Object} {version: $version}
		 */
		getEdge: function *(edgeId, version) {
			return yield trie.get(STR.EDGE + edgeId, version);
		},

		/**
		 * Removes a batch of edges
		 * @param {Array} edgeIds       Array of edge identifiers
		 * @param {Number} version       The version of the data structure
		 * @yield {Object} {version: $version}
		 */
		removeEdges: function *(edgeIds, version) {
			var obj = {}, ids = [];
			for (let id of edgeIds) {
				let edge = yield trie.get(STR.EDGE + id, version);
				if (!edge) throw new Error('Edge identifier "' + id + '" does not exist.');
				// Remove the edge identifiers from the nodes
				for (let n of FROM_TO) {
					if (!obj[STR.NODE + edge[n]]) obj[STR.NODE + edge[n]] = yield trie.get(STR.NODE + edge[n], version);
					delete obj[STR.NODE + edge[n]].edges[edge.id];
				}
				ids.push(STR.EDGE + id);
			}

			var rootNode = yield trie.put(obj, version, true);
			return yield trie.remove(ids, version, false, rootNode);
		},

		allEdges: function *(version) {
			return yield trie.keysOrValues(version, 
				function filter(str) { return str.indexOf(STR.EDGE) === 0; },
				function edit(str) { return str.substring(STR.EDGE.length); });
		},

		allFullEdges: function *(version) {
			return yield trie.keysOrValues(version,
				function filter(str) { return str.indexOf(STR.EDGE) === 0; },
				function edit(str) { return str.substring(STR.EDGE.length); },
				false, true);
		},

		allNodes: function *(version) {
			return yield trie.keysOrValues(version,
				function filter(str) { return str.indexOf(STR.NODE) === 0; },
				function edit(str) { return str.substring(STR.NODE.length); }, false, true);
		},

		allNodeIds: function * (version) {
			return yield trie.keysOrValues(version, 
				function filter(str) { return str.indexOf(STR.NODE) === 0; },
				function edit(str) { return str.substring(STR.NODE.length); });
		},

		merge: function *(versionA, versionB) {
			return yield trie.merge(versionA, versionB);
		},

		adjacent: function *(nodeId, version) {
			var node = yield trie.get(STR.NODE + nodeId, version);
			var nodes = {};
			for (let edgeId in node.edges) {
				let edge = yield trie.get(STR.EDGE + edgeId, version);
				let adjNode = edge.to == nodeId ? edge.from : edge.to;
				adjNode = yield trie.get(STR.NODE + adjNode, version);
				nodes[adjNode.id] = adjNode;
			}
			return nodes;
		},

		parents: function *(nodeId, version) {
			var node = yield trie.get(STR.NODE + nodeId, version);
			var parents = {};
			for (let edgeId in node.edges) {
				let edge = yield trie.get(STR.EDGE + edgeId, version);
				if (edge.to == nodeId) {
					let parentNode = yield trie.get(STR.NODE + edge.from, version);
					parents[parentNode.id] = parentNode;
				}
			}
			return parents;
		},

		children: function *(nodeId, version) {
			var node = yield trie.get(STR.NODE + nodeId, version);
			var children = {};
			for (let edgeId in node.edges) {
				let edge = yield trie.get(STR.EDGE + edgeId, version);
				if (edge.from == nodeId) {
					let childNode = yield trie.get(STR.NODE + edge.to, version);
					children[childNode.id] = childNode;
				}
			}
			return children;
		}

	};
}


VersionedGraph.STR = STR;
VersionedGraph.FROM_TO = FROM_TO;
module.exports = VersionedGraph;