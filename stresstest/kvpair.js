// var dat = require('dat-core');
var trie = require('../data_structure/VersionedTrie');
var co = require('co');
var leveldb = require('../config/leveldb')('./test');
var profiler = require('../data_structure/profiler');
var utils = require('../data_structure/utils');
var util = require('util');

// var sub = require('subleveldown');
// var db = dat(leveldb);
var db = trie('test1', leveldb, 'test1');
co(function * () {

	try {
		yield db.create();
	} catch (e){}
	var max = Number(process.argv[2]);
	var version = {0: 0};
	var randomStrings = [];
	for (var i = 0; i < max+1; i++) {
		randomStrings.push(['kaas', utils.randomBitString(16)]);
		// randomStrings.push(i);
	}
	
	var start = process.hrtime();
	for (i = 1; i < max+1; i++) {
		var obj = {};
		obj[randomStrings[i][0]] = randomStrings[i][1];
		version[i] = yield db.put(obj, version[i-1]);
	}

	console.log("PUT: ", process.hrtime(start) );
	start = process.hrtime();
	for (i = 1; i < max+1; i++) {
		var el = yield db.get(randomStrings[i][0], version[i]);
		console.assert(el == randomStrings[i][1]);
	}
	console.log("GET: ", process.hrtime(start));
	return 'done';
}).then(function (done) {
	leveldb._db.db.approximateSize('\0', '~', function (err, size) {
		if (err) return console.error('Ooops!', err)
		console.log('ApproxSize: %d', size)
	});
}).catch( function (err) {
	console.log(err.stack);
});