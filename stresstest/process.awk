BEGIN {
	i = 0;
	j = 0;
	num = 0;
	skip = 1
}
{
	if ($1 ~ /^[0-9]+$/) {
		gatherInfo();
	}

	if ($1 == "PUT:") {
		put[i] = substr($3, 0, length($3) - 1);
		put[i] += $4 * 10e-10;
		i++;
	}
	if ($1 == "GET:") {
		get[j] = substr($3, 0, length($3) - 1);
		get[j] += $4 * 10e-10;
		j++;
	}
}
END {
	gatherInfo();
	printInfo();
}

function gatherInfo() {
	if (skip) {
		skip = 0;
		next;
	}
	s = 0;
	for (p in put) {
		s += put[p];
	}
	allPut[num] = s / i;
	# print "Put: " s / i;

	s = 0;
	for (g in get) {
		s += get[g];
	}
	allGet[num] = s / j
	# print "Get: " s / j;
	i = 0;
	j = 0;
	num++;
	delete put;
	delete get;
}

function printInfo() {
	print "Put"
	for (p in allPut) {
		print allPut[p]
	}

	print "Get"
	for (g in allGet) {
		print allGet[g]
	}
}