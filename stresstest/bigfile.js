var fs = require('fs'),
	readline = require('readline'),
	stream = require('stream');

var instream = fs.createReadStream('checkbook\ 2014.csv'),
	outstream = new stream,
	rl = readline.createInterface(instream, outstream);

var request = require('superagent');

var count = 0;
var buffer = [];
var version = 0;

rl.on('line', function (line) {
	data = line.split(',');
	count++;
	
	buffer.push({
		key: data[0],
		payee: data[1],
		city: data[2],
		project: data[12],
		year: data[8],
		amount: data[9]
	});

	if (count < 21) {
		if (count % 10) {
			rl.pause();
			store(buffer);
		}
	}
});

function store(buffer) {
	console.log(version);
	if (buffer.length == 0) {
		rl.resume();
	} else {
		var element = buffer.shift();
		var key = element.key;
		delete element.key;

		// console.log(key, element);

		request.post('/api/pierre/Example1/'+version+'/map/'+key)
		.send(element).end(function (err, res) {
			if (err) return console.log(err);
			version = res.body.version;
			store(buffer);
		});
	}
}

rl.on('close', function () {
	console.log('done');
	console.log(count);
});
