var co = require('co');

function * run () {
	var i = 0;
	while (true) {
		yield {el: i};
		i++;
	}
}

var fn = co.wrap(function * () {
	var gen = run();
	return yield gen.next();
});

fn().then(function (val) {
	console.log(val);
})
.catch(function (err) {
	console.log(err);
});