set for [i=1:8] linetype i linewidth 4

set title "Random Key-Value Pair Insertion (No Checkouts)"
set xlabel "Number of Random Key-Value Pair Insertions"
set ylabel "Running Time (seconds)"
set terminal postscript eps enhanced color "Helvetica,24"
set output 'samver.eps'
input_file="samver.txt"
#set log x
#set log y
set xrange [2000:16000]
#set yrange [0.1:200]

#set style func linespoints
set style line 1 linecolor rgb "cyan"
set style line 2 linecolor rgb "green"
set style line 3 linecolor rgb "red"
set style line 4 linecolor rgb "pink"
set key left

f(x) = a*x + b
fit f(x) input_file using 1:2 via a,b
g(x) = c*x + d
fit g(x) input_file using 1:3 via c,d
j(x) = e*x + f
fit j(x) input_file using 1:4 via e,f
h(x) = g*x + h
fit h(x) input_file using 1:5 via g,h

plot f(x) title "DSaaS Get", input_file using 1:2  with points notitle, \
	 g(x) title "Dat Get", input_file using 1:3 with points notitle, \
	 j(x) title "DSaaS Put", input_file using 1:4 with points notitle, \
	 h(x) title "Dat Put", input_file using 1:5 with points notitle