set for [i=1:8] linetype i linewidth 4

set title "Versioned Trie vs Dat-Core"
set xlabel "Number of Iterations"
set ylabel "Running Time (seconds)"
set terminal postscript eps enhanced color font 'Helvetica,20'
set output 'samever_graph.eps'
input_file="samver.txt"
#set log x
#set log y
set xrange [2000:16000]
#set yrange [0.1:200]

#set style func linespoints
set style line 1 linecolor rgb "cyan"
set style line 2 linecolor rgb "green"
set style line 3 linecolor rgb "red"
set style line 4 linecolor rgb "pink"
set key left


plot input_file using 1:2 title "DSaaS Get" with lines, \
	 input_file using 1:3 title "Dat Get" with lines, \
	 input_file using 1:4 title "DSaaS Put" with lines, \
	 input_file using 1:5 title "Dat Put" with lines