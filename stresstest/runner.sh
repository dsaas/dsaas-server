#! /bin/sh
input=kvpair.js
output=new_results/same_key_diff_versions.txt

rm $output
for i in 1000 2000 4000 8000 16000
do
	echo $i
	echo $i >> $output
	for times in 0 .. 6
	do
		node --harmony $input $i >> $output
		du -sh ./test/ >> $output
		rm ./test/*
	done
done