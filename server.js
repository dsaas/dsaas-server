var app = require('express')(),
	server = require('http').Server(app),
	mongoose = require('mongoose'),
	io = require('socket.io')(server);


// Error.stackTraceLimit = Infinity;
server.listen(3000, function () {	
	// Note that this must run on 0.0.0.0:3000 in docker to work
	console.log('DSaaS server is listening at http://%s:%s', 
		server.address().address, 
		server.address().port
	);
});


process.env.MONGODB = process.env.MONGODB_PORT_27017_TCP_ADDR+'/test';
mongoose.connect('mongodb://' + process.env.MONGODB);
app.set('io', io);

// Set up express from configuration
require('./config/express')(app);

io.on('connection', function (socket) {
	console.log('connected');
	socket.emit('msg', {msg: 'Welcome to my domain.'});
});