var gulp = require('gulp'),
	nodemon = require('gulp-nodemon'),
	ngmin = require('gulp-ngmin'),
	changed = require('gulp-changed'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	jshint = require('gulp-jshint'),
	browserSync = require('browser-sync'),
	react = require('gulp-react'),
	stylus = require('gulp-stylus'),
	reload = browserSync.reload;


function errorLog(error) {
    console.log('******  Start of Error  ******');
    console.log(error.message);
    console.log('******  End of Error  ******');
    this.emit('end');
}


gulp.task('lint', function () {
	// gulp.src(['./**/*.js', '!./node_modules/**/*.js', '!./app/assets/lib/**/*.js', '!app/dist/*.js', '!./app/**/*.react.js'])
		// .pipe(jshint())
		// .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build', function () {
	gulp.src(['./app/*.js', './app/**/*.js', '!./app/assets/lib/**/*.js', '!./app/dist/**/*.js', '!./app/**/*.react.js'])
		// .pipe(ngmin())
		// .pipe(changed('./app/dist'))
		.pipe(concat('app.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./app/dist/'));
});

gulp.task('build-utils', function () {
	return gulp.src(['./app/modules/**/*.js', '!./app/modules/List/*', '!./app/modules/**/*.react.js'])
		.pipe(concat('utils.min.js'))
		// .pipe(uglify())
		.pipe(gulp.dest('./app/dist/'));
});

gulp.task('build-jsx', function () {
	return gulp.src(['./app/modules/**/*.react.js', './app/modules/**/*.jsx'])
		.pipe(react())
		.on('error', errorLog)
		.pipe(concat('app-jsx.min.js'))
		// .pipe(uglify())
		.pipe(gulp.dest('./app/dist/'));
});

gulp.task('build-css', function () {
	return gulp.src(['./app/stylesheets/*.styl'])
		.pipe(stylus())
		.pipe(concat('main.css'))
		.pipe(gulp.dest('./app/dist'));
});

gulp.task('serve', function () {
	browserSync({
		server: {
			baseDir: 'app'
		}
	});

	gulp.watch(['./**/*.html', './**/*.js', './**/*.jsx', './**/*.styl'], {cwd: 'app'}, reload);
});

gulp.task('default', function () {
	nodemon({ 
		script: 'server.js', 
		ext: 'js', 
		ignore: ['./app/dist/*.js', 'app/dist/*'] 
	})
	.on('restart', function () { console.log('restarted...'); });
});