var client = require('redis').createClient();
var lock = require('redislock').createLock(client, {
	timeout: 10000,
	retries: 100,
	delay: 5
});

client.lock = lock;

module.exports = client;