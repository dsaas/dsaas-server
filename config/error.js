'use strict';

require('extend-error');

var ClientError = Error.extend('ClientError', 400);
var Module = {
	AUTH: ClientError.extend('Unauthorized', 401),
	API: ClientError.extend('APIError', 400),
	NOT_FOUND: ClientError.extend('Not Found', 404)
};

Module.handler = (err, req, res, next) => {
	console.log('Error: ', err.stack);
	// throw err;
	if (err instanceof ClientError)
		return res.status(err.code).json({error: err});
	else return res.status(500).json({error: err.message});
};

module.exports = Module;