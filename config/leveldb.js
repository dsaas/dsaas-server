var levelup = require('levelup');
var wrap = require('co-level');
var db = {};

// Export the database
module.exports = function (dblocation) {
	var _db;
	if (!dblocation) dblocation = 'db_data';
	if (!db.hasOwnProperty(dblocation)) {
		_db = levelup(dblocation);
		db[dblocation] = wrap(_db);
	}
	else return db[dblocation];

	db[dblocation]._db = _db;

	// Set the constants
	db[dblocation].constant = {
		// delimeter: '\0',
		delimeter: '.',
		metadata: 'meta',
		datastructures: 'datastructures',
		DAG: 'dag',
		trie: 'trie',
		node: 'n',
		package: 'p',
		nodeId: 'nodeId',
		packageId: 'packageId',
		version: 'version',
		root: 'root',
		graphNodeId: 'graphNodeId'
	};

	/**
	 * Build and return a properly delimeted string
	 * @return {String} Properly delimeted for database storage
	 */
	db[dblocation].__ = function __ () {
		var size = arguments.length;
		var str = "" + arguments[0];
		for (var i = 1; i < size; i++) {
			str += db[dblocation].constant.delimeter + arguments[i];
		}
		return str;
	};

	db[dblocation].putJSON = function* (key, obj) {
		return yield db[dblocation].put(key, JSON.stringify(obj));
	};

	db[dblocation].getJSON = function* (key) {
		return JSON.parse(yield db[dblocation].get(key));
	};

	db[dblocation]._getJSON = function (key, cb) {
		db[dblocation]._db.get(key, function (err, value) {
			cb(err, value && JSON.parse(value));
		});
	};

	db[dblocation]._putJSON = function (key, obj, cb) {
		db[dblocation]._db.put(key, JSON.stringify(obj), function (err) {
			cb(err);
		});
	};

	return db[dblocation];
};