var bodyParser = require('body-parser'),
	express = require('express'),
	jwt = require('jsonwebtoken'),
	error = require('./error');

var Schema = require('../schema'); // Instantiate schema's - Do not remove!!!
var JWTCollection = require('../schema/JWT');
module.exports = function (app) {

	// Set the appropriate headers and log information
	app.use(function (req, res, next) {
		var start = new Date();
		
		req.on('end', function () {
			console.log('total time: ', (new Date()).getTime() - start);
		});

		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		console.log(req.method + ' - ' + req.originalUrl);
		next();
	});

	// JWT Header Checking
	app.use(function (req, res, next) {
		// Refresh the JWT and extract user if necessary
		var token = req.get('Authorization') || req.query.authorization;
		if (token) {
			token = jwt.decode(token, {complete: true});
			
			if (!token) return next(error.AUTH('Authorization header is invalid.'));
			req.jwt = token.payload;

			JWTCollection.findById(req.jwt.id, (err, doc) => {
				if (err) return next(Error(err));
				if (!doc) return next(error.AUTH('JWT does not match'));
				if (req.jwt.key !== doc.key) return next(error.AUTH('JWT is invalid'));
				req.jwt.userId = doc.userId;
				next();
			});
		} else {
			next();
		}
	});

	// Middle-ware creation
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());


	// Setup all the routes
	require('../api')(app); 	// Route: /api

	// Setup redirection route
	app.get('/', function (req, res, next) {
		res.redirect('/api');
	});

	// Setup error handler
	app.use(error.handler);

};
