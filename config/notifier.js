var	events = require('events');

var emitters = {};
module.exports = function (emitterName) {
	if (!emitters[emitterName]) {
		emitters[emitterName] = new events.EventEmitter();
	}
	return emitters[emitterName];
};