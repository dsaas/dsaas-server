#! /bin/sh
# Starting the server (for local development purposes) with nodemon
MONGODB_PORT_27017_TCP_ADDR=localhost ./node_modules/nodemon/bin/nodemon.js --ignore node_modules --ignore db_data --ignore test --harmony server.js