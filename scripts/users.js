module.exports = {
	"public":  {
		"email": "public@example.com",
		"password": "public",
		"namespace": "public",
		"name": "anonymous"
	},
	"others": [
		{
			"email": "ashley@example.com",
			"password": "ashley",
			"namespace": "ashley",
			"name": "ashley"
		},
		{
			"email": "ashton@example.com",
			"password": "ashton",
			"namespace": "ashton",
			"name": "ashton"
		},
	]
};