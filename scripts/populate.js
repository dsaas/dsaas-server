#! /bin/node
// This script populates the database with data.
// Note that this is only for the mongodb database.
var mongoose = require('mongoose'),
	bcrypt = require('bcryptjs'),
	utils = require('../api/utils'),
	User = require('../schema/User');

var users = require('./users');
var i = 0;

process.env.MONGODB_TEST = process.env.MONGODB;

mongoose.connect('mongodb://' + process.env.MONGODB);
mongoose.connection.on('open', createPublicUser);

function createPublicUser(err) {
	if (err) return console.log(err.stack);
	createUser(users.public, function (err) {
		if (err) return console.log(err.stack);
		i = 0;
		createOtherUsers();
	});
}

function createOtherUsers() {
	createUser(users.others[i], function (err) {
		if (err) return console.log(err.stack);
		if (++i < users.others.length) createOtherUsers();
		else mongoose.connection.close(dbClosed);
	});
}

function createUser(user, cb) {
	bcrypt.hash(user.password, utils.salt, function (err, password) {
		if (err) console.log(err.stack);
		user.password = password;
		User.create(user, cb);
	});
}

function dbClosed(err) {
	console.log('Database closed.');
	if (err) console.log(err.stack);
}