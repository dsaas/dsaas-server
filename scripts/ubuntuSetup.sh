#! /bin/bash

check_package() {
	pkg=$1
	echo -n "Checking if $pkg is installed..."
	if dpkg --get-selections | grep -q "^$pkg[[:space:]]*install$"; then
		echo "yep"
		return -1
	elif ! type -P $pkg &> /dev/null; then
		echo "nope"
		echo "Install $pkg? (Yes/no) "
		read answer
		if echo "$answer" | grep -iq "^n"; then
			echo "Not installing, exiting script..."
			exit 1
		fi
	fi
}

install_package() {
	pkg=$1
	echo -n "Installing $pkg..."
	if apt-get -qq install $pkg; then
		echo "Success"
	else
		echo "Error installing $pkg"
		exit 1
	fi
}

# check root permissions
if [ $UID -ne 0 ] ; then
    echo "Please start the script as root or sudo!"
    exit 1
fi
echo "..."
echo "Note that pressing enter accepts the prompt in all cases."
echo "..."
echo "Updating your repositories"
# apt-get update
if check_package "wget"; then
	install_package "wget"
fi

if check_package "docker"; then
	# wget -qO- https://get.docker.com/ | sh
	echo "Installing"
fi

if check_package "docker-compose"; then
	# curl -L https://github.com/docker/compose/releases/download/1.2.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
	# chmod +x /usr/local/bin/docker-compose
fi