#! /bin/node
// This script removes all the data from the database
var mongoose = require('mongoose');

process.env.MONGODB_TEST = process.env.MONGODB;

mongoose.connect('mongodb://' + process.env.MONGODB);
mongoose.connection.on('open', function () {
	mongoose.connection.db.dropDatabase(dbDropped);
});

function dbDropped(err) {
	console.log('Database has been dropped.')
	if (err) console.log(err.stack);
	mongoose.connection.close(dbClosed);
}

function dbClosed(err) {
	console.log('Database closed.');
	if (err) console.log(err.stack);
}