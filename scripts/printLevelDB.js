var db = require('../config/leveldb')('../db_data');

var start = '\0',
	end = '~';

db.createReadStream({
	gte: start,
	lte: end
})
.on('data', function(data) {
	console.log(data);
})
.on('error', done)
.on('end', done);

function done(err) {
	if (err) console.log(err);
	console.log('Done');
}