#! /bin/bash
# host=172.17.0.10/test
host=localhost/test
echo 'Unit Tests - Data Structures...'
# ./node_modules/.bin/istanbul cover ./node_modules/.bin/_mocha -- --compilers js:babel/register --reporter spec --recursive ./test/data_structure
# ./node_modules/.bin/babel-node ./node_modules/.bin/isparta cover ./node_modules/.bin/_mocha -- --reporter spec --recursive ./test/data_structure
# MONGODB=$host node-debug --no-preload ./node_modules/mocha-co/bin/_mocha --compilers js:mocha-traceur --reporter spec --recursive ./test/
MONGODB=$host ./node_modules/.bin/mocha --recursive ./test/data_structure --compilers js:babel/register --reporter spec
# data_structure -g "small test"

echo 'API Tests - Handlers...'
# MONGODB=$host ./node_modules/.bin/babel-node ./node_modules/.bin/isparta cover ./node_modules/.bin/_mocha  -- --reporter spec --recursive ./test/api
MONGODB=$host ./node_modules/.bin/mocha --recursive ./test/api --compilers js:babel/register --reporter spec
# rm *.map