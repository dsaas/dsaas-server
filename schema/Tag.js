var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Tag', new Schema({
	datastructureId: {required: true, type: String},
	namespace: {required: true, type: String},
	name: {required: true, type: String},
	version: {required: true, type: String}
}));