var mongoose = require('mongoose');

module.exports = mongoose.model('User', new mongoose.Schema({
	email: {type: String, required: true, unique: true},
	password: {type: String, required: false},
	name: String,
	namespace: {type: String, required: true, unique: true}
}));