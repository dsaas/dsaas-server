var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('JWT', new Schema({
	userId: Schema.Types.ObjectId,
	name: String,
	enabled: Boolean,
	token: String,
	key: String
}));