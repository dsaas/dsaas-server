var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Log', new Schema({
	userId: {type: Schema.Types.ObjectId, ref: 'User'},
	datastructureId: String,
	namespace: String,
	url: String,
	method: String,
	date: Date,
	duration: Number,
	versionCalled: String
}));