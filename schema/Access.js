var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AccessSchema = new Schema({
	namespace: {type: String, required: true},
	datastructureId: {type: String, required: true},
	_user: {type: Schema.Types.ObjectId, ref: 'User'},
	_ds: {type: Schema.Types.ObjectId, ref: 'Datastructure'},
	type: String
});

AccessSchema.index({_user: 1, _ds: 1}, {unique: true});
module.exports =  mongoose.model('Access', AccessSchema);