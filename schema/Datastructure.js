var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Datastructure', new Schema({
	namespace: {type: String, required: true},
	datastructureId: {type: String, required: true},
	type: {type: String, required: true},
	forkID: Schema.Types.ObjectId,
	preferences: Object,
	access: {
		public: {type: String, required: true, default: 'none'},
		registered: {type: String, required: true, default: 'none'}
	}
}));