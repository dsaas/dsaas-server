# DSaaS (Data Structures as a Service) #

# For more information see:
[DSaaS Main Page](http://dsaas.pbit.co.za/)

[DSaaS Article Information](http://www.cs.sun.ac.za/~kroon/pubs/leroux2016dsaas/)

### What is this repository for? ###

This repository contains the code that is used for experimentation, prototyping and eventually the complete project.

Version: 0.0.1  -  Prototype

### Running ###
Install [NodeJS](https://nodejs.org/)
As soon as this is installed, run the following commands from the terminal.

sudo npm install && npm start

The server will be running as soon as this is done.
Important: Make sure you have a local instance of mongodb installed and running.

### Testing ###
Mocha is used as the test runner.
To test run the following command:

npm test

### How do I get set up? ###

#### Docker ####
Download and install (Docker)[https://docs.docker.com/installation/#installation].

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* pierrebleroux@gmail.com
* Other community or team contact