'use strict';
var express = require('express'),
	Datastructures = require('../schema/Datastructure'),
	notifier = require('../config/notifier')('global');

/* An object containing logic for building the response.
 * Keeps the system DRY.
 */
var g = {
	/**
	 * Constructs a function for managing the reply.
	 * @param  {Object} res NodeJS's response object.
	 * @return {Function}   A function to handle the returning message's logic.
	 */
	reply: function (res) {
		/**
		 * Handles the error if it occurs, otherwise returns a JSON response.
		 */
		return function (err, result) {
			if (err) 
				return res.json({error: err.message});
			else 
				return res.json(result);
		}
	},
	/**
	 * Constructs a function for managing the reply in a socket.
	 * @param  {Function} fn   Callback function for acknowledging the socket.
	 * @return {Function}      Builds the acknowledgment --- which sends the data to the client.
	 */
	sreply: function (fn) {
		return function (err, result) {
			if (err)
				return fn({error: err.message});
			else
				return fn(result);
		}
	}
};

module.exports = function (app) {
	var router = express.Router();
	var io = app.get('io');

	/**
	 * Test API
	 */
	function echo(req, res, next) {
		res.json(req.body);
	}

	router.route('/test/')
		.get((req, res, next) => {
			console.log('Test route hit');
			res.json({'success': 'yes'});
		})
		.post(echo)
		.put(echo)
		.delete(echo);

	router.route('/testfail')
		.get((req, res, next) => {
			next("Fail Yes");
		});
	
	/**
	 * Main API
	 */
	require('./user')(router, io, g);
	require('./general')(router, io, g);
	require('./graph')(router, io, g);
	require('./map')(router, io, g);
	
	/**
	 * Route for showing what RESTfull API calls are available.
	 */
	router.route('/')
		.get((req, res, next) => {
			let routeList = {
				'Description': 'The following are the available api calls'
			};
			let prefix = '/api';
			for (let r of router.stack) {
				if (r.route) {
					if (req.query.q && r.route.path.indexOf(req.query.q) !== -1 || !req.query.q)
						routeList[prefix + r.route.path] = r.route.description;
				}
			}
			res.send(routeList);
		})
		.description = 'Show the available API calls';

	app.use('/api', router);

	notifier.on('versionAdded', function () {
		console.log('VersionAdded clean run');
	});

	// Setup the socket events
	io.on('connection', function (socket) {
		console.log('user connected');

		var SUBSCRIBED_ID = null;
		
		socket.on('/api/history/register', function (namespace, id, cb) {
			console.log('user registered to ' + namespace + ' ' + id);
			Datastructures.findOne({namespace: namespace, datastructureId: id}, function (err, ds) {
				if (err) console.log(err);
				console.log(ds._id);
				SUBSCRIBED_ID = ds._id;
				cb();
			});
		});

		var notifyVersionAdded = function (child, parent, ID) {
			console.log('ID: ', String(ID) == String(SUBSCRIBED_ID));
			if (String(SUBSCRIBED_ID) == String(ID)) {
				socket.emit('versionAdded', child, parent);
			}
		};
		notifier.on('versionAdded', notifyVersionAdded);



		socket.on('disconnect', function () {
			console.log('user disconnected');
			notifier.removeListener('versionAdded', notifyVersionAdded);
		});
	});
};