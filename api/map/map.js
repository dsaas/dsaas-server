'use strict';

var VersionedTrie = require('../../data_structure/VersionedTrie'),
	co = require('co'),
	keyValueDB = require('../../config/leveldb')(),
	utils = require('../utils');

function * getVersionedTrie(namespace, datastructureId) {
	var datastructure = yield utils.getDatastructure(namespace, datastructureId);
	utils.checkType(datastructure, 'map');
	var id = datastructure.forkID || datastructure._id;
	// Get the versioned trie from the database
	return VersionedTrie(id, keyValueDB, datastructure._id);
}

/**
 * Map Handler
 */
module.exports = Map = {
	/**
	 * Add data.
	 * Creates a node if it does not exist and then adds the obj.
	 *
	 * @param  {String}   namespace Namespace of data structure
	 * @param  {String}   key       The key for this key-value pair.
	 * @param  {Object}   value     The value for this key-value pair.
	 * @param  {Number}   version   The version that this creation of node is based on.
	 * @param  {Function} cb        The asynchronous callback function.
	 * @return {Number}             The identifier for the node
	 */
	put: function put (namespace, datastructureId, key, value, version, cb) {
		co(function *(){

			var versionedTrie = yield getVersionedTrie(namespace, datastructureId);

			// Add the key value pair in the correct version of the trie
			var keyValuePair = {};
			keyValuePair[key] = value;
			var res = yield versionedTrie.put(keyValuePair, version);

			return {version: res};
		}).then(val => cb(null, val), err => cb(err));
	},

	/**
	 * Get the value of a key in the given version.
	 * @param  {String}   namespace       User's namespace
	 * @param  {String}   datastructureId User's identifier of the data structure
	 * @param  {String}   key             The associated key
	 * @param  {Number}   version         The version to operate on
	 * @param  {Function} cb              The asynchronous callback function
	 * @return {String}                   JSON Object
	 */
	getValueOf: function getValueOf (namespace, datastructureId, key, version, cb) {
		co(function* () {
			return yield (yield getVersionedTrie(namespace, datastructureId)).get(key, version);
		}).then((val) => cb(null, val), (err) => cb(err));
	},

	/**
	 * Get the set of keys
	 * @param  {String}   namespace       User's namespace
	 * @param  {String}   datastructureId User's identifier of the data structure
	 * @param  {Number}   version         The version to operate on
	 * @param  {Function} cb              The asynchronous callback function
	 * @return {Array}                    Array of the keys
	 */
	keys: function keys (namespace, datastructureId, version, cb) {
		co(function* () {
			return yield (yield getVersionedTrie(namespace, datastructureId)).keysOrValues(version);
		}).then((val)=>cb(null,val), (err)=>cb(err));
	},

	/**
	 * Removes a key-value pair from the map referenced by the version.
	 * @param  {String}   namespace       User's namespace
	 * @param  {String}   datastructureId User's identifier of the data structure
	 * @param  {String}   key             The key to remove
	 * @param  {Number}   version         The version of the data structure
	 * @param  {Function} cb              The asynchronous callback function
	 * @return {Object}                   {$version: version}
	 */
	remove: function remove(namespace, datastructureId, key, version, cb) {
		co(function* () {
			var res = yield (yield getVersionedTrie(namespace, datastructureId)).remove([key], version);
			return yield {version: res}
		}).then(val=>cb(null,val), err=>cb(err))
	},

	values: function values(namespace, datastructureId, version, cb) {
		co(function* () {
			return yield (yield getVersionedTrie(namespace, datastructureId)).keysOrValues(version, null, null, true);
		}).then(val=>cb(null,val), err=>cb(err));
	},

	entries: function entries(namespace, datastructureId, version, start, end, cb) {
		co(function* () {
			return yield (yield getVersionedTrie(namespace, datastructureId))
				.keysOrValues(version, null, null, false, true, start, end);
		}).then(val=>cb(null,val), err=>cb(err));
	}
};