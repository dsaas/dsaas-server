'use strict';
/**
 * Map index.js file.
 * Note: All index files are automatically called when the directory is ``required''
 *
 * Specifies the API routing for MAP.
 */
var MapDataStructure = require('./map'),
	log = require('../log'),
	auth = require('../auth');

// Export the function that will accept the Express App
// and then set up the API service.
module.exports = function (router, io, g) {
	
	//////////
	// REST //
	//////////

	/**
	 * ROUTE: Retrieves the keys of a map per specified version.
	 */
	router.route('/:namespace/:datastructureId/:version/map/keys')
		.get(auth('read'), log, (req, res) => {
			MapDataStructure.keys(req.params.namespace,
				req.params.datastructureId,
				req.params.version,
				g.reply(res));
		})
		.description = {
			'get':'Retrieves the keys in the version.'
		};

	/**
	 * ROUTE: Retrieves the values of the map per specified version.
	 */
	router.route('/:namespace/:datastructureId/:version/map/values')
		.get(auth('read'), log, (req, res) => {
			MapDataStructure.values(req.params.namespace,
				req.params.datastructureId,
				req.params.version,
				g.reply(res));
		})
		.description = {
			'get':'Retrieves all values in the version.'
		};

	router.route('/:namespace/:datastructureId/:version/map/entries')
		.get(auth('read'), log, (req, res) => {
			MapDataStructure.entries(req.params.namespace,
				req.params.datastructureId,
				req.params.version,
				Number(req.query.start) || undefined,
				Number(req.query.end) || undefined,
				g.reply(res));
		})
		.description = {
			'get':'Retrieves all entries in the version.'
		};

	router.route('/:namespace/:datastructureId/:version/map/:key')
		.post(auth('read/write'), log, (req, res) => {
			MapDataStructure.put(req.params.namespace,
				req.params.datastructureId,
				req.params.key, 
				req.body,
				req.params.version, 
				g.reply(res));
		})
		.get(auth('read'), log, (req, res) => {
			MapDataStructure.getValueOf(req.params.namespace, 
				req.params.datastructureId,
				req.params.key, 
				req.params.version, 
				g.reply(res));
		})
		.delete(auth('read/write'), log, (req, res) => {
			MapDataStructure.remove(req.params.namespace,
				req.params.datastructureId,
				req.params.key,
				req.params.version,
				g.reply(res));
		})
		.description = {
			'info': 'Map Handler',
			'post': 'Add a key-value pair to the Map associated with the version.',
			'get': 'Retrieves the value associated with the key from the Map associated with the version.',
			'delete': 'Removes a key-value pair from the Map associated with the version.'
		};

	/////////////
	// Sockets //
	/////////////
	
	io.on('connection', function (socket) {
		console.log("Connected?");
	// 	/*
	// 	 * sROUTE: Retrieve the keys of a map per specified version.
	// 	 */
	// 	socket.on('/api/map/keys', function (namespace, id, version, fn) {
	// 		MapDataStructure.keys(namespace, id, version, g.sreply(fn));
	// 	});

	// 	/**
	// 	 * sROUTE: Retrieves the values of the map per specified version.
	// 	 */
	// 	socket.on('/api/map/values', function (namespace, id, version, fn) {
	// 		MapDataStructure.values(namespace, id, version,	g.sreply(fn));
	// 	});

	// 	socket.on('/api/map/entries', function (namespace, id, version, fn) {
	// 		MapDataStructure.entries(namespace, id, version, g.sreply(fn));
	// 	});

		socket.on('/api/map:put', function (namespace, id, version, key, value, fn) {
			console.log('MAP PUT!');
			console.log(namespace, id, key, value, version);
			MapDataStructure.put(namespace, id, key, value, version, g.sreply(fn));
		});

	// 	socket.on('/api/map:get', function (namespace, id, version, key, fn) {
	// 		MapDataStructure.getValueOf(namespace, id, key, version, g.sreply(fn));
	// 	});

	// 	socket.on('/api/map:remove', function (namespace, id, version, key, fn) {
	// 		MapDataStructure.remove(namespace, id, key, version, g.sreply(fn));
	// 	});
	});
};