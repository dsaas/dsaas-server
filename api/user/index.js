'use strict';
/**
 * User index.js file.
 * Note: All index files are automatically called when the directory is ``required''
 *
 * Specifies the API for the user handler
 */
var co = require('co'),
	bcrypt = require('co-bcryptjs'),
	jwt = require('jsonwebtoken'),
	error = require('../../config/error'),
	crypto = require('crypto'),
	auth = require('../auth'),
	utils = require('../utils'),
	mongoose = require('mongoose');

var User = mongoose.model('User'),
	Datastructures = mongoose.model('Datastructure'),
	JWTCollection = mongoose.model('JWT'),
	Access = mongoose.model('Access');

// Export the function that will accept the Express App
// and then set up the API service.
module.exports = function (router, socket, g) {
	router.route('/shared/:namespace?').get((req, res, next) => {
		var filter = {
			'access.public': {$ne: 'none'}
		};
		if (req.params.namespace) {
			filter.namespace = req.params.namespace;
		}
		Datastructures.find(filter,
			(err, docs) => {
				if (err) return next(err);
				res.json(docs);
			});
	});

	router.route('/user')
		.post((req, res, next) => {
			co(function * () {
				var email 	  = utils.check(req.body.email, 'email'),
					name 	  = utils.check(req.body.name, 'name'),
					// password  = utils.check(req.body.password, 'password'),
					namespace = utils.check(req.body.namespace, 'namespace');

				var user = yield User.findOne({
					email: email
				}).exec();
				
				if (user) throw error.API('User already exists.');

				// var npassword = yield bcrypt.hash(password, utils.salt);
				var user = yield User.create({
					email: email,
					name: name,
					surname: req.body.surname,
					namespace: namespace
				});

				return user._id;
			}).then(val => res.send(val), next);
		})
		.get((req, res, next) => {
			co(function * () {
				var user = yield User.findById(req.jwt.userId).exec();
				return user;
			}).then(val => res.json(val), next);
		})
		.put((req, res, next) => {
			co(function * () {

				var update = {};
				
				if (req.body.name)
					update.name = req.body.name;

				if (req.body.surname)
					update.surname = req.body.surname;

				yield User.findByIdAndUpdate(req.jwt.userId, {
					$set: update
				}).exec();

				return 'updated';
			}).then(val => res.send(val), next);
		})
		.description = {
			'post': 'Create a new user, Expects: email, name, password, namespace and surname (optional).',
			'get': 'Retrieve user\'s profile',
			'put': 'Update user\'s profile'
		};


	router.route('/user/signin')
		.post((req, res, next) => {
			co(function * () {
				var user;
				if (req.body.googleToken) {
					var token = jwt.decode(req.body.googleToken, {complete: true});
					if (token.payload.iss !== 'accounts.google.com' && token.payload.iss !== 'https://accounts.google.com' &&
						token.payload.aud !== '508772266412-0ivq36d67487qg6dmb41bs77gciiu1r7.apps.googleusercontent.com')
						throw error.AUTH('Google token is invalid');

					user = yield User.findOne({
						email: token.payload.email
					}).exec();
					
					if (!user)
						throw error.AUTH('User does not exist.');

				} else if (req.body.githubToken) {
				} else {
					user = yield User.findOne({
						email: req.body.email,
					}).exec();

					if (!user)
						throw error.AUTH('User does not exist.');

					// if (!user || !(yield bcrypt.compare(req.body.password, user.password))) 
						// throw error.NOT_FOUND('Combined information of user does not match our records.');

				}

				// Set the JWT token
				var jwtDoc = yield JWTCollection.findOne({
					userId: user._id,
					name: 'personal'
				}).exec();

				if (!jwtDoc) {
					jwtDoc = {
						userId: user._id,
						name: 'personal'
					};
					jwtDoc.enabled = true;
					jwtDoc.key = crypto.randomBytes(32).toString('hex'); // TODO: Make async
					jwtDoc = yield JWTCollection.create(jwtDoc);
				}

				var signedToken = utils.signJWT(jwtDoc._id, jwtDoc.key);
				yield JWTCollection.findByIdAndUpdate(jwtDoc._id, {$set: {token: signedToken}}).exec();
				return signedToken;
				
			}).then(val => res.send(val), next);
		})
		.description = {
			'post': 'Sign the user in'
		};

	router.route('/user/signout')
		.post((req, res, next) => {
			co(function * () {
				// Change the document of the JWT
				var jwtDoc = yield JWTCollection.findByIdAndRemove(req.jwt.id).exec();
				return 'ok';
			}).then(val => res.send(val), next);
		})
		.description = {
			'post': 'Sign the user out'
		};

	router.route('/users')
		.get((req, res, next) => {
			co(function * () {
				var obj = {};
				for (let a in req.query)
					obj[a] = {$regex: req.query[a]};
				return yield User.find(obj, '-password -_id').exec();
			}).then(val => res.json(val), next);
		})
		.description = {
			'get': 'Retrieve all the user'
		};


	// Permissions to Access Data Structures
	router.route('/:namespace/:datastructureId/access')
		.get(auth('admin'), (req, res, next) => {

			// Find all the access permissions on the data structure
			Access.find({
				namespace: req.params.namespace, 
				datastructureId: req.params.datastructureId,
			})
			.populate('_user')
			.exec(function (err, access) {
				if (err) return next(err);

				// Find the access permissions of the public and private users
				Datastructures.findOne({
					namespace: req.params.namespace,
					datastructureId: req.params.datastructureId
				}, function (err, ds) {
					if (err) return next(err);

					// Add these permissions
					access.push({_user: 'public', type: ds.access.public});
					access.push({_user: 'registered', type: ds.access.registered});
					res.json(access);
				});
			});
		})
		.description = {
			'get': 'Retrieve all access permissions on the datastructure'
		};


	function* accessCheck(params) {
		// Retrieve the user and data structure in parallel from the database.
		var db = yield {
			user: User.findOne({email: params.email}).exec(),
			datastructure: Datastructures.findOne({namespace: params.namespace, datastructureId: params.datastructureId}).exec()
		};

		// Verify that the user exists and the data structure exists.
		if (!db.user) throw error.API('This email does not match any of our records.');
		if (!db.datastructure) throw error.API('The data structure or namespace does not exist.');
		return db;
	}

	router.route('/:namespace/:datastructureId/access/:email')
		.put(auth('admin'), (req, res, next) => {
			co(function * () {
				// Set the special users if requested
				if (req.params.email === 'public' || req.params.email === 'registered') {
					yield updateSpecialUserPermission(req.params, req.body.type);
					return 'ok';
				} else {
					// Otherwise set the user according to their email
					var type = utils.check(req.body.type, 'type');
					var db = yield accessCheck(req.params);

					if (req.jwt.userId === db.user._id) throw error.API('Owner always has admin permissions.');

					try {
						yield Access.create({
							namespace: req.params.namespace,
							datastructureId: req.params.datastructureId,
							_user: db.user._id,
							_ds: db.datastructure._id,
							type: type
						});
					} catch (e) {
						if (e.code == 11000) {
							throw new error.API(req.params.email + ' already has access.');
						} else {
							throw e;
						}
					}

					return 'ok';
				}

			}).then(val => res.send(val), next);
		})
		.post(auth('admin'), (req, res, next) => {
			co(function * () {

				if (req.params.email === 'public' || req.params.email === 'registered') {
					yield updateSpecialUserPermission(req.params, req.body.type);
					return 'ok';
				} else {
					var type = utils.check(req.body.type, 'type');
					var db = yield accessCheck(req.params);

					yield Access.update({
						namespace: req.params.namespace,
						datastructureId: req.params.datastructureId,
						_user: db.user._id
					}, {
						$set: {
							type: type
						}
					});

					return 'ok';
				}

			}).then(val => res.send(val), next);		
		})
		.delete(auth('admin'), (req, res, next) => {
			co(function * () {
				
				var db = yield accessCheck(req.params);

				yield Access.remove({
					namespace: req.params.namespace,
					datastructureId: req.params.datastructureId,
					_user: db.user._id
				});

				return 'ok';
			}).then(val => res.send(val), next);
		})
		.description = {
			'get': 'Get a specific user\'s access permission',
			'post': 'Update a user\'s access permission',
			'delete': 'Remove a user\'s access permission'
		}

		/**
		 * Update a special user's permissions according to the type.
		 * @param {Object} params        parameters passed on from the root.
		 * @param {String} type          permissions are given to the user.
		 */
		function* updateSpecialUserPermission(params, type) {
			if (type === 'admin') throw error.API('Cannot set registered or public as admin.');

			var access = {};
			access['access.'+params.email] = type;

			yield Datastructures.findOneAndUpdate({
				namespace: params.namespace,
				datastructureId: params.datastructureId
			}, {
				$set: access
			}).exec();
		}

	require('./JWT')(router);
};