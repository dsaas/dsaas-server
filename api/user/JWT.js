var co = require('co'),
	error = require('../../config/error'),
	crypto = require('crypto'),
	utils = require('../utils'),
	auth = require('../auth');


var	JWTCollection = require('../../schema/JWT');



module.exports = function (router) {

	// Auth JWTs

	router.route('/token')
		.get(auth('owner'), (req, res, next) => {
			co(function *() {
				return yield JWTCollection.find({
					userId: req.jwt.userId
				}, 'name enabled token -_id');
			}).then(val => res.send(val), next);
		})
		.description = {
			get: 'Get all the JWT tokens of the namespace'
		};

	router.route('/token/:name')
		.put(auth('owner'), (req, res, next) => {
			co(function* () {
				var jwtDoc = {
					userId: req.jwt.userId,
					name: req.params.name,
					key: crypto.randomBytes(32).toString('hex'), // TODO: Make async
					enabled: true
				};

				jwtDoc = yield JWTCollection.create(jwtDoc);
				var signedToken = utils.signJWT(jwtDoc._id, jwtDoc.key);
				yield JWTCollection.findByIdAndUpdate(jwtDoc._id, {
					$set: {
						token: signedToken
					}
				}).exec();
				return signedToken;
			}).then(val => res.send(val), next);
		})
		.get(auth('owner'), (req, res, next) => {
			co(function* () {
				var jwtDoc = yield JWTCollection.findOne({
					userId: req.jwt.userId,
					name: req.params.name
				}, '-_id -key').exec();

				return jwtDoc;
			}).then(val => res.send(val), next);
		})
		.delete(auth('owner'), (req, res, next) => {
			co(function* () {

				// Avoid deleting ``personal'' tokens.
				if (req.params.name == 'personal') return;

				var jwtDoc = yield JWTCollection.remove({
					userId: req.jwt.userId,
					name: req.params.name
				}).exec();

				return 'ok';
			}).then(val => res.send(val), next);
		})
		.description = {
			put: 'Create an authentication JWT',
			get: 'Get a specific authentication JWT',
			delete: 'Remove an authentication JWT'
		};

	router.route('/token/:name/enable')
		.post(auth('owner'), (req, res, next) => {
			co(function* () {
				yield JWTCollection.update({
					userId: req.jwt.userId,
					name: req.params.name
				}, {
					$set: {
						enabled: true
					}
				});

				return 'ok';
			}).then(val => res.send(val), next);
		})
		.description = {
			'post': 'Enable a token by the name'
		};

	router.route('/token/:name/disable')
		.post(auth('owner'), (req, res, next) => {
			co(function* () {
				if (req.params.name == 'personal') throw error.API('Cannot disable a personal token.');
				yield JWTCollection.update({
					userId: req.jwt.userId,
					name: req.params.name
				}, {
					$set: {
						enabled: false
					}
				});

				return 'ok';
			}).then(val => res.send(val), next);
		})
		.description = {
			'post': 'Disable a token by the token name'
		};
}