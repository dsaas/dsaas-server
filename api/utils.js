'use strict';

var	co = require('co'),
	jwt = require('jsonwebtoken'),
	error = require('../config/error');

var Datastructure = require('../schema/Datastructure');


var salt = require('bcryptjs').genSaltSync(10);

module.exports = {
	/**
	 * Retrieve the data structure from the document database.
	 * @param {String} namespace       User's namespace
	 * @param {String} datastructureId Data structure identifier
	 * @yield {Object} The MongoDB data structure wrapper.
	 */
	getDatastructure: function* getDatastructure(namespace, datastructureId) {
		// Find the data structure
		var datastructure = yield Datastructure.findOne({
			datastructureId: datastructureId,
			namespace: namespace
		}).exec();

		if (!datastructure) throw Error('Data Structure does not exist.');
		return datastructure;
	},

	createDatastructure: function* (namespace, datastructureId, type, forkID) {
		// Check if the identifier exists
		var datastructure = yield Datastructure.findOne({
			namespace: namespace,
			datastructureId: datastructureId
		});

		if (datastructure) throw error.API('Data structure already exists.');

		// Add the new data structure
		datastructure = yield Datastructure.create({
			namespace: namespace, // Assume namespace is used to identify the user
			datastructureId: datastructureId,
			type: type,
			forkID: forkID || null,
			preferences: null
		});

		return datastructure;
	},

	checkType: function checkType(datastructure, type) {
		if (datastructure.type != type) throw Error('The data structure is not of type "'+type+'".');
	},

	check: function check(obj, validation) {
		if (validation === 'email') {

			if (require('email-validator').validate(obj)) return obj;
			else throw error.API('Invalid email');

		} else if (validation === 'password') {

			if (obj.length > 5) return obj;
			else throw error.API('Password is invalid, it should be of length more than 5.');

		} else if (validation === 'type') {

			if (obj !== 'none' && obj !== 'read' && obj !== 'read/write' && obj !== 'admin')
				throw error.API('The type "'+obj+'" is incorrect, it should be "read", "read/write" or "admin".');
			return obj;

		} else {

			if (obj !== '') return obj;
			else throw error.API(validation + ' may not be empty.');

		}
	},

	signJWT: function (id, key) {
		return jwt.sign({id: id, key: key}, this.secret, {
			issuer: this.issuer,
			noTimestamp: true
		});
	},

	secret: 'thiswill123alwaysbe{}][--a--se--~``+=+cret',

	salt: salt
};