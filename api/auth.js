/**
 * Protects routes with authentication.
 */
var	error = require('../config/error'),
	Access = require('../schema/Access'),
	Datastructure = require('../schema/Datastructure'),
	Users = require('../schema/User');

module.exports = function (type) {

	// Check if the type is set correctly by the developer
	if (['owner', 'read', 'read/write', 'admin'].indexOf(type) === -1) {
		throw Error('Auth needs parameter of type owner, read, read/write or admin not "' + type + '"');
	}

	// Return the middleware for checking the authentication
	return function (req, res, next) {
		Datastructure.findOne({
			namespace: req.params.namespace, 
			datastructureId: req.params.datastructureId
		}, datastructureRetrieved);

		function datastructureRetrieved(err, datastructure) {
			if (err) return next(err);

			// Check if the data structure's public operation is the same as the type
			// If so then let the user proceed.
			if (!datastructure) {
				// return next(error.NOT_FOUND('The data structure with the namespace and identifier does not exist.'));
			}

			if (datastructure && allowed(datastructure.access.public)) {
				return next();
			}

			if (!req.jwt) {
				// Otherwise throw this error
				return next(error.AUTH('Unknown user, please use authoriziation token.'));
			} else {
				// User is registered (they have a valid JWT)
				// So check if the data structure allows registered user operations.
				if (allowed(datastructure && datastructure.access.registered)) {
					return next();
				}

				// Check user permissions
				Access.findOne({
					_user: req.jwt.userId, 
					namespace: req.params.namespace, 
					datastructureId: req.params.datastructureId
				}, checkPermission);
			}
		}

		function checkPermission(err, permission) {
			var params = req.params;
			if (err) return next(err);
			if (permission) permission = permission.type;

			// Check if the user has a permission on the data structure
			if (!permission) {
				// Check if owner
				Users.findById(req.jwt.userId, (err, user) => {
					if (err) return next(err);
					req.jwt.user = user;
					// Allow through the user if no namespace was defined???
					if (!params.namespace) return next();
					// Allow through the user if the namespaces are equal.
					if (req.jwt.user.namespace === params.namespace) return next();
					// Otherwise reject the user.
					return next(error.AUTH('User does not have access permissions set on this API'));
				});
			} else if (allowed(permission)) {
				// Permission was allowed
				return next();
			} else {
				// Permission was not allowed
				return next(error.AUTH('User does not have access permissions set on this API'));
			}
		}

		function allowed(permission) {
			return convert(type) <= convert(permission);
		}

		function convert(permission) {
			switch (permission) {
				case 'none': return 0;
				case 'read': return 1;
				case 'read/write': return 2;
				case 'admin': return 3;
				case 'owner': return 4;
			}
			return -1;
		}

	};
};