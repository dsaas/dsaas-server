'use strict';

var VersionedGraph = require('../../data_structure/VersionedGraph'),
	co = require('co'),
	keyValueDB = require('../../config/leveldb')(),
	utils = require('../utils');

function* getVersionedGraph(namespace, datastructureId) {
	var datastructure = yield utils.getDatastructure(namespace, datastructureId); // E1
	utils.checkType(datastructure, 'graph'); // E2
	var id = datastructure.forkID || datastructure._id;
	return VersionedGraph(id, keyValueDB, null, datastructure._id);
}

module.exports = {
	createNode: function createNode(namespace, datastructureId, properties, version, cb, nodeId) {
		co(function *() {
			var nodeIds;
			if (nodeId) {
				nodeIds = [nodeId];
			}
			var ret = yield (yield getVersionedGraph(namespace, datastructureId)).addNodes([properties], version, nodeIds);

			// Change format of the returned object
			if (!nodeId) nodeIds = ret.nodeIds;
			delete ret.nodeIds;
			ret.id = nodeIds[0];
			
			return ret;
		}).then(val => cb(null, val), err => cb(err));
	},

	updateNode: function updateNode(namespace, datastructureId, nodeId, properties, version, cb) {
		co(function *() {

			var node = {};
			node[nodeId] = properties;
			return yield {
				version: (yield getVersionedGraph(namespace, datastructureId))
					.updateNodes(node, version)
			};

			// Errors E3 and E4 are thrown in the above statement.
		}).then(val => cb(null, val), err => cb(err));
	},

	getNode: function getNode(namespace, datastructureId, nodeId, version, cb) {
		co(function * () {

			return yield (yield getVersionedGraph(namespace, datastructureId))
				.getNode(nodeId, version);

		}).then(val => cb(null, val), err => cb(err));
	},

	// getNodeIdentifier: function getNodeIdentifier(namespace, datastructureId, nodeId, version, cb) {
	// 	co(function * () {
	// 		var datastructure = yield utils.getDatastructure(namespace, datastructureId);
	// 	}).then(val => cb(null, val), err => cb(err));
	// },

	removeNode: function removeNode(namespace, datastructureId, nodeId, version, cb) {
		console.log('removign: ', namespace, datastructureId, nodeId, version);
		co(function *() {
			return yield {
				version: (yield getVersionedGraph(namespace, datastructureId))
					.removeNodes([nodeId], version)
			};

		}).then(val => cb(null, val), err => cb(err));
	},

	createEdge: function createEdge(namespace, datastructureId, nodeIdA, nodeIdB, properties, version, cb) {
		co(function *() {
			
			var edge = properties;
			edge.from = nodeIdA;
			edge.to = nodeIdB;

			var ret = yield (yield getVersionedGraph(namespace, datastructureId))
				.addEdges([edge], version);

			let edgeIds = ret.edgeIds;
			delete ret.edgeIds;
			ret.id = edgeIds[0];

			return ret;

		}).then(val => cb(null, val), err => cb(err));
	},

	getEdge: function getEdge(namespace, datastructureId, edgeId, version, cb) {
		co(function *() {

			return yield (yield getVersionedGraph(namespace, datastructureId))
				.getEdge(edgeId, version);

		}).then(val => cb(null, val), err => cb(err));
	},

	updateEdge: function updateEdge(namespace, datastructureId, edgeId, properties, version, cb) {
		co(function *() {

			var edge = {};
			edge[edgeId] = properties;
			return yield {
				version: (yield getVersionedGraph(namespace, datastructureId))
					.updateEdges(edge, version)
			};

		}).then(val => cb(null, val), err => cb(err));
	},

	removeEdge: function removeEdge(namespace, datastructureId, edgeId, version, cb) {
		co(function *() {

			return yield {
				version: (yield getVersionedGraph(namespace, datastructureId))
					.removeEdges([edgeId], version)
			};

		}).then(val => cb(null, val), err => cb(err));
	},

	allNodes: function allNodes(namespace, datastructureId, version, cb) {
		co(function *() {

			return yield (yield getVersionedGraph(namespace, datastructureId))
				.allNodeIds(version);

		}).then(val => cb(null, val), err => cb(err));
	},

	allFullNodes: function allFullNodes(namespace, datastructureId, version, cb) {
		co(function * () {
			
			return yield (yield getVersionedGraph(namespace, datastructureId))
				.allNodes(version);

		}).then(val => cb(null, val), err => cb(err));
	},

	allEdges: function allEdges(namespace, datastructureId, version, cb) {
		co(function *() {

			return yield (yield getVersionedGraph(namespace, datastructureId))
				.allEdges(version);

		}).then(val => cb(null, val), err => cb(err));
	},

	allFullEdges: function allFullNodes(namespace, datastructureId, version, cb) {
		co(function *() {

			return yield (yield getVersionedGraph(namespace, datastructureId))
				.allFullEdges(version);

		}).then(val => cb(null, val), err => cb(err));
	},

	adjacentNodes: function adjacentNodes(namespace, datastructureId, nodeId, version, cb) {
		co(function *() {

			return yield (yield getVersionedGraph(namespace, datastructureId))
				.adjacent(nodeId, version);

		}).then(val => cb(null, val), err => cb(err));
	},

	childNodes: function childNodes(namespace, datastructureId, nodeId, version, cb) {
		co(function *() {

			return yield (yield getVersionedGraph(namespace, datastructureId))
				.children(nodeId, version);

		}).then(val => cb(null, val), err => cb(err));
	},

	parentNodes: function parentNodes(namespace, datastructureId, nodeId, version, cb) {
		co(function *() {

			return yield (yield getVersionedGraph(namespace, datastructureId))
				.parents(nodeId, version);

		}).then(val => cb(null, val), err => cb(err));
	}
};