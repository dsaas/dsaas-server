'use strict';
/**
 * Graph index.js file.
 * Note: All index files are automatically called when the directory is ``required''
 *
 * Specifies the API routing for Graph.
 */

var Graph = require('./graph'),
	VersionedGraph = require('../../data_structure/VersionedGraph'),
	log = require('../log'),
	auth = require('../auth');

// Export the function that will accept the Express App
// and then set up the API service.
module.exports = function (router, socket, g) {

	// NODES
	// Create a new node
	router.route('/:namespace/:datastructureId/:version/graph/node')
		.post(auth('read/write'), log, (req, res) => {
			Graph.createNode(req.params.namespace,
				req.params.datastructureId,
				req.body, 
				req.params.version, 
				g.reply(res));
		})
		.description = {
			'post': 'Creates a new node at the given version.'
		};

	router.route('/:namespace/:datastructureId/:version/graph/node/:nodeId')
		.get(auth('read'), log, (req, res) => {
			Graph.getNode(req.params.namespace,
				req.params.datastructureId,
				req.params.nodeId, 
				req.params.version,
				g.reply(res));
		})
		.put(auth('read/write'), log, (req, res) => {
			Graph.updateNode(req.params.namespace,
				req.params.datastructureId,
				req.params.nodeId, 
				req.body, 
				req.params.version, 
				g.reply(res));
		})
		.post(auth('read/write'), log, (req, res) => {
			Graph.createNode(req.params.namespace,
				req.params.datastructureId,
				req.body,
				req.params.version,
				g.reply(res),
				req.params.nodeId);
		})
		.delete(auth('read/write'), log, (req, res) => {
			Graph.removeNode(req.params.namespace,
				req.params.datastructureId,
				req.params.nodeId,
				req.params.version,
				g.reply(res));
		})
		.description = {
			info: 'Manages the node given by nodeId.',
			post: {
				description: 'Create a new node with given identifier',
				body: 'The value of the node'
			},
			put: 'Updates Node', 
			get: 'Retrieves the node properties',
			delete: 'Removes the node'
		};

	// router.route('/:namespace/:datastructureId/graph/node/id')
	// 	.post(auth('read'), (req, res) => {
	// 		Graph.getNodeIdentifier(
	// 			req.params.datastructureId,
	// 			req.params.nodeId,
	// 			req.params.version,
	// 			req.body,
	// 			g.reply(res));
	// 	})
	// 	.description = {
	// 		post: 'Retrieve identifier of a node according to the node\'s properties'
	// 	};


	router.route('/:namespace/:datastructureId/:version/graph/nodes')
		.get(auth('read'), log, (req, res) => {
			if (req.query.full) {
				Graph.allFullNodes(req.params.namespace,
					req.params.datastructureId,
					req.params.version,
					g.reply(res));
			} else {
				Graph.allNodes(req.params.namespace,
					req.params.datastructureId,
					req.params.version,
					g.reply(res));
			}
		})
		.description = {
			get: 'Retrieves the set of all the node identifiers'
		}

	// EDGES
	router.route('/:namespace/:datastructureId/:version/graph/edge/:nodeIdA/:nodeIdB')
		.post(auth('read/write'), log, (req, res) => {
			Graph.createEdge(req.params.namespace,
				req.params.datastructureId,
				req.params.nodeIdA,
				req.params.nodeIdB,
				req.body,
				req.params.version,
				g.reply(res));
		})
		.get(auth('read'), log, (req, res) => {
			Graph.getEdge(req.params.namespace,
				req.params.datastructureId,
				req.params.nodeIdA+VersionedGraph.STR.EDGE_SEPARATOR+req.params.nodeIdB,
				req.params.version,
				g.reply(res));
		})
		.description = {
			post: {
				description: 'Create a new edge from nodeA to nodeB',
				body: 'Value of the edge'
			},
			get: 'Get the edge between the two node identifiers'
		};

	router.route('/:namespace/:datastructureId/:version/graph/edge/:edgeId')
		.get(auth('read'), log, (req, res) => {
			Graph.getEdge(req.params.namespace,
				req.params.datastructureId,
				req.params.edgeId,
				req.params.version,
				g.reply(res));
		})
		.put(auth('read/write'), log, (req, res) => {
			Graph.updateEdge(req.params.namespace,
				req.params.datastructureId,
				req.params.edgeId,
				req.body,
				req.params.version,
				g.reply(res));
		})
		.delete(auth('read/write'), log, (req, res) => {
			Graph.removeEdge(req.params.namespace, 
				req.params.datastructureId,
				req.params.edgeId,
				req.params.version, 
				g.reply(res));
		})
		.description = {
			'get': 'Retrieve the edge by the identifier',
			'put': 'Update the edge by the identifier',
			'delete': 'Removes the edge with the label, at the node'
		};

	router.route('/:namespace/:datastructureId/:version/graph/edges')
		.get(auth('read'), log, (req, res) => {
			if (req.query.full) {
				Graph.allFullEdges(req.params.namespace,
					req.params.datastructureId,
					req.params.version,
					g.reply(res));
			} else {
				Graph.allEdges(req.params.namespace,
					req.params.datastructureId,
					req.params.version,
					g.reply(res));
			}
		})
		.description = {
			get: 'Retrieves the set of all the edge identifiers'
		};

	router.route('/:namespace/:datastructureId/:version/graph/:nodeId/adjacent')
		.get(auth('read'), log, (req, res) => {
			Graph.adjacentNodes(req.params.namespace,
				req.params.datastructureId,
				req.params.nodeId,
				req.params.version,
				g.reply(res));
		})
		.description = {
			get: 'Retrieves all the adjacent nodes to some node.'
		};

	router.route('/:namespace/:datastructureId/:version/graph/:nodeId/parents')
	.get(auth('read'), log, (req, res) => {
			Graph.parentNodes(req.params.namespace,
				req.params.datastructureId,
				req.params.nodeId,
				req.params.version,
				g.reply(res));
		})
		.description = {
			get: 'Retrieves all the adjacent nodes to some node.'
		};

	router.route('/:namespace/:datastructureId/:version/graph/:nodeId/children')
	.get(auth('read'), log, (req, res) => {
			Graph.childNodes(req.params.namespace,
				req.params.datastructureId,
				req.params.nodeId,
				req.params.version,
				g.reply(res));
		})
		.description = {
			get: 'Retrieves all the adjacent nodes to some node.'
		};
};