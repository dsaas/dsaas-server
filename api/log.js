var Log = require('../schema/Log'),
	ObjectId = require('mongoose').Types.ObjectId,
	error = require('../config/error');

module.exports = (req, res, next) => {
	var start = new Date();

	req.on('end', () => {
		var end = new Date();
		var duration = end.getTime() - start.getTime();
		var id = req.jwt ? ObjectId(req.jwt.userId) : null;

		Log.create({
			userId: id,
			datastructureId: req.params.datastructureId,
			namespace: req.params.namespace,
			url: req.originalUrl,
			method: req.method,
			date: end,
			duration: duration,
			versionCalled: req.params.version
		}, function (err, log) {
			if (err) console.log(err);
		});
	});

	next();
};