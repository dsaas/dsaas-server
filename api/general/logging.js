'use strict';

var co = require('co'),
	auth = require('../auth'),
	utils = require('../utils'),
	error = require('../../config/error'),

	Log = require('../../schema/Log'),
	Datastructures = require('../../schema/Datastructure');

// Note that these routes are documented in the description section of each route.
module.exports = function (router) {
	router.route('/:namespace/:id/log/enable')
		.get(auth('read/write'), (req, res, next) => {
			co(function * () {
				var datastructure = yield utils.getDatastructure(req.params.namespace, req.params.id);
				yield Datastructures.findByIdAndUpdate(datastructure._id, {$set: {logging: true}}).exec();
				return 'ok'
			}).then(val => res.send(val), next);
		})
		.description = {
			get: 'Enables logging for this data structure'
		};

	router.route('/:namespace/:id/log/disable')
		.get(auth('read/write'), (req, res, next) => {
			co(function * () {
				var datastructure = yield utils.getDatastructure(req.params.namespace, req.params.id);
				yield Datastructures.findByIdAndUpdate(datastructure._id, {$set: {logging: false}}).exec();
				return 'ok'
			}).then(val => res.send(val), next);
		})
		.description = {
			get: 'Disables logging for this data structure'
		};

	router.route('/:namespace/:id/log')
		.get(auth('read'), (req, res, next) => {
			var props = {};

			if (req.query.sort) {
				props.sort = {};
				if (req.query.sort.indexOf('-') != -1)
					props.sort[req.query.sort.substring(1)] = -1;
				else
					props.sort[req.query.sort] = 1;
			}

			var filter = {
				namespace: req.params.namespace, 
				datastructureId: req.params.id,
			};
			
			if (req.query.ltDate)
				filter[date] = {$lte: req.query.ltDate};

			Log.find(filter, req.query.q, {})
				.limit(req.query.limit)
				.populate('userId', 'email -_id')
				.exec((err, log) => {
					if (err) return next(err);
					res.json(log);
				});
		})
		.description = {
			get: {
				def: 'Retrieves the entire log of the datastructure.',
				query: {
					sort: 'Sort the logs according to some param e.g. name / -name',
					q: 'Query the data',
					limit: 'Limits the data'
				}
			}
		};
};