'use strict';

var co = require('co'),
	auth = require('../auth'),
	VersionedTrie = require('../../data_structure/VersionedTrie'),
	VersionedGraph = require('../../data_structure/VersionedGraph'),
	keyValueDB = require('../../config/leveldb')(),
	utils = require('../utils'),
	error = require('../../config/error'),

	Datastructures = require('../../schema/Datastructure'),
	Access = require('../../schema/Access'),
	User = require('../../schema/User'),
	Tag = require('../../schema/Tag');

// Note that these routes are documented in the description section of each route.
module.exports = function (router) {
	require('./export-import')(router);
	/**
	* For manipulating the data structures within a namespace
	*/
	router.route('/:namespace/:datastructureId')
		.post(auth('owner'), (req, res, next) => {
			co(function *() {
				if (!req.body.type) throw error.API('No datastructure type given.');
				if (['map', 'graph'].indexOf(req.body.type) === -1) {
					throw error.API('Data structure type of "'+req.body.type+'" is incorrect');
				}

				var datastructure = yield utils.createDatastructure(req.params.namespace, req.params.datastructureId, req.body.type);

				// Create version 0 of the data structure
				let type = req.body.type.toLowerCase();
				if (type == 'map') {
					yield VersionedTrie(datastructure._id, keyValueDB, datastructure._id).create();
				} else if (type == 'graph') {
					yield VersionedGraph(datastructure._id, keyValueDB, null, datastructure._id).create();
				}

				return 'ok';
			}).then(val => res.send(val), next); // next function for error handling
		})
		.get((req, res, next) => {
			co(function *() {
				
				return yield Datastructures.findOne({
					namespace: req.params.namespace, 
					datastructureId: req.params.datastructureId
				}).exec();

			}).then((val) => {
				res.send(val);
			}, next);
		})
		// TEMPORARY!!!
		.delete((req, res, next) => {
			co(function *() {
				yield Datastructures.remove({
					namespace: req.params.namespace,
					datastructureId: req.params.datastructureId
				}).exec();
				return 'ok';
			}).then((val) => {
				res.send(val);
			}, next);
		})
		.description = {
			'info': 'For managing the data structures within the namespace.',
			'post': 'Create a new data structure.',
			'get': 'Retrieve some data structure.'
		};

	router.route('/datastructures')
		.get((req, res, next) => {
			co(function* () {
				if (!req.jwt.userId) throw error.AUTH('User is not signed in.');
				
				var datastructures = {};
				
				var user = yield User.findById(req.jwt.userId).exec();

				datastructures[user.namespace] = yield Datastructures.find({namespace: user.namespace}, 'namespace datastructureId type').
				lean().exec();

				// Add admin properties
				for (let doc in datastructures[user.namespace]) {
					datastructures[user.namespace][doc].access = 'admin';
				}


				// Retrieve the documents the user has access to
				var docs = yield Access.find({_user: req.jwt.userId}).populate('_ds').lean().exec();

				// Setup the different namespaces
				for (let doc of docs) {
					doc._ds.access = doc.type;
					if (datastructures[doc.namespace])
						datastructures[doc.namespace].push(doc._ds);
					else
						datastructures[doc.namespace] = [doc._ds];
				}

				return datastructures;
			}).then(val=>res.json(val), next);
		})
		.description = {
			'get': 'Retrieve all data structures for signed in user'
		};

	router.route('/:namespace').get((req, res, next) => {
		Datastructures.find({
			namespace: req.params.namespace
		}, 'namespace datastructureId type', (err, docs) => {
			if (err) return next(err);
			res.send(docs);
		});
	}).description = {
		'get': 'Retrieve all data structures'
	};

	/**
	 * For merging two data structures.
	 */
	router.route('/:namespace/:datastructureId/merge')
		.put((req, res, next) => {
			co(function *() {
				var datastructure = yield utils.getDatastructure(req.params.namespace, req.params.datastructureId);
				return {
					version: yield VersionedTrie(datastructure.forkID || datastructure._id, keyValueDB, datastructure._id)
									.merge(req.body.versionA, req.body.versionB)
				};
			}).then(val => res.send(val), next);
		})
		.description = {
			'put': 'Combines two separate versions of the same data structure. Expects {versionA: <versionA>, versionB: <versionB>}'
		};

	/**
	 * For getting the history of the data structure.
	 */
	router.route('/:namespace/:datastructureId/history')
		.get((req, res, next) => {
			co(function * () {
				var datastructure = yield utils.getDatastructure(req.params.namespace, req.params.datastructureId);
				return VersionedTrie(datastructure.forkID || datastructure._id, keyValueDB, datastructure._id).versionHistory;
			}).then(val => {
				var level = req.query.level;
				val.get((err, DAG) => {
					if (err) return next(err);
					res.json(DAG);
				}, level || null);
			}, next);
		})
		.description = {
			'get': 'Retrieve the history of the data structure.'
		};

	router.route('/:namespace/:datastructureId/tag')
		.get(auth('read'), (req, res, next) => {
			Tag.find({
				namespace: req.params.namespace,
				datastructureId: req.params.namespace
			}, function (err, tags) {
				if (err) return next(err);
				res.json(tags);
			});
		});

	router.route('/:namespace/:datastructureId/tag/:version/name')
		.get(auth('read'), function (req, res, next) {
			Tag.findOne({
				datastructureId: req.params.datastructureId,
				namespace: req.params.namespace,
				version: req.params.version
			}, 'name', function (err, name) {
				if (err) return next(err);
				res.send(name);
			});
		});

	router.route('/:namespace/:datastructureId/tag/:tagname')
		.get(auth('read'), function (req, res, next) {
			Tag.findOne({
				datastructureId: req.params.datastructureId,
				namespace: req.params.namespace,
				name: req.params.tagname
			}, 'version', function (err, version) {
				if (err) return next(err);
				res.send(version);
			});
		})
		.post(function (req, res, next) {
			Tag.findOne({
				datastructureId: req.params.datastructureId,
				namespace: req.params.namespace,
				name: req.params.tagname
			}, function (err, tag) {
				if (err) return next(err);
				
				function response(err, done) {
					if (err) return next(err);
					res.send("ok");
				}

				if (!tag) {
					Tag.create({
						datastructureId: req.params.datastructureId,
						namespace: req.params.namespace,
						name: req.params.tagname,
						version: req.body.version
					}, response);
				} else {
					Tag.findByIdAndUpdate(tag._id, {$set: {version: req.body.version}}, response);
				}
				
			});
		})
		.delete(function (req, res, next) {
			Tag.remove({
				namespace: req.params.namespace,
				datastructureId: req.params.datastructureId,
				name: req.params.tagname
			}, function (err, done) {
				if (err) return next(err);
				res.send("ok");
			});
		})
		.description = {
			get: 'Retrieve the version for the tag name',
			post: 'Insert a new tag name and version pair'
		};

	router.route('/:namespace/:datastructureId/diff/:versionA/:versionB')
		.all(auth('read'), function (req, res, next) {
			co(function *() {
				var datastructure = yield utils.getDatastructure(req.params.namespace,
																	req.params.datastructureId);

				var diff = yield VersionedTrie(datastructure.forkID || datastructure._id, 
												keyValueDB, 
												datastructure._id)
									.diff(req.params.versionA, req.params.versionB);
				return diff;
			}).then(val => res.send(val), next);
		});

		require('./forking')(router);
		require('./logging')(router);
};