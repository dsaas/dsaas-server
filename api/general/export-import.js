'use strict';

var co = require('co'),
	auth = require('../auth'),
	VersionedTrie = require('../../data_structure/VersionedTrie'),
	VersionedGraph = require('../../data_structure/VersionedGraph'),
	keyValueDB = require('../../config/leveldb')(),
	read = require('co-read'),
	utils = require('../utils'),
	error = require('../../config/error');

// Note that these routes are documented in the description section of each route.
module.exports = function (router) {
	router.route('/:namespace/:id/export')
		.get(auth('read'), (req, res, next) => {
			co(function * () {
				var datastructure = yield utils.getDatastructure(req.params.namespace, req.params.id);
				return datastructure;
			}).then(datastructure => {
				res.writeHead(200, { "Content-Type": "text/x-please-download-me", "Cache-control": "no-cache" });

				var stream = keyValueDB.createReadStream({
					gte: datastructure._id,
					lte: datastructure._id + '~'
				});

				res.write('{');
				res.write('"type": "' + datastructure.type + '",');
				res.write('"data": [');

				stream.on('data', function (data)  {
					res.write(JSON.stringify(data) + ',');
				})
				.on('end', function () {
					res.write('{"done": 1}]}');
					res.end();
				})
				.on('error', next);
			}, next);
		})
		.description = {
			get: 'Exports the entire structure in a certain format'
		};

	router.route('/:namespace/import')
		.post(auth('read'), (req, res, next) => {
			var id = req.body.datastructureId;
			var data = JSON.parse(req.body.data);

			co(function * () {
				if (!id) {
					throw error.API('Please provide a data structure identifier.');
				}
				if (!data) {
					throw error.API('Please provide some data structure to import.');
				}

				var datastructure = yield utils.createDatastructure(req.params.namespace, id, data.type);
				var ops = [];

				for (let idx in data.data) {
					if (data.data[idx].done) break;
					var key = data.data[idx].key;
					let newKey = datastructure._id + key.substring(key.indexOf(keyValueDB.constant.delimeter));
					ops.push({type: 'put', key: newKey, value: data.data[idx].value});
				}

				yield keyValueDB.batch(ops);
			}).then(val => res.send(val), next);
		})
		.description = {
			post: 'Import\'s exported data --- here'
		};
};