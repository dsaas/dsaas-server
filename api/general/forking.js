'use strict';

var co = require('co'),
	auth = require('../auth'),
	keyValueDB = require('../../config/leveldb')(),
	VersionHistory = require('../../data_structure/VersionHistory'),
	utils = require('../utils');

// Note: after the data structure has been forked, at this stage if the user can find the version identifier then they can get access to any changes in the new data structure.
// Therefore there will have to be granular control at the level of the actual data structure.
module.exports = function (router) {
	router.route('/:namespace/:datastructureId/fork')
		.post(auth('read'), (req, res, next) => {
			co(function * () {
				var datastructure = yield utils.getDatastructure(req.params.namespace, req.params.datastructureId);
				var forkID = datastructure.forkID || datastructure._id;

				var forkedDatastructure = yield utils.createDatastructure(req.body.namespace, req.body.datastructureId, datastructure.type, forkID);
				var forkedVersionHistory = VersionHistory(forkedDatastructure._id, keyValueDB);
				
				yield forkedVersionHistory.create();
				return [forkedVersionHistory, datastructure, datastructure.type];
			}).then(objs => {
				objs[0].import(objs[1]._id, function (err) {
					if (err) return next(err);
					res.json({type: objs[2]})
				});
			}, next);
		})
		.description = {
			post: 'Forks the data-structure by creating a copy of it.'
		};
};