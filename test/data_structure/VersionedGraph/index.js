var expect = require('chai').expect;
var util = require('util');
var dbname = './test/data_structure/VersionedGraph/test.db';
var leveldown = require('leveldown');

/* jshint ignore:start */
var VersionedGraph = require('../../../data_structure/VersionedGraph');
var VersionedTrie = require('../../../data_structure/VersionedTrie');

describe('Versioned Graph', function () {
	var db;
	before(function (done) {
		leveldown.destroy(dbname, (err) => {
			if (err) console.log(err);
			db = require('../../../config/leveldb')(dbname);
			done();
		});
	});

	describe('basic functionality testing', function () {
		var graph, trie, v = {0: 0};

		before(function *() {
			trie = VersionedTrie('test1', db, 'test1');
			graph = VersionedGraph('test1', db, trie);
			yield graph.create();
		});

		it('should add a node', function *() {
			let temp = yield graph.addNodes([{
				label: 'eat'
			}], 0);
			v[1] = temp.version;
			expect(temp.nodeIds).to.deep.equal([1]);
			
			expect(yield trie.get(VersionedGraph.STR.NODE + 1, temp.version))
			.to.deep.equal({id: 1, edges: {}, props: {label: 'eat'}});
		});

		it('should add two nodes (batch)', function *() {
			let temp = yield graph.addNodes([{
				label: 'sleep'
			},
			{
				label: 'rave'
			}], v[1]);
			v[2] = temp.version;
			expect(temp.nodeIds).to.deep.equal([2,3]);

			expect(yield trie.get(VersionedGraph.STR.NODE + 1, v[2]))
			.to.deep.equal({id: 1, edges: {}, props: {label: 'eat'}});

			expect(yield trie.get(VersionedGraph.STR.NODE + 2, v[2]))
			.to.deep.equal({id: 2, edges: {}, props: {label: 'sleep'}});			

			expect(yield trie.get(VersionedGraph.STR.NODE + 3, v[2]))
			.to.deep.equal({id: 3, edges: {}, props: {label: 'rave'}});
		});

		it('should add an edge', function *() {
			let temp = yield graph.addEdges([{
				from: 1,
				to: 2,
				label: 'then'
			}], v[2]);

			v[3] = temp.version;
			expect(temp.edgeIds).to.deep.equal(['1-->2']);

			expect(yield trie.get(VersionedGraph.STR.EDGE + '1-->2', v[3]))
			.to.deep.equal({id: '1-->2', from: 1, to: 2, label: 'then'});
		});

		it('should add two edges (batch)', function *() {
			let temp = yield graph.addEdges([{
				from: 2, to: 3, label: 'after which we'
			}, {
				from: 3, to: 1, label: 'repeat!!!'
			}], v[3]);

			v[4] = temp.version;
			expect(temp.edgeIds).to.deep.equal(['2-->3', '3-->1']);

			expect(yield trie.get(VersionedGraph.STR.EDGE + '1-->2', v[4]))
			.to.deep.equal({id: '1-->2', from: 1, to: 2, label: 'then'});

			expect(yield trie.get(VersionedGraph.STR.EDGE + '2-->3', v[4]))
			.to.deep.equal({id: '2-->3', from: 2, to: 3, label: 'after which we'});

			expect(yield trie.get(VersionedGraph.STR.EDGE + '3-->1', v[4]))
			.to.deep.equal({id: '3-->1', from: 3, to: 1, label: 'repeat!!!'});

			expect(yield trie.get(VersionedGraph.STR.NODE + 1, v[4]))
			.to.deep.equal({id: 1, edges: {'1-->2':1, '3-->1':1}, props: {label: 'eat'}});

			expect(yield trie.get(VersionedGraph.STR.NODE + 2, v[4]))
			.to.deep.equal({id: 2, edges: {'1-->2':1, '2-->3':1}, props: {label: 'sleep'}});

			expect(yield trie.get(VersionedGraph.STR.NODE + 3, v[4]))
			.to.deep.equal({id: 3, edges: {'2-->3':1, '3-->1':1}, props: {label: 'rave'}});
		});

		it('should get the nodes correctly', function *() {
			expect(yield graph.getNode(1, v[4]))
			.to.deep.equal({id: 1, edges: {'1-->2':1, '3-->1':1}, props: {label: 'eat'}});

			expect(yield graph.getNode(2, v[4]))
			.to.deep.equal({id: 2, edges: {'1-->2':1, '2-->3':1}, props: {label: 'sleep'}});

			expect(yield graph.getNode(3, v[4]))
			.to.deep.equal({id: 3, edges: {'2-->3':1, '3-->1':1}, props: {label: 'rave'}});
		});

		it('should get the edges correctly', function *() {
			expect(yield graph.getEdge('1-->2', v[4]))
			.to.deep.equal({id: '1-->2', from: 1, to: 2, label: 'then'});

			expect(yield graph.getEdge('2-->3', v[4]))
			.to.deep.equal({id: '2-->3', from: 2, to: 3, label: 'after which we'});

			expect(yield graph.getEdge('3-->1', v[4]))
			.to.deep.equal({id: '3-->1', from: 3, to: 1, label: 'repeat!!!'});

		});

		it('should call adjacent correctly', function *() {
			var node = yield [
				graph.getNode(1, v[4]), 
				graph.getNode(2, v[4]),
				graph.getNode(3, v[4])
			];

			expect(yield graph.adjacent(1, v[4])).to.deep.equal({
				2: node[1],
				3: node[2]
			});
			expect(yield graph.adjacent(2, v[4])).to.deep.equal({
				1: node[0],
				3: node[2]
			});
			expect(yield graph.adjacent(3, v[4])).to.deep.equal({
				1: node[0],
				2: node[1]
			});
		});

		it('should call parents correctly', function *() {
			var node = yield [
				graph.getNode(1, v[4]), 
				graph.getNode(2, v[4]),
				graph.getNode(3, v[4])
			];

			expect(yield graph.parents(1, v[4])).to.deep.equal({
				3: node[2]
			});
			expect(yield graph.parents(2, v[4])).to.deep.equal({
				1: node[0]
			});
			expect(yield graph.parents(3, v[4])).to.deep.equal({
				2: node[1]
			});
		});

		it('should call children correctly', function *() {
			var node = yield [
				graph.getNode(1, v[4]), 
				graph.getNode(2, v[4]),
				graph.getNode(3, v[4])
			];

			expect(yield graph.children(1, v[4])).to.deep.equal({
				2: node[1]
			});
			expect(yield graph.children(2, v[4])).to.deep.equal({
				3: node[2]
			});
			expect(yield graph.children(3, v[4])).to.deep.equal({
				1: node[0]
			});
		});

		it('should call all the edges', function * () {
			var edges = yield graph.allEdges(v[4]);
			expect(edges).to.deep.equal([
					'1-->2',
					'2-->3',
					'3-->1'
				]);
		});

		it('should call all the nodeIds', function * () {
			var nodeIds = yield graph.allNodeIds(v[4]);
			expect(nodeIds).to.deep.equal([
					'1',
					'2',
					'3'
				]);
		});

		it('should update two nodes correctly (batch)', function *() {
			v[5] = yield graph.updateNodes({
				1: {label: 'eating'},
				2: {label: 'sleeping'}
			}, v[4]);

			expect(yield graph.getNode(1, v[5])).to.deep.equal({
				id: 1,
				edges: {'1-->2':1, '3-->1':1},
				props: {label: 'eating'}
			});

			expect(yield graph.getNode(2, v[5])).to.deep.equal({
				id: 2,
				edges: {'1-->2':1, '2-->3':1},
				props: {label: 'sleeping'}
			});
		});

		it('should update two edges correctly (batch)', function *() {
			v[6] = yield graph.updateEdges({
				'1-->2': {label: 'thus'},
				'3-->1': {label: 'rave'}
			}, v[5]);

			expect(yield graph.getEdge('3-->1', v[6])).to.deep.equal({
				id: '3-->1',
				from: 3,
				to: 1,
				label: 'rave'
			});

			expect(yield graph.getEdge('1-->2', v[6])).to.deep.equal({
				id: '1-->2',
				from: 1,
				to: 2,
				label: 'thus'
			});
		});

		it('should remove an edge correctly', function *() {
			v[7] = yield graph.removeEdges(['1-->2'], v[6]);
			// This should be 7, but this is a feature that needs to be added
			expect(yield graph.getEdge('1-->2', v[7])).to.be.null;

		});

		it('should remove a node correctly', function *() {
			v[8] = yield graph.removeNodes([1], v[6]);

			expect(yield graph.getNode(1, v[8])).to.equal.null;
			expect(yield graph.getEdge('1-->2', v[8])).to.equal.null;
			expect(yield graph.getEdge('3-->1', v[8])).to.equal.null;
			expect((yield graph.getEdge('2-->3', v[8])).id).to.equal('2-->3');
			expect((yield graph.getNode(2, v[8])).id).to.equal(2);
			expect((yield graph.getNode(3, v[8])).id).to.equal(3);
		});

		it('should remove two nodes correctly (batch)', function *(){
			v[9] = yield graph.removeNodes([2,3], v[6]);
			expect(yield graph.getNode(2, v[9])).to.be.null;
			expect(yield graph.getNode(3, v[9])).to.be.null;
			expect(yield graph.getEdge('1-->2', v[9])).to.be.null;
			expect(yield graph.getEdge('2-->3', v[9])).to.be.null;
			expect(yield graph.getEdge('3-->1', v[9])).to.be.null;
			expect((yield graph.getNode(1, v[9]))).to.deep.equal({id: 1, edges:{}, props: {label: 'eating'}});
		});

		it('should remove two edges correctly (batch)', function *() {
			v[10] = yield graph.removeEdges(['2-->3','3-->1'], v[6]);
			expect(yield graph.getNode(1, v[10])).to.deep.equal({id: 1, edges:{'1-->2':1}, props: {label: 'eating'}});
			expect(yield graph.getNode(2, v[10])).to.deep.equal({id: 2, edges:{'1-->2':1}, props: {label: 'sleeping'}});
			expect(yield graph.getNode(3, v[10])).to.deep.equal({id: 3, edges:{}, props: {label: 'rave'}});
			expect(yield graph.getEdge('1-->2', v[10])).to.not.be.null;
			expect(yield graph.getEdge('2-->3', v[10])).to.be.null;
			expect(yield graph.getEdge('3-->1', v[10])).to.be.null;
		});
	});

	describe('small test', function () {
		var graph, trie, v = {0: 0}, temp;

		before(function *() {
			trie = VersionedTrie('test2', db, 'test2');
			graph = VersionedGraph('test2', db, trie);
			yield graph.create();
		});

		it('should add two nodes', function* () {
			temp = yield graph.addNodes([{label: 'node 1'}], 0, ['node1']);
			temp = yield graph.addNodes([{label: 'node 2'}], temp.version, ['node2']);
			expect(yield graph.getNode('node1', temp.version)).to.deep.equal({id: 'node1', edges: {}, props: {label: 'node 1'}});
			expect(yield graph.getNode('node2', temp.version)).to.deep.equal({id: 'node2', edges: {}, props: {label: 'node 2'}});
		});

		it('should add a directed link between the two nodes', function* () {
			var edge = {label: 'kills'};
			edge.from = 'node1';
			edge.to = 'node2';
			temp = yield graph.addEdges([edge], temp.version);

			expect(yield graph.adjacent('node1', temp.version)).to.deep.equal({'node2': {id: 'node2', edges: {'node1-->node2': 1}, props: {label: 'node 2'}}});
			expect(yield graph.adjacent('node2', temp.version)).to.deep.equal({'node1': {id: 'node1', edges: {'node1-->node2': 1}, props: {label: 'node 1'}}});
		});
	});

	after(function (done) {
		db.close((err) => {
			if (err) console.log(err);
			leveldown.destroy(dbname, (err) => {
				if (err) console.log(err);
				done();		
			});
		});
	});
});