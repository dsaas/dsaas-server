'use strict';

var expect = require('chai').expect;
var util = require('util');
var dbname = './test/data_structure/VersionedTrie/test.db';
var leveldown = require('leveldown');

var VersionedTrie = require('../../../data_structure/VersionedTrie');


describe('Versioned Trie', function () {
	var db;

	before(function (done) {
		leveldown.destroy(dbname, (err) => {
			if (err) console.log(err);
			db = require('../../../config/leveldb')(dbname);
			// drop(done);
			done();
		});
	});


	describe('testing trie behaviour', function () {
		var trie, v = {};
		before(function () {
			trie = VersionedTrie('test1', db, 'test1');
		});

		it('should properly create the trie', function*() {
			yield trie.create();
		});

		it('should share the same nodes', function*() {
			v[1] = yield trie.put({'names': 'pierre'}, 0);
			v[3] = yield trie.put({'names': 'pierre'}, 0);
			v[4] = yield trie.put({'names': 'pierre'}, 0);
			v[5] = yield trie.put({'names': 'pierre'}, 0);
			expect(v[1] == v[3] && v[3] == v[4] && v[4] == v[5]).to.be.true;
			v[2] = yield trie.put({'named': 'bernard'}, 0);
			expect(yield trie.get('names', v[1])).to.equal('pierre');
			expect(yield trie.get('named', v[2])).to.equal('bernard');
		});

		it('should have the right keys', function*() {
			expect(yield trie.keysOrValues(v[2])).to.deep.equal(['named']);
			expect(yield trie.keysOrValues(v[1])).to.deep.equal(['names']);
		});
	});

	describe('small test', function () {
		var trie, v = {};
		before(function* () {
			trie = VersionedTrie('test2', db, 'test2');
			yield trie.create();
		});

		it('should put one item correctly', function* () {
			/**
			 * 1) 
			 * as->a
			 */
			v[1] = yield trie.put({'as': 'a'}, 0);
			expect(yield trie.get('as', v[1])).to.equal('a');
			expect(yield trie.get('as', 0)).to.be.null;
		});

		it('should put another item correctly on the next version', function* () {
			/**
			 * 2) 
			 * as->a
			 * be->b
			 */
			v[2] = yield trie.put({'be': 'b'}, v[1]);
			expect(yield trie.get('be', v[2])).to.equal('b');
			expect(yield trie.get('as', v[2])).to.equal('a');
			expect(yield trie.get('be', v[1])).to.be.null;
			expect(yield trie.get('be', 0)).to.be.null;
		});

		it('should have full persistence', function* () {
			/**
			 * 3)
			 * cde->c
			 */
			v[3] = yield trie.put({'cde': 'c'}, 0);
			expect(yield trie.get('cde', v[3])).to.equal('c');
			expect(yield trie.get('as', v[3])).to.be.null;
			expect(yield trie.get('be', v[3])).to.be.null;
		});

		it('should have full persistence', function* () {
			/**
			 * 4)
			 * as->a
			 * abc->ABC
			 */
			v[4] = yield trie.put({'abc': 'ABC'}, v[1]);
			expect(yield trie.get('abc', v[4])).to.equal('ABC');
			expect(yield trie.get('as', v[4])).to.equal('a');
			expect(yield trie.get('be', v[4])).to.be.null;
			expect(yield trie.get('cde', v[4])).to.be.null;
		});

		it('should have full persistence', function* () {
			/**
			 * 5)
			 * as->a
			 * boom->kaas
			 */
			v[5] = yield trie.put({'boom': 'kaas'}, v[1]);
			expect(yield trie.get('boom', v[5])).to.equal('kaas');
		});

		it('should have confluent persistence', function* () {
			/**
			 * 6)
			 * as->a
			 * be->b
			 * abc->ABC
			 */
			v[6] = yield trie.merge(v[4], v[2]);
			expect(yield trie.get('boom', v[6])).to.be.null;
			expect(yield trie.get('as', v[6])).to.equal('a');
			expect(yield trie.get('be', v[6])).to.equal('b');
			expect(yield trie.get('abc', v[6])).to.equal('ABC');
			/**
			 * 7)
			 * as->a
			 * be->b
			 * abc->ABC
			 * cde->c
			 */
			v[7] = yield trie.merge(v[6], v[3]);
			expect(yield trie.get('cde', v[7])).to.equal('c');
			expect(yield trie.get('as', v[7])).to.equal('a');
			expect(yield trie.get('be', v[7])).to.equal('b');
			expect(yield trie.get('abc', v[7])).to.equal('ABC');
			/**
			 * 8)
			 * as->a
			 * be->b
			 * abc->ABC
			 * cde->c
			 * boom->kaas
			 */
			v[8] = yield trie.merge(v[5], v[7]);
			expect(yield trie.get('boom', v[8])).to.equal('kaas');
			expect(yield trie.get('cde', v[8])).to.equal('c');
			expect(yield trie.get('abc', v[8])).to.equal('ABC');
			expect(yield trie.get('be', v[8])).to.equal('b');
			expect(yield trie.get('as', v[8])).to.equal('a');
		});

		it('should not create different versions for the same states', function* () {
			var temp = yield trie.put({'as': 'a'}, 0);
			expect(temp == v[1]).to.be.true;
			temp = yield trie.put({'be': 'b'}, temp);
			expect(temp == v[2]).to.be.true;
			temp = yield trie.put({'abc': 'ABC'}, temp);
			expect(temp == v[6]).to.be.true;
			temp = yield trie.put({'cde': 'c'}, temp);
			expect(temp == v[7]).to.be.true;
			v[9] = yield trie.put({'boom': 'kaas'}, temp);
			expect(v[9] == v[8]).to.be.true;

		});

		it('should calculate the diff between versions correctly', function* () {
			expect(yield trie.diff(v[0], v[1])).to.deep.equal('+ as: a\n');
			expect(yield trie.diff(v[1], v[0])).to.deep.equal('- as: a\n');
			expect(yield trie.diff(v[1], v[2])).to.deep.equal('+ be: b\n');
			expect(yield trie.diff(v[1], v[3])).to.deep.equal('+ cde: c\n- as: a\n');
			expect(yield trie.diff(v[6], v[7])).to.deep.equal('+ cde: c\n');
			expect(yield trie.diff(v[7], v[8])).to.deep.equal('+ boom: kaas\n');

			expect(yield trie.diff(v[2], v[1])).to.deep.equal('- be: b\n');
			expect(yield trie.diff(v[3], v[1])).to.deep.equal('+ as: a\n- cde: c\n');
			expect(yield trie.diff(v[7], v[6])).to.deep.equal('- cde: c\n');
			expect(yield trie.diff(v[8], v[7])).to.deep.equal('- boom: kaas\n');
		});
	});

	describe('butterfly test 1 - confluence persistence', function () {
		/*
		  |-----1---|
		  |			9 ------|
		  |-----2---|		11-----
		0-|			  |-----|  	  |
		  |-------3   6 		  12
		  |        \ /  		  |
		  |			5 ---7--|	  |
		  |		   / \		10-----
		  |-------4	  8 ----|
		 */
		var trie, v = {0: 0};
		before(function* () {
			trie = VersionedTrie('test3', db, 'test3');
			yield trie.create();
		});

		it('should add all the nodes in the predefined way', function* () {
			v[1] = yield trie.put({'aa': 1}, v[0]);
			v[2] = yield trie.put({'bb': 2}, v[0]);
			v[3] = yield trie.put({'cc': 3}, v[0]);
			v[4] = yield trie.put({'dd': 4}, v[0]);

			v[5] = yield trie.merge(v[4], v[3]);

			v[6] = yield trie.put({'ee': 5}, v[5]);
			v[7] = yield trie.put({'ff': 6}, v[5]);
			v[8] = yield trie.put({'gg': 7}, v[5]);

			v[9] = yield trie.merge(v[2], v[1]);
			v[10] = yield trie.merge(v[8], v[7]);
			v[11] = yield trie.merge(v[9], v[6]);
			v[12] = yield trie.merge(v[11], v[10]);
		});

		it('should have contain ``aa`` and nothing else in version 1', function* () {
			var list = ['bb', 'cc', 'dd', 'ee', 'ff', 'gg'];
			expect(yield trie.get('aa', v[1])).to.equal(1);
			for (var l in list) {
				l = list[l];
				expect(yield trie.get(l, v[1])).to.be.null;
			}
		});

		it('should contain ``bb`` and nothing else in version 2', function* () {
			var list = ['aa', 'cc', 'dd', 'ee', 'ff', 'gg'];
			expect(yield trie.get('bb', v[2])).to.equal(2);
			for (var l in list) {
				l = list[l];
				expect(yield trie.get(l, v[2])).to.be.null;
			}
		});

		it('should contain everything correctly in version 12', function* () {
			var i = 1;
			var list = ['aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg'];

			for (var l in list) {
				l = list[l];
				expect(yield trie.get(l, v[12])).to.equal(i++);
			}
		});
	});

	describe('bigger test', function () {
		var trie, v = {0: 0};
		before(function* () {
			trie = VersionedTrie('test4', db, 'test4');
			yield trie.create();
		});

		it('should put multiple items correctly', function* () {
			var map = {
				'a': ['a', 1, 1],
				'api': ['b', 2, 2],	
				'kaas': ['c', 3, 3],
				'blaas': ['d', 4, 4],
				'boom': ['e', 1, 5],
				'eren': ['ie', 6, 6],
				'kalli': ['f', 7, 7],
				'messa': ['g', 1, 8]
			};


			for (var key in map) {
				var obj = {};
				obj[key] = map[key][0];
				v[map[key][2]] = yield trie.put(obj, v[map[key][1] - 1]);
			}

			for (var key in map) {
				expect(yield trie.get(key, v[map[key][2]])).to.equal(map[key][0]);
			}

		});
	});

	describe('immutable object in trie test', function () {
		var trie, v = {0: 0};
		before(function* () {
			trie = VersionedTrie('test5', db, 'test5');
			yield trie.create();
		});

		it('should add a few immutable objects', function* () {
			v[1] = yield trie.put({'aa': {'a': 'a'}}, v[0]);
			v[2] = yield trie.put({'bb': {'b': 'b'}}, v[1]);
			v[3] = yield trie.put({'cc': {'c': 'c'}}, v[2]);
		});

		it('should get the immutable objects', function* () {
			expect(yield trie.get('aa', v[3])).to.deep.equal({'a': 'a'});
			expect(yield trie.get('bb', v[3])).to.deep.equal({'b': 'b'});
			expect(yield trie.get('cc', v[3])).to.deep.equal({'c': 'c'});
		});

		it('should be fully persistent', function* () {
			v[4] = yield trie.put({'aa': {'aa': 'aa'}}, v[0]);
			/* 5)
			 *  bb:{b:b}
			 *  cc:{c:c}
			 *  aa: {aaa: aaa}
			 */
			v[5] = yield trie.put({'aa': {'aaa': 'aaa'}}, v[3]);
			expect(yield trie.get('aa', v[4])).to.deep.equal({'aa': 'aa'});
			expect(yield trie.get('aa', v[5])).to.deep.equal({'aaa': 'aaa'});
		});

		it('should be confluently persistent', function* () {
			v[6] = yield trie.merge(v[5], v[4]);
			expect(yield trie.get('aa', v[6])).to.deep.equal({'aaa': 'aaa'});
			v[7] = yield trie.merge(v[4], v[5]);
			// expect(yield trie.get('aa', v[7])).to.deep.equal({'aa': 'aa'});
			v[8] = yield trie.merge(v[3], v[4]);
			expect(yield trie.get('aa', v[8])).to.deep.equal({'a': 'a'});
		});
	});

	describe('numbers as keys', function () {
		var trie, v = {0: 0};
		
		before(function* () {
			trie = VersionedTrie('test6', db, 'test6');
			yield trie.create();
		});

		it('should add the numbers as keys', function* () {
			v[1] = yield trie.put({0: 0}, v[0]);
			expect(yield trie.get(0,v[1])).to.equal(0);

			v[2] = yield trie.put({1: 1}, v[1]);
			expect(yield trie.get(1,v[2])).to.equal(1);

			v[3] = yield trie.put({2: 2}, v[2]);
			expect(yield trie.get(2,v[3])).to.equal(2);

			v[4] = yield trie.put({3: 3}, v[3]);
			expect(yield trie.get(3,v[4])).to.equal(3);

			v[5] = yield trie.put({4: 4}, v[4]);
			expect(yield trie.get(4,v[5])).to.equal(4);

		});

		it('should retrieve all the keys correctly', function* () {
			expect(yield trie.keysOrValues(v[5])).to.deep.equal(['0', '1', '2', '3', '4']);
		});
	});

	describe('removal tests', function () {

		var trie, v = {0: 0};

		before(function* () {
			trie = VersionedTrie('test7', db, 'test7');
			yield trie.create();
		});

		it('should add keys [a, b] and [c, d] (batch)', function* () {
			v[1] = yield trie.put({'a': 'a', 'b': 'b'}, v[0]);
			expect(yield trie.get('a', v[1])).to.equal('a');
			expect(yield trie.get('b', v[1])).to.equal('b');

			v[2] = yield trie.put({'c': 'c', 'd': 'd'}, v[1]);
			expect(yield trie.get('a', v[1])).to.equal('a');
			expect(yield trie.get('b', v[1])).to.equal('b');
			expect(yield trie.get('a', v[2])).to.equal('a');
			expect(yield trie.get('b', v[2])).to.equal('b');

			expect(yield trie.get('c', v[1])).to.be.null;
			expect(yield trie.get('d', v[1])).to.be.null;

			expect(yield trie.get('c', v[2])).to.equal('c');
			expect(yield trie.get('d', v[2])).to.equal('d');

		});

		it('should remove keys "a" and "c" correctly', function *() {
			v[3] = yield trie.remove(['a', 'c'], v[2]);
			expect(yield trie.keysOrValues(v[3])).to.deep.equal(['b', 'd']);
		});

		it('should remove key "b" correctly', function *() {
			v[4] = yield trie.remove(['b'], v[1]);
			expect(yield trie.keysOrValues(v[4])).to.deep.equal(['a']);
		});

		it('should remove all characters correctly', function *() {
			v[5] = yield trie.remove(['a', 'b', 'c', 'd'], v[2]);
			expect(yield trie.keysOrValues(v[5])).to.deep.equal([]);
		});

		it('should have the same version for the same states', function* () {
			expect(v[5] == v[0]).to.be.true;
			let temp = yield trie.put({'b': 'b', 'd': 'd'}, v[0]);
			expect(v[3] == temp).to.be.true;
			temp = yield trie.put({'a': 'a'}, v[0]);
			expect(v[4] == temp).to.be.true;
		});
	});

	describe('removal and merge tests', function () {

		var trie, v = {0: 0};

		before(function* () {
			trie = VersionedTrie('test8', db, 'test8');
			yield trie.create();
		});

		it('should batch add [a, b, c] and [d, e, f]', function* () {
			v[1] = yield trie.put({'a': 'a', 'b': 'b', 'c': 'c'}, 0);
			expect(yield trie.get('a', v[1])).to.equal('a');
			expect(yield trie.get('b', v[1])).to.equal('b');
			expect(yield trie.get('c', v[1])).to.equal('c');

			v[2] = yield trie.put({'e': 'e', 'd': 'd', 'f': 'f'}, 0);
			expect(yield trie.get('d', v[2])).to.equal('d');
			expect(yield trie.get('e', v[2])).to.equal('e');
			expect(yield trie.get('f', v[2])).to.equal('f');

			expect(yield trie.get('a', v[2])).to.be.null;
			expect(yield trie.get('b', v[2])).to.be.null;
			expect(yield trie.get('c', v[2])).to.be.null;

			expect(yield trie.get('d', v[1])).to.be.null;
			expect(yield trie.get('e', v[1])).to.be.null;
			expect(yield trie.get('f', v[1])).to.be.null;
		});

		it('should merge 1 and 2', function* () {
			v[3] = yield trie.merge(v[1], v[2]);
			expect(yield trie.get('a', v[3])).to.equal('a');
			expect(yield trie.get('b', v[3])).to.equal('b');
			expect(yield trie.get('c', v[3])).to.equal('c');
			expect(yield trie.get('d', v[3])).to.equal('d');
			expect(yield trie.get('e', v[3])).to.equal('e');
			expect(yield trie.get('f', v[3])).to.equal('f');
		});

		it('should remove "a" from 3 to form 4 and remove ["e", "f", "b"] to form 5', function* () {
			v[4] = yield trie.remove(['a'], v[3]);
			expect(yield trie.get('a', v[4])).to.be.null;
			expect(yield trie.get('b', v[4])).to.equal('b');
			expect(yield trie.get('c', v[4])).to.equal('c');
			expect(yield trie.get('d', v[4])).to.equal('d');
			expect(yield trie.get('e', v[4])).to.equal('e');
			expect(yield trie.get('f', v[4])).to.equal('f');

			v[5] = yield trie.remove(['b', 'e', 'f'], v[3]);
			expect(yield trie.get('a', v[5])).to.equal('a');
			expect(yield trie.get('b', v[5])).to.be.null;
			expect(yield trie.get('c', v[5])).to.equal('c');
			expect(yield trie.get('d', v[5])).to.equal('d');
			expect(yield trie.get('e', v[5])).to.be.null;
			expect(yield trie.get('f', v[5])).to.be.null;
		});

		it('should properly merge 4 and 5 to form 6', function* () {
			v[6] = yield trie.merge(v[4], v[5]);
			expect(yield trie.get('a', v[6])).to.be.null;
			expect(yield trie.get('b', v[6])).to.be.null;
			expect(yield trie.get('c', v[6])).to.equal('c');
			expect(yield trie.get('d', v[6])).to.equal('d');
			expect(yield trie.get('e', v[6])).to.be.null;
			expect(yield trie.get('f', v[6])).to.be.null;
		});

		it('should update "a" in version 3 to form 7', function* () {
			v[7] = yield trie.put({'a': 'aa'}, v[3]);
			expect(yield trie.get('a', v[7])).to.equal('aa');
			expect(yield trie.get('b', v[7])).to.equal('b');
			expect(yield trie.get('c', v[7])).to.equal('c');
			expect(yield trie.get('d', v[7])).to.equal('d');
			expect(yield trie.get('e', v[7])).to.equal('e');
			expect(yield trie.get('f', v[7])).to.equal('f');
		});

		it('should properly merge 6 and 7 to form 8', function* () {
			v[8] = yield trie.merge(v[7], v[6]);
			expect(yield trie.get('a', v[8])).to.equal('aa');
			expect(yield trie.get('b', v[8])).to.be.null;
			expect(yield trie.get('c', v[8])).to.equal('c');
			expect(yield trie.get('d', v[8])).to.equal('d');
			expect(yield trie.get('e', v[8])).to.be.null;
			expect(yield trie.get('f', v[8])).to.be.null;
		});
	});

	describe('all keys retrieval function', function () {
		
		var trie, v = {0: 0};

		before(function* () {
			trie = VersionedTrie('test9', db, 'test9');
			yield trie.create();
		});

		it('should add the key-value pairs in different version', function* () {
			v[1] = yield trie.put({'node1': 'a'}, v[0]);
			v[2] = yield trie.put({'ABCnode2': 'b'}, v[1]);
			v[3] = yield trie.put({'EFGnode3': 'c'}, v[2]);
			v[4] = yield trie.put({'edge1': 'ea'}, v[3]);
			v[5] = yield trie.put({'KKKKRedge2': 'eb'}, v[4]);
			v[6] = yield trie.put({'edge3': 'ec'}, v[5]);
		});

		it('should retrieve only the keys with the substring of "node"', function * () {
			expect(yield trie.keysOrValues(v[6], 
				function (str) { 
					return str.indexOf('node') !== -1; 
				}))
			.to.deep.equal(['node1', 'ABCnode2','EFGnode3']);
		});

		it('should retrieve only the keys with the substring of "edge"', function * () {
			expect(yield trie.keysOrValues(v[6],
				function (str) {
					return str.indexOf('edge') !== -1;
				}))
			.to.deep.equal(['edge1', 'edge3', 'KKKKRedge2']);
		});
	});

	after(function (done) {
		db.close((err) => {
			if (err) console.log(err);
			leveldown.destroy(dbname, (err) => {
				if (err) console.log(err);
				done();
			});
		});
	});
});