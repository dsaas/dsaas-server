'use strict';

var expect = require('chai').expect;
var util = require('util');

var utilities = require('../../../data_structure/utils');

describe('Data Structure Utilities module testing', function () {
	var bitstrings = [];
	it('should generate a few random bit string', function () {
		for (var i = 0; i < 10; i++) {
			bitstrings.push(utilities.randomKeyValueString());
		}
		// console.log(bitstrings[0].length);
	});

	it('should consistently xor the bitstrings together', function () {
		// console.log(utilities.zobristHash(bitstrings[0], bitstrings[1]));
	});
});