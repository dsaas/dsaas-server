var expect = require('chai').expect;
var util = require('util');
var dbname = './test/data_structure/VersionHistory/test.db';
var leveldown = require('leveldown');
var ID = 'testID';

/* jshint ignore:start */
var VersionHistory = require('../../../data_structure/VersionHistory');


describe('Version History', function () {
	var db;
	before(function (done) {
		leveldown.destroy(dbname, (err) => {
			if (err) console.log(err);
			db = require('../../../config/leveldb')(dbname);
			done();
		});
	});

	// Helper function
	function* get(version) {
		return yield db.getJSON(db.__(ID, db.constant.DAG, version));
	}

	describe('normal behaviour', function () {
		var versionHistory;
		before(function* () {
			versionHistory = VersionHistory(ID, db);
		});

		it('should properly create the version history instance', function*() {
			yield versionHistory.create();
			expect(yield get(0)).to.deep.equal({children: {}, parents: {}});
		});

		it('should add child for one parent, children may have multiple parents', function*() {
			/**
			 *
			 * 	 / 3     7 - 9 \
			 * 0 - 2    /        10
			 * 	 \ 1 - 5 - 6 - 8 /
			 * 		 \ 4 /	
			 */
			function addVersionHistory(parent, child) {
				return function (callback) {
					versionHistory.add(parent, child, callback);
				}
			}
			yield addVersionHistory(1, 0);
			yield addVersionHistory(1, 0);
			expect(yield get(1)).to.deep.equal({parents: {0: 1}, children: {}});

			yield addVersionHistory(2, 0);
			expect(yield get(2)).to.deep.equal({parents: {0: 1}, children: {}});

			yield addVersionHistory(3, 0);
			expect(yield get(3)).to.deep.equal({parents: {0: 1}, children: {}});

			yield addVersionHistory(3, 0);
			expect(yield get(3)).to.deep.equal({parents: {0: 1}, children: {}});

			yield addVersionHistory(4, 1);
			expect(yield get(4)).to.deep.equal({parents: {1: 1}, children: {}});

			yield addVersionHistory(5, 1);
			expect(yield get(5)).to.deep.equal({parents: {1: 1}, children: {}});

			yield addVersionHistory(6, 4);
			expect(yield get(6)).to.deep.equal({parents: {4: 1}, children: {}});

			yield addVersionHistory(6, 5);
			expect(yield get(6)).to.deep.equal({parents: {4: 1, 5: 1}, children: {}});

			yield addVersionHistory(7, 5);
			expect(yield get(7)).to.deep.equal({parents: {5: 1}, children: {}});

			yield addVersionHistory(9, 7);
			expect(yield get(9)).to.deep.equal({parents: {7: 1}, children: {}});

			yield addVersionHistory(8, 6);
			expect(yield get(8)).to.deep.equal({parents: {6: 1}, children: {}});

			yield addVersionHistory(10, 8);
			yield addVersionHistory(10, 9);
			expect(yield get(10)).to.deep.equal({parents: {8: 1, 9: 1}, children: {}});
		});

		it('should find the correct commonAncestor of two nodes', function* () {
			expect(yield versionHistory.commonAncestor(2,3)).to.equal('0');
			expect(yield versionHistory.commonAncestor(1,3)).to.equal('0');
			expect(yield versionHistory.commonAncestor(0,3)).to.equal('0');
			expect(yield versionHistory.commonAncestor(3,0)).to.equal('0');
			expect(yield versionHistory.commonAncestor(4,5)).to.equal('1');
			expect(yield versionHistory.commonAncestor(7,6)).to.equal('5');
			expect(yield versionHistory.commonAncestor(5,6)).to.equal('5');
			expect(yield versionHistory.commonAncestor(10,6)).to.equal('6');
			expect(yield versionHistory.commonAncestor(8,9)).to.equal('5');
		});

		it('should return if the version exists or not', function * () {
			expect(yield versionHistory.exists(2)).to.be.true;
			expect(yield versionHistory.exists(2000)).to.be.false;
		});

		it('should retrieve the entire history', done => {
			versionHistory.get((err, history) => {
				expect(err).to.be.null;
				expect(history).to.deep.equal({
					0: {
						children: ['1','2','3'],
						parents: []
					},
					1: {
						children: ['4', '5'],
						parents: ['0']
					},
					2: { 
						children: [],
						parents: ['0']
					},
					3: {
						children: [],
						parents: ['0']
					},
					4: {
						children: ['6'],
						parents: ['1']
					},
					5: {
						children: ['6','7'],
						parents: ['1']
					},
					6: {
						children: ['8'],
						parents: ['4', '5']
					},
					7: {
						children: ['9'],
						parents: ['5']
					},
					8: {
						children: ['10'],
						parents: ['6']
					},
					9: {
						children: ['10'],
						parents: ['7']
					},
					10: {
						children: [],
						parents: ['8', '9']
					}
				});
				done();
			});
		});

		var newVersionHistory;
		it('should create the a new history', function * () {
			newVersionHistory = VersionHistory('testID2', db);
			yield newVersionHistory.create();
		});

		it('should import the entire version history', done => {
			newVersionHistory.import(ID, err => {
				expect(err).to.be.null;
				done();
			});
		});

		it('should have the same version histories', done => {
			versionHistory.get((err, history) => {
				expect(err).to.be.null;
				newVersionHistory.get((err, history2) => {
					expect(err).to.be.null;
					expect(history2).to.deep.equals(history);
					done();
				});
			});
		});
	});

	after(function (done) {
		db.close((err) => {
			if (err) console.log(err);
			leveldown.destroy(dbname, (err) => {
				if (err) console.log(err);
				done();		
			});
		});
	});
});