var request = require('supertest');
var expect = require('chai').expect;

module.exports = url => {
	describe('Graph', () => {
		
		var alice = require('../mock').alice;
		var v = {0: 0};
		it('should log alice in to get the token', done => {
			request(url).post('/api/user/signin')
			.send({email: alice.email, password: alice.password})
			.end((err, res) => {
				alice.token = res.text;
				done();
			});
		});

		it('should create a new graph', done => {
			request(url).post('/api/wonderland/legraph')
				.send({type: 'graph'})
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({});
					
					var datastructures = require('../../../schema/Datastructure');

					datastructures.findOne({namespace: 'wonderland', datastructureId: 'legraph'}, (err, doc) => {
						expect(doc.namespace).to.equal('wonderland');
						expect(doc.datastructureId).to.equal('legraph');
						expect(doc.type).to.equal('graph');
						expect(doc.forkID).to.be.null;
						expect(doc.preferences).to.be.null;
					});
					done();
			  });
		});

		it('should add a new node', done => {
			request(url).post('/api/wonderland/legraph/0/graph/node')
				.send({label: 'my favourite node'})
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.id).to.deep.equal(1);
					v[1] = res.body.version;
					done();
				});
		});

		it('should retrieve the node', done => {
			request(url).get('/api/wonderland/legraph/'+v[1]+'/graph/node/1')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({
						id: 1,
						edges: {},
						props: {
							label: 'my favourite node'
						}
					});
					done();
				});
		});

		it('should update the node', done => {
			request(url).put('/api/wonderland/legraph/'+v[1]+'/graph/node/1')
				.send({
					label: 'not so favourite',
					extra: 'sauce'
				})
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					v[2] = res.body.version;
					done();
				});
		});

		it('should retrieve the updated node', done => {
			request(url).get('/api/wonderland/legraph/'+v[2]+'/graph/node/1')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({
						id: 1,
						edges: {},
						props: {
							label: 'not so favourite',
							extra: 'sauce'
						}
					});
					done();
				});
		});

		it('should remove the node', done => {
			request(url).delete('/api/wonderland/legraph/'+v[2]+'/graph/node/1')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					v[3] = res.body.version;
					done();
				});
		});

		it('should not exist in the removed version', done => {
			request(url).get('/api/wonderland/legraph/'+v[3]+'/graph/node/1')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.be.null;
					done();
				});
		});

		it('should add another node and create an edge between the nodes', done => {
			request(url).post('/api/wonderland/legraph/'+v[2]+'/graph/node')
				.send({
					label: 'another node'
				})
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.id).to.equal(2);
					v[4] = res.body.version;
					request(url).post('/api/wonderland/legraph/'+v[4]+'/graph/edge/1/2')
						.send({
							label: 'my favourite edge'
						})
						.set('Authorization', alice.token)
						.end((err, res) => {
							expect(err).to.be.null;
							v[5] = res.body.version;
							expect(res.body.id).to.equal('1-->2');
							done();
						});
				});
		});

		it('should be able to retrieve the new edge', done => {
			request(url).get('/api/wonderland/legraph/'+v[5]+'/graph/edge/1-->2')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({
						id: '1-->2',
						from: '1',
						to: '2',
						label: 'my favourite edge'
					});
					done();
				});
		});

		it('should update the edge', done => {
			request(url).put('/api/wonderland/legraph/'+v[5]+'/graph/edge/1-->2')
				.send({
					label: 'yeah',
					from: 2112,
					to: 234,
					id: 2144
				})
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					v[6] = res.body.version;
					done();
				});
		});

		it('should be able to retrieve the updated edge', done => {
			request(url).get('/api/wonderland/legraph/'+v[6]+'/graph/edge/1-->2')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({
						id: '1-->2',
						from: '1',
						to: '2',
						label: 'yeah'
					});
					done();
				});
		});

		it('should delete the edge', done => {
			request(url).delete('/api/wonderland/legraph/'+v[5]+'/graph/edge/1-->2')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					v[7] = res.body.version;
					done();
				});
		});

		it('should not be able to get the removed edge', done => {
			request(url).get('/api/wonderland/legraph/'+v[7]+'/graph/edge/1-->2')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.be.null;
					done();
				});
		});

		it('should retrieve all available edges', done => {
			request(url).get('/api/wonderland/legraph/'+v[6]+'/graph/edges')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal(['1-->2']);
					done();
				});
		});

		it('should retrieve all available nodes', done => {
			request(url).get('/api/wonderland/legraph/'+v[6]+'/graph/nodes')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal(['1', '2']);
					done();
				});
		});

		it('should retrieve the correct adjacent nodes', done => {
			request(url).post('/api/wonderland/legraph/'+v[6]+'/graph/node')
			.send({label: 'Sir Node the third'}).set('Authorization', alice.token).end((err, res) => {
				expect(err).to.be.null;
				v[8] = res.body.version;
				expect(res.body.id).to.deep.equal(3);

				request(url).post('/api/wonderland/legraph/'+v[8]+'/graph/edge/2/3')
				.send({ label: 'two to three'}).set('Authorization', alice.token).end((err, res) => {
					expect(err).to.be.null;
					v[9] = res.body.version;
					expect(res.body.id).to.equal('2-->3');

					request(url).post('/api/wonderland/legraph/'+v[9]+'/graph/edge/1/3')
					.send({label: 'one to three'}).set('Authorization', alice.token).end((err, res) => {
						expect(err).to.be.null;
						v[10] = res.body.version;
						expect(res.body.id).to.equal('1-->3');

						request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/1/adjacent')
						.set('Authorization', alice.token).end((err, res) => {
							expect(err).to.be.null;
							expect(res.body).to.deep.equal({
								2: {id: 2, edges: {'1-->2':1, '2-->3':1}, props: {label: 'another node'}},
								3: {id: 3, edges: {'2-->3':1, '1-->3':1}, props: {label: 'Sir Node the third'}}
							});

							request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/2/adjacent')
							.set('Authorization', alice.token).end((err, res) => {
								expect(err).to.be.null;
								expect(res.body).to.deep.equal({
									1: {id: 1, edges: {'1-->2':1, '1-->3':1}, props: {label: 'not so favourite', extra: 'sauce'}},
									3: {id: 3, edges: {'2-->3':1, '1-->3':1}, props: {label: 'Sir Node the third'}}
								});

								request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/3/adjacent')
								.set('Authorization', alice.token).end((err, res) => {
									expect(err).to.be.null;
									expect(res.body).to.deep.equal({
										1: {id: 1, edges: {'1-->2':1, '1-->3':1}, props: {label: 'not so favourite', extra: 'sauce'}},
										2: {id: 2, edges: {'1-->2':1, '2-->3':1}, props: {label: 'another node'}}
									});
									done();
								});
							});
						});
					});
				})
			})
		});

		it('should retrieve the correct children nodes', done => {

			request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/1/children')
			.set('Authorization', alice.token).end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.deep.equal({
					2: {id: 2, edges: {'1-->2':1, '2-->3':1}, props: {label: 'another node'}},
					3: {id: 3, edges: {'2-->3':1, '1-->3':1}, props: {label: 'Sir Node the third'}}
				});

				request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/2/children')
				.set('Authorization', alice.token).end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({
						3: {id: 3, edges: {'2-->3':1, '1-->3':1}, props: {label: 'Sir Node the third'}}
					});

					request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/3/children')
					.set('Authorization', alice.token).end((err, res) => {
						expect(err).to.be.null;
						expect(res.body).to.deep.equal({
						});
						done();
					});
				});
			});
		});

		it('should retrieve the correct parent nodes', done => {
			request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/1/parents')
			.set('Authorization', alice.token).end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.deep.equal({
				});

				request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/2/parents')
				.set('Authorization', alice.token).end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({
						1: {id: 1, edges: {'1-->2':1, '1-->3':1}, props: {label: 'not so favourite', extra: 'sauce'}},
					});

					request(url).get('/api/wonderland/legraph/'+v[10]+'/graph/3/parents')
					.set('Authorization', alice.token).end((err, res) => {
						expect(err).to.be.null;
						expect(res.body).to.deep.equal({
							1: {id: 1, edges: {'1-->2':1, '1-->3':1}, props: {label: 'not so favourite', extra: 'sauce'}},
							2: {id: 2, edges: {'1-->2':1, '2-->3':1}, props: {label: 'another node'}}
						});
						done();
					});
				});
			});
		});

	});
};