var request = require('supertest');
var expect = require('chai').expect;

module.exports = (url, docDB) => {
	describe('Map', () => {
		var alice = require('../mock').alice;
		var v = {};

		it('get alice\'s personal token', done => {
			request(url).post('/api/user/signin')
			.send({email: alice.email, password: alice.password})
			.end((err, res) => {
				alice.token = res.text;
				done();
			});
		});

		it('should create a new map', done => {
			request(url).post('/api/wonderland/id')
				.send({type: 'map'})
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({});

					var datastructures = require('../../../schema/Datastructure');

					datastructures.findOne({namespace: 'wonderland', datastructureId: 'id'}, (err, doc) => {
						expect(doc.namespace).to.equal('wonderland');
						expect(doc.datastructureId).to.equal('id');
						expect(doc.type).to.equal('map');
						expect(doc.forkID).to.be.null;
						expect(doc.preferences).to.be.null;
					});
					done();
			  });
		});

		it('should add a key-value pair', done => {
			request(url).post('/api/wonderland/id/0/map/newkey')
				.send({val: 'levalue'})
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.version).to.not.be.null;
					v[1] = res.body.version;
					done();
				});
		});

		it('should retrieve the key-value pair', done => {
			request(url).get('/api/wonderland/id/'+v[1]+'/map/newkey')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({val: 'levalue'});
					done();
				});
		});

		it('should retrieve all the key-value pairs', done => {
			request(url).get('/api/wonderland/id/'+v[1]+'/map/keys')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal(['newkey']);
					done();
				});
		});

		it('should remove the key-value pair', done => {
			request(url).del('/api/wonderland/id/'+v[1]+'/map/newkey')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.version).to.not.be.null;
					v[2] = res.body.version;
					done();
				});
		});

		it('should not contain the key-value pair at the next version', done => {
			request(url).get('/api/wonderland/id/'+v[2]+'/map/newkey')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.be.null;
					done();
				});
		});

		it('should log all the requests', done => {
			request(url).get('/api/wonderland/id/log?q=-__v&sort=date')
			.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.length).to.equal(4);
					expect(res.body[0].datastructureId).to.equal('id');
					expect(res.body[0].namespace).to.equal('wonderland');
					expect(res.body[0].method).to.equal('GET');
					done();
				});
		});

	});
};