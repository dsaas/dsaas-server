var request = require('supertest');
var expect = require('chai').expect;
var async = require('async');

module.exports = url => {
	describe('General Forking', () => {
		
		var alice = require('../mock').alice;
		var id = 'forktest';

		it('should log alice in to get the token', done => {
			request(url).post('/api/user/signin')
			.send({email: alice.email, password: alice.password})
			.end((err, res) => {
				alice.token = res.text;
				done();
			});
		});

		it('should create a new data structure in the given namespace', done => {
			request(url).post('/api/wonderland/' + id)
				.set('Authorization', alice.token)
				.send({type: 'map'})
				.expect(200)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({});
					
					var datastructures = require('../../../schema/Datastructure');
					
					datastructures.findOne({namespace: 'wonderland', datastructureId: id}, (err, doc) => {
						expect(err).to.be.null;
						expect(doc.namespace).to.equal('wonderland');
						expect(doc.datastructureId).to.equal(id);
						expect(doc.type).to.equal('map');
						expect(doc.forkID).to.be.null;
						expect(doc.preferences).to.be.null;
					});
					done();
			  });
		});

		var v = {};
		it('should add versions...', done => {
			async.series([
				cb => request(url).post('/api/wonderland/'+id+'/0/map/a')
				.set('Authorization', alice.token)
				.send({'a': 'a'})
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.version).to.not.be.null;
					v[1] = res.body.version;
					cb();
				}),

				cb => request(url).post('/api/wonderland/'+id+'/'+v[1]+'/map/b')
				.set('Authorization', alice.token)
				.send({'b': 'b'})
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.version).to.not.be.null;
					v[2] = res.body.version;
					cb();
				}),

				cb => request(url).post('/api/wonderland/'+id+'/'+v[1]+'/map/c')
				.set('Authorization', alice.token)
				.send({c: 'c'})
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.version).to.not.be.null;
					v[3] = res.body.version;
					cb();
				}),

				cb => request(url).delete('/api/wonderland/'+id+'/'+v[2]+'/map/b')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.version).to.not.be.null;
					v[4] = res.body.version;
					cb();
				}),

				cb => request(url).put('/api/wonderland/'+id+'/merge')
				.set('Authorization', alice.token)
				.send({versionA: v[1], versionB: v[3]})
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.version).to.not.be.null;
					v[5] = res.body.version;
					cb();
				})
			], err => {
				done();
			});
		});

		it('should fork the data structure', done => {
			async.series([
				cb => request(url).post('/api/wonderland/'+id+'/fork')
				.set('Authorization', alice.token)
				.send({namespace: 'wonderland', datastructureId: 'forktest2'})
				.end((err, res) => {

					expect(err).to.be.null;
					expect(res.body).to.deep.equal({type: 'map'});
					cb();
				}),

				cb => request(url).get('/api/wonderland/forktest2/' + v[5] + '/map/entries')
				.set('Authorization', alice.token)
				.end((err, res) => {

					expect(err).to.be.null;
					expect(res.body).to.deep.equal({a: {a: 'a'}, c: {c: 'c'}});
					cb();
				})

			], err => {
				done();
			});
		});

		it('should compare the version history of the forked and the original data structure', done => {
			async.waterfall([
				cb => request(url).get('/api/wonderland/forktest/history')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					cb(null, res.body);
				}),

				(historyA, cb) => request(url).get('/api/wonderland/forktest2/history')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(historyA).to.deep.equal(res.body);
					cb();
				})
			], err => {
				done();
			});
		});

		it('should not affect changing the history of the one and the other', done => {
			async.series([
				cb => request(url).post('/api/wonderland/'+id+'/'+v[5]+'/map/new').set('Authorization', alice.token).send({'new': 'new'}).end((err, res) => {
						expect(err).to.be.null;
						expect(res.body.version).to.not.be.null;
						v[6] = res.body.version;
						cb();
					}),

				cb => request(url).post('/api/wonderland/forktest2/'+v[5]+'/map/newish').set('Authorization', alice.token).send({'newish': 'newish'}).end((err, res) => {
						expect(err).to.be.null;
						expect(res.body.version).to.not.be.null;
						v[7] = res.body.version;
						cb();
					})
			], err => {
				expect(v[6]).to.not.equal(v[7]);
				done();
			});
		});

		it('should not have the same version histories now', done => {
			async.waterfall([
				cb => request(url).get('/api/wonderland/forktest/history')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					console.log(res.body);
					expect(res.body[v[6]]).to.deep.equal([{parents: [], children: []}]);
					expect(res.body[v[5]]).to.deep.equal([v[6]]);
					cb(null, res.body);
				}),

				(historyA, cb) => request(url).get('/api/wonderland/forktest2/history')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(historyA).to.not.deep.equal(res.body);
					expect(res.body[v[7]]).to.deep.equal({parents: [], children: []});
					expect(res.body[v[5]]).to.deep.equal([v[7]]);
					cb();
				})
			], err => {
				done();
			});
		});

		it('should fork another version history', done => {
			request(url).post('/api/wonderland/forktest2/fork')
			.set('Authorization', alice.token)
			.send({namespace: 'wonderland', datastructureId: 'forktest3'})
			.end((err, res) => {

				expect(err).to.be.null;
				expect(res.body).to.deep.equal({type: 'map'});

				async.waterfall([
					cb => request(url).get('/api/wonderland/forktest2/history')
					.set('Authorization', alice.token)
					.end((err, res) => {
						expect(err).to.be.null;
						cb(null, res);
					}),

					(historyA, cb) => request(url).get('/api/wonderland/forktest3/history')
					.set('Authorization', alice.token)
					.end((err, res) => {
						expect(err).to.be.null;
						expect(historyA).to.not.equal(res);
						cb();
					})
				], err => {
					done();
				});
			});
		});
	});
};