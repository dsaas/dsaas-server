var request = require('supertest');
var expect = require('chai').expect;
var async = require('async');

module.exports = url => {
	describe('General', () => {
		
		var alice = require('../mock').alice;

		it('should log alice in to get the token', done => {
			request(url).post('/api/user/signin')
			.send({email: alice.email, password: alice.password})
			.end((err, res) => {
				alice.token = res.text;
				done();
			});
		});

		it('should create a new data structure in the given namespace', done => {
			request(url).post('/api/wonderland/testid')
				.set('Authorization', alice.token)
				.send({type: 'map'})
				.expect(200)
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body).to.deep.equal({});
					
					var datastructures = require('../../../schema/Datastructure');
					
					datastructures.findOne({namespace: 'wonderland', datastructureId: 'testid'}, (err, doc) => {
						expect(err).to.be.null;
						expect(doc.namespace).to.equal('wonderland');
						expect(doc.datastructureId).to.equal('testid');
						expect(doc.type).to.equal('map');
						expect(doc.forkID).to.be.null;
						expect(doc.preferences).to.be.null;
					});
					done();
			  });
		});

		it('should add versions and test the version history', done => {
			var v = {};
			async.series([
				cb => request(url).post('/api/wonderland/testid/0/map/a').set('Authorization', alice.token).send({}).end((err, res) => {
						expect(err).to.be.null;
						expect(res.body.version).to.not.be.null;
						v[1] = res.body.version;
						cb();
					}),
				cb => request(url).post('/api/wonderland/testid/'+v[1]+'/map/b').set('Authorization', alice.token).send({}).end((err, res) => {
						expect(err).to.be.null;
						expect(res.body.version).to.not.be.null;
						v[2] = res.body.version;
						cb();
					}),
				cb => request(url).post('/api/wonderland/testid/'+v[1]+'/map/b').set('Authorization', alice.token).send({whaat: 'whaat'}).end((err, res) => {
						expect(err).to.be.null;
						expect(res.body.version).to.not.be.null;
						v[3] = res.body.version;
						cb();
					}),
				cb => request(url).get('/api/wonderland/testid/history').set('Authorization', alice.token).end((err, res) => {
						expect(err).to.be.null;
						var history = {0: v[1]};

						expect(res.body[0]).to.deep.equal({parents: [], children: [v[1]]});
						expect(res.body[v[2]]).to.deep.equal({parents: [v[1]], children: []});
						expect(res.body[v[3]]).to.deep.equal({parents: [v[1]], children: []});
						cb();
					})
			], err => {
				done();
			});
		});
	});

	require('./forking')(url);
};