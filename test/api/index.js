var expect = require('chai').expect;
var util = require('util');
var leveldown = require('leveldown');
var mongoose = require('mongoose');
var async = require('async');

process.env.MONGODB_TEST = process.env.MONGODB;

describe('API Testing', () => {
	var db, url = 'http://localhost:3000';

	function drop(fn) {
		mongoose.connection.db.dropDatabase(fn);
	}

	before(done => {
		mongoose.connect('mongodb://' + process.env.MONGODB);
		mongoose.connection.on('open', () => {
			// Drop all changes
			drop(done);
		})

		// leveldown.destroy('./', err => {
		// 	if (err) console.log(err);
		// 	db = require('../../config/leveldb')();
		// 	done();
		// });
	});

	require('./user')(url);
	require('./general')(url);
	require('./map')(url);
	require('./graph')(url);

	after(done => {
		drop(done);
		// Delete the leveldb
		// db.close(err => {
		// 	if (err) console.log(err);
		// 	leveldown.destroy('./', (err) => {
		// 		if (err) console.log(err);
		// 		done();		
		// 	});
		// });
		
	});
});