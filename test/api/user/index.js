var request = require('supertest');
var expect = require('chai').expect;
var async = require('async');
var mongoose = require('mongoose');
var Users = require('../../../schema/User');
var JWTCollection = require('../../../schema/JWT');

module.exports = url => {
	describe('User', () => {
		
		var alice = require('../mock').alice;

		it('should be able to create a new user', done => {
			request(url).post('/api/user')
			.send(alice)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.not.be.null;
				alice._id = res.body;
				done();
				// Check database
				// Users.findById(res.body, (err, user) => {
				// 	expect(err).to.be.null;
				// 	for (var check of ['name', '_id', 'namespace'])
				// 		expect(String(user[check])).to.equal(alice[check]);
				// 	done();
				// });
			});
		});

		it('should be able to sign in the user (email + password)', done => {
			request(url).post('/api/user/signin')
			.send({email: alice.email, password: alice.password})
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.not.be.null;
				alice.token = res.text;

				JWTCollection.findOne({name: 'personal', userId: alice._id}, (err, jwtToken) => {
					if (err) console.log(err);
					expect(jwtToken.enabled).to.be.true;
					done();
				});

			});

		});

		it('should be able to retrieve the user\'s own profile', done => {
			request(url).get('/api/user')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;

				for (var check of ['name', '_id', 'namespace'])
					expect(String(res.body[check])).to.equal(alice[check]);

				done();
			});
		});

		it('should be able to sign out the user', done => {
			request(url).post('/api/user/signout')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should not be able to retrieve the user\'s own profile', done => {
			request(url).get('/api/user')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err.status).to.equal(401);
				done();
			});
		});

		it('should be able to sign in the user (email + password) (second time)', done => {
			request(url).post('/api/user/signin')
			.send({email: alice.email, password: alice.password})
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.not.be.null;
				alice.token = res.text;

				JWTCollection.findOne({name: 'personal', userId: alice._id}, (err, jwtToken) => {
					if (err) console.log(err);
					expect(jwtToken.enabled).to.be.true;
					done();
				});

			});

		});

		it('should be able to retrieve the user\'s own profile (second time)', done => {
			request(url).get('/api/user')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;

				for (var check of ['name', '_id', 'namespace'])
					expect(String(res.body[check])).to.equal(alice[check]);

				done();
			});
		});

		it('should be able to update the user\'s profile', done => {
			alice.name = 'alise';
			alice.surname = 'crooke';

			request(url).put('/api/user')
			.send(alice)
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('updated');
				done();
			});
		});

		it('should create multiple users (3)', done => {
			async.parallel([
				cb => request(url).post('/api/user').send({email: 'koos@gmail.com', password: 'abcdef', namespace: 'koos'})
					.end(cb),
				cb => request(url).post('/api/user').send({email: 'japie@gmail.com', password: 'bcdefg', namespace: 'japie'})
					.end(cb),
				cb => request(url).post('/api/user').send({email: 'random@gmail.com', password: 'cdefgh', namespace: 'random'})
					.end(cb)
			], (err, fin) => {
				done();
			});
		});

		it('should return multiple users', done => {
			request(url).get('/api/users').end((err, res) => {
				expect(err).to.be.null;
				expect(res.body.length).to.equal(4);

				request(url).get('/api/users?email=gmail').end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.length).to.equal(3);
					done();
				});
			});
		});

		it('should create a data structure (map)', done => {
			request(url).post('/api/wonderland/explore/')
			.set('Authorization', alice.token)
			.send({type: 'map'})
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});


		//
		//
		// Permissions
		// 
		// 
		it('should retrieve users permission on some data structure (0)', done => {
			request(url).get('/api/wonderland/explore/access')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.deep.equal([
					{_user: 'public', type: 'none'},
					{_user: 'registered', type: 'none'}
				]);
				done();
			});
		});

		it('should give a user permission on some data structure', done => {
			request(url).put('/api/wonderland/explore/access/koos@gmail.com')
			.set('Authorization', alice.token)
			.send({type: 'read'})
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');

				request(url).put('/api/wonderland/explore/access/random@gmail.com')
				.set('Authorization', alice.token)
				.send({type: 'read/write'})
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.text).to.equal('ok');

					done();
				});
			});
		});

		it('should retreive users permission on some data structure (2)', done => {
			request(url).get('/api/wonderland/explore/access')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body.length).to.equal(4);
				expect(res.body[0].type).to.equal('read');
				expect(res.body[1].type).to.equal('read/write');
				done();
			});
		});

		it('should update user permission on the data structure', done => {
			request(url).post('/api/wonderland/explore/access/koos@gmail.com')
			.set('Authorization', alice.token)
			.send({type: 'admin'})
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should retrieve the new user permissions on the data structure', done => {
			request(url).get('/api/wonderland/explore/access')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body.length).to.equal(4);
				done();
			});
		});

		it('should revoke a user\'s permission on the data structure', done => {
			request(url).delete('/api/wonderland/explore/access/koos@gmail.com')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should retrieve all the user\'s on the data structure excluding the removed one', done => {
			request(url).get('/api/wonderland/explore/access')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body.length).to.equal(3);
				expect(res.body[0].type).to.equal('read/write');
				done();
			});
		});

		it('should revoke another user\'s permission on the data structure', done => {
			request(url).delete('/api/wonderland/explore/access/random@gmail.com')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should be able to give registered users in general permission on some data structure', done => {
			request(url).post('/api/wonderland/explore/access/registered')
			.set('Authorization', alice.token)
			.send({type: 'read/write'})
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should be able to retrieve permissions, showing that only registered user\'s have access', done => {
			request(url).get('/api/wonderland/explore/access')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.deep.equal([
					{_user: 'public', type: 'none'},
					{_user: 'registered', type: 'read/write'}
				]);
				done();
			});
		});

		it('should be able to give public users in general permission on some data structure', done => {
			request(url).post('/api/wonderland/explore/access/public')
			.set('Authorization', alice.token)
			.send({type: 'read/write'})
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should be able to retrieve permissions, showing that only registered user\'s have access', done => {
			request(url).get('/api/wonderland/explore/access')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.deep.equal([
					{_user: 'public', type: 'read/write'},
					{_user: 'registered', type: 'read/write'}
				]);
				done();
			});
		});

		it('should be able to create an element on the data structure', done => {
			request(url).post('/api/wonderland/explore/0/map/akey')
				.set('Authorization', alice.token)
				.send({val: 'levalue'})
				.end((err, res) => {
					expect(err).to.be.null;
					expect(res.body.version).to.not.be.null;
					done();
				});
		});

		// 
		// 
		// TOKENS
		// 
		// 
		var laptopToken;
		it('should create an auth jwt', done => {
			request(url).put('/api/token/laptop')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				laptopToken = res.text;
				expect(res.text).to.not.be.null;
				done();
			});
		});

		it('should be able to retrieve auth jwt', done => {
			request(url).get('/api/token/laptop')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body.token).to.equal(laptopToken);
				expect(res.body.name).to.equal('laptop');
				expect(res.body.enabled).to.equal(true);
				done();
			});
		});


		it('should be able to retrieve all jwt tokens', done => {
			request(url).get('/api/token/laptop')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				laptopToken = res.body.token;
				
				request(url).get('/api/token')
				.set('Authorization', alice.token)
				.end((err, res) => {
					expect(err).to.be.null;
					var tokens = [{name: 'personal', enabled: true, token: alice.token}, {name: 'laptop', enabled: true, token: laptopToken}];
					if (res.body[0].name == 'personal') {
						expect(res.body[0]).to.deep.equal(tokens[0]);
						expect(res.body[1]).to.deep.equal(tokens[1]);
					} else {
						expect(res.body[0]).to.deep.equal(tokens[1]);
						expect(res.body[1]).to.deep.equal(tokens[0]);
					}
					done();
				});
			});
		});

		it('should be able to disable the jwt token', done => {
			request(url).post('/api/token/laptop/disable')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should be able to retrieve all jwt tokens', done => {
			request(url).get('/api/token')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.deep.equal([{name: 'personal', enabled: true, token: alice.token}, {name: 'laptop', enabled: false, token: laptopToken}]);
				done();
			});
		});

		it('should be able to enable the jwt token', done => {
			request(url).post('/api/token/laptop/enable')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should be able to retrieve all jwt tokens', done => {
			request(url).get('/api/token')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.deep.equal([{name: 'personal', enabled: true, token: alice.token}, {name: 'laptop', enabled: true, token: laptopToken}]);
				done();
			});
		});

		it('should remove the jwt token', done => {
			request(url).delete('/api/token/laptop')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.text).to.equal('ok');
				done();
			});
		});

		it('should be able to retrieve all jwt tokens', done => {
			request(url).get('/api/token')
			.set('Authorization', alice.token)
			.end((err, res) => {
				expect(err).to.be.null;
				expect(res.body).to.deep.equal([{name: 'personal', enabled: true, token: alice.token}]);
				done();
			});
		});
	});
};