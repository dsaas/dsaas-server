LABELS = {
	'node': 'node'
};

module.exports = Visual = function (versionedGraph, version) {
	var edges = versionedGraph.getEdges(version);
	var nodes = versionedGraph.getNodes(version);
	var seenNodes = {};
	console.log('digraph G {');
	for (var n in nodes) {
		n = nodes[n];
		if (!seenNodes[n]) {
			// seenNodes[n] = true;
			dfs(n, versionedGraph, version, seenNodes);
		}
	}
	console.log('}');
};

function dfs(nodeId, versionedGraph, version, seen) {
	if (seen[nodeId]) return;
	var node = versionedGraph.getNode(nodeId, version);
	console.log(nodeId + ' [label="'+node.props+'"];');
	seen[nodeId] = true;
	for (var e in node.edges) {
		e = node.edges[e];
		var edge = versionedGraph.getEdge(e, version);
		if (edge.from == nodeId) {
			console.log(''+edge.from, '->', edge.to + ';');
			dfs(edge.to, versionedGraph, version, seen);
		}
	}
}