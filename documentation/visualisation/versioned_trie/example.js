var co = require('co');
co(function *() {
	var VersionedTrie = require('../../../data_structure/VersionedTrie');
	var db = require('../../../config/leveldb')();
	var vt = new VersionedTrie('visual1', db);
	var printVisual = require('./visual');

	yield gen3(vt);

	var version = process.argv[2];
	var startversion = process.argv[3] || 0;
	yield printVisual(vt, startversion, version);
}).then(
res => {},
err => {
	console.log(err);
}
);

function* gen1(vt) {
	yield vt.create();
	yield vt.put({'abcde': 'a'}, 0);
	yield vt.put({'fghij': 'b'}, 0);
	yield vt.put({'klmno': 'c'}, 0);
	yield vt.put({'pqrst': 'd'}, 0);

	yield vt.merge(1,2); // Version 5
	yield vt.merge(3,4); // Version 6
	yield vt.merge(5,6); // Version 7
	yield vt.remove(['abcde'], 7);
	yield vt.put({'blabla': 'e'}, 8);
}

function* gen2(vt) {
	yield vt.create();
	yield vt.put({'aa': 1}, 0);
	yield vt.put({'bb': 2}, 0);
	yield vt.put({'cc': 3}, 0);
	yield vt.put({'dd': 4}, 0);

	yield vt.merge(4, 3);

	yield vt.put({'ee': 5}, 5);
	yield vt.put({'ff': 6}, 5);
	yield vt.put({'gg': 7}, 5);

	yield vt.merge(2, 1);
	yield vt.merge(8, 7);
	yield vt.merge(9, 6);
	yield vt.merge(11, 10);
}

function* gen3(vt) {
	yield vt.create();

	yield vt.put({'abcde': 'a', 'fghij': 'b', 'klmno': 'c'}, 0);
	yield vt.put({'c': 'c', 'd': 'd', 'e': 'e'}, 0);
	yield vt.remove(['c', 'd'], 2);
	yield vt.merge(3, 1);
	yield vt.remove(['abcde'], 4);
}