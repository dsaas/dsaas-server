'use strict';

var LABELS = {
	'node': 'node',
	'value': 'package',
	'version': 'version',
	'root': 'root'
};

function* printVisual(vt, start, end) {
	var edges = {};
	console.log('digraph G {');

	for (var v = start; v <= end; v++) {
		try {
			var rootNode = yield vt.getRoot(v);
			console.log(LABELS.root + v, '[label="'+LABELS.version+v+'" fontcolor=blue];');
			yield dfs(rootNode, 0, edges, vt, v);
		} catch (err) {}
	}
	console.log('}');
}

function* dfs(node, depth, edges, vt, isRoot) {
	if (node === null) return;
	try {
		if (node.type() === 'Node') {
			for (var pointer of node.arrayOfPointers) {
				if (typeof(pointer) === 'number') {
					pointer = yield vt.getNode(pointer);
					node.setPointer(pointer.HID, pointer);
				}
				var type = pointer.type() === 'Node' ? LABELS.node : LABELS.value;
				var nodeLabel = typeof(isRoot) === 'number' ? LABELS.root + isRoot : LABELS.node + node.id;
				var edge = nodeLabel + ' -> '+ type + String(pointer) + ';';
				if (!edges[edge]) {
					console.log(edge);
					if (pointer.type() !== 'Node') {
						console.log(type + String(pointer) + ' [shape=rectangle label="'+pointer.key+':'+pointer.value+'"];');
					}
					edges[edge] = true;
					yield dfs(pointer, depth+1, edges, vt, false);
				}
			}
		}
	} catch (err) {
		throw Error('Failed in DFS, \n', err.message);
	}
}

module.exports = printVisual;