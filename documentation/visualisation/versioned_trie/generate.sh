echo "Creating the visualization"
file="versionedtrie"
num=$1
rm -r db_data
node --harmony example.js ${num} > ${file}.dot
dot -Tps ${file}.dot -o ${file}.eps
# rm ${file}.dot
open ${file}.eps
