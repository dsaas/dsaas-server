#! /bin/sh

file='output'
for i in {10..22}
do
	node TopologicalSort.js ${i} > ${file}${i}.dot
	dot -Tps ${file}${i}.dot -o ${file}${i}.ps
	open ${file}${i}.ps &
done
rm *.dot