/**
 * Topological sort using the VersionedGraph.
 */
var VersionedGraph = require('../../../version_control/datastore/VersionedGraph');
var VisualizeGraph = require('../../visualisation/versioned_graph/visual');
var VisualizeTrie = require('../../visualisation/versioned_trie/visual');

var graph = new VersionedGraph();
	
	// Nodes
var eat 		= graph.addNode('eat').id,
	sleep 		= graph.addNode('sleep').id,
	rave 		= graph.addNode('rave').id,
	findWork	= graph.addNode('find work').id,
	work 		= graph.addNode('work').id,
	washHands 	= graph.addNode('wash hands').id,
	toilet 		= graph.addNode('toilet').id,
	
	// Edges
	eat_sleep 	= graph.addEdge(eat, sleep).id,
	eat_rave 	= graph.addEdge(eat, rave).id,
	eat_work 	= graph.addEdge(eat, work).id,
	washHands_eat = graph.addEdge(washHands, eat).id,
	findWork_work = graph.addEdge(findWork, work).id,
	work_toilet   = graph.addEdge(work, toilet).id,
	sleep_toilet  = graph.addEdge(sleep, toilet).id;

// VisualizeTrie(graph.nodes,graph.nodes.version-1, graph.nodes.version);
// VisualizeTrie(graph.nodes,0	, 6);
// console.log(graph.nodes.store[0]);
// console.log(graph.nodes.store[14]);

// Actual topological sort.
var sorted = [];
while (graph.getNodes().length > 0) {
	var nodeIds = graph.getNodes();
	// Find a node without dependencies
	var node, found;
	for (var n in nodeIds) {
		n = nodeIds[n];
		node = graph.getNode(n);
		found = true;
		for (var e in node.edges) {
			e = node.edges[e];
			var edge = graph.getEdge(e);
			if (edge.to == n) { found = false; break; }
		}
		if (found) break;
	}

	// Add node
	sorted.push(node);

	// Remove node and its edges
	for (var e in node.edges) {
		e = node.edges[e];
		graph.removeEdge(e);
	}
	graph.removeNode(node);
}

console.log(sorted);
VisualizeGraph(graph, process.argv[2] || graph.head);